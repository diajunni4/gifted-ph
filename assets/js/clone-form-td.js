/*
Author: Tristan Denyer (based on Charlie Griefer's original clone code, and some great help from Dan - see his comments in blog post)
Plugin repo: https://github.com/tristandenyer/Clone-section-of-form-using-jQuery
Demo at http://tristandenyer.com/using-jquery-to-duplicate-a-section-of-a-form-maintaining-accessibility/
Ver: 0.9.5.0
Last updated: Oct 23, 2015

The MIT License (MIT)

Copyright (c) 2011 Tristan Denyer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
$(function () {
    $('#btnAdd').click(function () {
        var num     = $('.clonedInput').length, // Checks to see how many "duplicatable" input fields we currently have
            newNum  = new Number(num + 1),      // The numeric ID of the new input field being added, increasing by 1 each time
            newElem = $('#entry' + num).clone().attr('id', 'entry' + newNum).fadeIn('slow'); // create the new element via clone(), and manipulate it's ID using newNum value
		
    /*  This is where we manipulate the name/id values of the input inside the new, cloned element
        Below are examples of what forms elements you can clone, but not the only ones.
        There are 2 basic structures below: one for an H2, and one for form elements.
        To make more, you can copy the one for form elements and simply update the classes for its label and input.
        Keep in mind that the .val() method is what clears the element when it gets cloned. Radio and checkboxes need .val([]) instead of .val('').
    */
        // H2 - section
        newElem.find('.heading-reference').attr('id', 'ID' + newNum + '_reference').attr('name', 'ID' + newNum + '_reference').html('Order #' + newNum);
		
		// Brand - select
        newElem.find('.label_product_id').attr('for', 'product_id'+ newNum);
        newElem.find('.select_product_id').attr('id', 'product_id'+ newNum).val('');
		
		// Greetings Card - select
        newElem.find('.label_recipient_card_id').attr('for', 'recipient_card_id'+ newNum);
        newElem.find('.select_recipient_card_id').attr('id', 'recipient_card_id'+ newNum).val('');
		
		// Denomination - select
        newElem.find('.label_product_denomination').attr('for', 'product_denomination'+ newNum);
		newElem.find('.select_product_denomination').attr('id', 'product_denomination'+ newNum).val('');
		newElem.find('#product_denomination'+ newNum+' option').remove();
        newElem.find('#product_denomination'+ newNum).append($("<option></option>").attr("value","").text("No Brand Selected"));
		
		// Quantity - text
        newElem.find('.label_quantity').attr('for', 'quantity'+ newNum);
        newElem.find('.input_quantity').attr('id', 'quantity'+ newNum).val(1);
		
		// Ammount - text
        newElem.find('.label_amount').attr('for', 'amount'+ newNum);
        newElem.find('.input_amount').attr('id', 'amount'+ newNum).val(0);
		
		// Firstname - text
        newElem.find('.label_fname').attr('for', 'fname'+ newNum);
        newElem.find('.input_fname').attr('id', 'fname'+ newNum).val('');
		
		// Lastname - text
        newElem.find('.label_lname').attr('for', 'lname'+ newNum);
        newElem.find('.input_lname').attr('id', 'lname'+ newNum).val('');
		
		// Email - text
        newElem.find('.label_email').attr('for', 'email'+ newNum);
        newElem.find('.input_email').attr('id', 'email'+ newNum).val('');
		
		// Recommended Gift Items - select
        newElem.find('.label_subcategory').attr('for', 'subcategory'+ newNum);
		newElem.find('.select_subcategory').attr('id', 'subcategory'+ newNum).val('');
		newElem.find('#subcategory'+ newNum+' option').remove();
        newElem.find('#subcategory'+ newNum).append($("<option></option>").attr("value","").text("No Brand Selected"));
		
		// Message - text
        newElem.find('.label_message').attr('for', 'message'+ newNum);
        newElem.find('.input_message').attr('id', 'message'+ newNum).val('');
		
		// Send Date - text
        newElem.find('.label_send_date').attr('for', 'send_date'+ newNum);
        newElem.find('.input_send_date').attr('id', 'send_date'+ newNum).val('');
		
		// FromX - text
        newElem.find('.label_fromx').attr('for', 'fromx'+ newNum);
        newElem.find('.input_fromx').attr('id', 'fromx'+ newNum).val('');
		
		
		
		
	/*
        // Title - select
        newElem.find('.label_ttl').attr('for', 'ID' + newNum + '_title');
        newElem.find('.select_ttl').attr('id', 'ID' + newNum + '_title').attr('name', 'ID' + newNum + '_title').val('');

        // First name - text
        newElem.find('.label_fn').attr('for', 'ID' + newNum + '_first_name');
        newElem.find('.input_fn').attr('id', 'ID' + newNum + '_first_name').attr('name', 'ID' + newNum + '_first_name').val('');

        // Last name - text
        newElem.find('.label_ln').attr('for', 'ID' + newNum + '_last_name');
        newElem.find('.input_ln').attr('id', 'ID' + newNum + '_last_name').attr('name', 'ID' + newNum + '_last_name').val('');

        // Flavor - checkbox
        // Note that each input_checkboxitem has a unique identifier "-0". This helps pair up duplicated checkboxes and labels correctly. A bit verbose, at the moment.
        newElem.find('.label_checkboxitem').attr('for', 'ID' + newNum + '_checkboxitem');
        newElem.find('.input_checkboxitem-0').attr('id', 'ID' + newNum + '_checkboxitem-0').attr('name', 'ID' + newNum + '_checkboxitem').val([]);
        newElem.find('.input_checkboxitem-1').attr('id', 'ID' + newNum + '_checkboxitem-1').attr('name', 'ID' + newNum + '_checkboxitem').val([]);
        newElem.find('.input_checkboxitem-2').attr('id', 'ID' + newNum + '_checkboxitem-2').attr('name', 'ID' + newNum + '_checkboxitem').val([]);
        newElem.find('.input_checkboxitem-3').attr('id', 'ID' + newNum + '_checkboxitem-3').attr('name', 'ID' + newNum + '_checkboxitem').val([]);

        // Flavor - checkbox labels
        // Note that each checkboxitem has a unique identifier "-0". This helps pair up duplicated checkboxes and labels correctly. A bit verbose, at the moment.
        newElem.find('.checkboxitem-0').attr('for', 'ID' + newNum + '_checkboxitem-0');
        newElem.find('.checkboxitem-1').attr('for', 'ID' + newNum + '_checkboxitem-1');
        newElem.find('.checkboxitem-2').attr('for', 'ID' + newNum + '_checkboxitem-2');
        newElem.find('.checkboxitem-3').attr('for', 'ID' + newNum + '_checkboxitem-3');

        // Skate - radio
        newElem.find('.label_radio').attr('for', 'ID' + newNum + '_radioitem');
        newElem.find('.input_radio').attr('id', 'ID' + newNum + '_radioitem').attr('name', 'ID' + newNum + '_radioitem').val([]);

        // Email - text
        newElem.find('.label_email').attr('for', 'ID' + newNum + '_email_address');
        newElem.find('.input_email').attr('id', 'ID' + newNum + '_email_address').attr('name', 'ID' + newNum + '_email_address').val('');

        // Twitter handle (for Bootstrap demo) - append and text
        newElem.find('.label_twt').attr('for', 'ID' + newNum + '_twitter_handle');
        newElem.find('.input_twt').attr('id', 'ID' + newNum + '_twitter_handle').attr('name', 'ID' + newNum + '_twitter_handle').val('');
	*/
		
    // Insert the new element after the last "duplicatable" input field
        $('#entry' + num).after(newElem);
        $('#ID' + newNum + '_title').focus();

    // Enable the "remove" button. This only shows once you have a duplicated section.
        $('#btnDel').attr('disabled', false);

    // Right now you can only add 4 sections, for a total of 5. Change '5' below to the max number of sections you want to allow.
        //if (newNum == 5)
        //$('#btnAdd').attr('disabled', true).prop('value', "You've reached the limit"); // value here updates the text in the 'add' button when the limit is reached 
  	

   });

    $('#btnDel').click(function () {
    // Confirmation dialog box. Works on all desktop browsers and iPhone.
        if (confirm("Are you sure you wish to remove this order? This cannot be undone."))
            {
                var num = $('.clonedInput').length;
                // how many "duplicatable" input fields we currently have
                $('#entry' + num).slideUp('slow', function () {$(this).remove();
                // if only one element remains, disable the "remove" button
                    if (num -1 === 1)
                $('#btnDel').attr('disabled', true);
                // enable the "add" button
                $('#btnAdd').attr('disabled', false).prop('value', "add section");});
            }
        return false; // Removes the last section you added
    });
    // Enable the "add" button
    $('#btnAdd').attr('disabled', false);
    // Disable the "remove" button
    $('#btnDel').attr('disabled', true);
});