$(document).ready(function(){

	function load_manacost(data){
		var manacost_disp = '';
		var newStr = data.replace(/[}{]/g, '');

		for(var count = 0; count < newStr.length; count++) {
			manacost_disp += '<i class="mana-'+ newStr[count].toLowerCase() +'"></i>';
		}
		return manacost_disp;
	}
	var manacost = $('.mana_cost').html();
	if(manacost != ''){
		$('.mana_cost').html(load_manacost(manacost));
	}
 
	function related_card_section(){
		$(".related-card-section").not('.slick-initialized').slick({
			lazyLoad: 'ondemand',
	    autoplay: true,
	    dots: true,
	    slidesToShow: 4,
	  	slidesToScroll: 4,
	    responsive: [{ 
	        breakpoint: 767,
	        settings: {
	        arrows: false,
	        infinite: false,
	        slidesToShow: 2,
	        slidesToScroll: 2
	        } 
	    }]
		});	
	}

	var card_ability = convertSymbol($('.card-ability').html());
	$('.card-ability').html(card_ability);

	$(document).on('click', '.add_item_to_wishlist', function(){
		var id = $(this).data('id');

		if($(this).hasClass('btn-success')){
			$.ajax({
				type: 'POST',
				url: base_url + 'api/add_item_to_wishlist',
				data: {
					id: id
				},
				success: function(data){

				}
			});

			$(this).html('Remove from Watchlist');
			$(this).removeClass('btn-success');
			$(this).addClass('btn-danger');
			return;
		}

		$.ajax({
			type: 'POST',
			url: base_url + 'api/remove_item_to_wishlist',
			data: {
				id: id
			},
			success: function(data){

			}
		});
		$(this).html($(this).data('default-text'));
		$(this).addClass('btn-success');
		$(this).removeClass('btn-danger');
	});

	related_card_section();
	//Now it will not throw error, even if called multiple times.
	$(window).on( 'resize', related_card_section );

	var category_tree = [];

	card_category.forEach(function(cat){
		var subcategory = cat.subcategory;
		var sub_category_code = JSON.parse(cat.sub_category_code);
		if(cat.subcategory){
			var subcat_list = [];

			subcategory.forEach(function(subcat, index){
				var sub_code = sub_category_code[index] + '';
				if(sub_code != ""){
					subcat_list.push({
						text: subcat,
						href: base_url + 'cards/?cat=' + sub_code.toLowerCase()
					});
				}
				
			});

			category_tree.push({
				text: '<span style="color:#333">'+cat.main_category+'</span>',
			  state: {
			    expanded: false
			  },
				nodes: subcat_list
			});
		}
	});

	$('#card-tree-view').treeview({
		data: category_tree,
		enableLinks: true, 
		multiSelect: false
	});

	$(document).on('mouseup', '.node-card-tree-view', function(){
		$(this).find('.expand-icon').trigger('click');
	});

});