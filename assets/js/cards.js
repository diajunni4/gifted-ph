$(document).ready(function(){

	
	var cards_list;
	var card_list_all;
	var gallery_count = 0;
	var gallery_max_count = 24;
	var gallery_load = true;
	var show_stock = true;

	var card_table_html = '<table class="table table-hover table-responsive table-card-list cards_table_list">'
			+	'<thead>'
			+		'<th>Image</th>'
			+		'<th>Name</th>'
			+		'<th>Edition</th>'
			// +		'<th>Cost</th>'
			+		'<th>Condition</th>'
			+		'<th style="color:black !important;padding-right:20px !important;">Price</th>'
			+		'<th></th>'
			+	'</thead>'
			+	'<tbody>'
			+	'</tbody>'
			+'</table>';

	load_all_cards();

	function load_all_cards() {
		$('.loading-container').show();
		if(display_type != 'all'){
		 $('.filter-variation [data-value="regular"]').prop('checked', true);
		 $('.mobile-filter-variation [data-value="regular"]').addClass('active');
		}
		for(var i in session_sort['sort']) {
			$('.checkbox-group[data-value="' + i +'"] input[data-value="' + session_sort['sort'][i] + '"]').prop('checked', true);
		  $('.mobile-group-filter[data-value="' + i +'"] [data-value="' + session_sort['sort'][i] + '"]').addClass('active');
		}

		sort_product();
		return;

		if(search_name || cat_code) {
			$.ajax({
				type: 'POST',
				url: base_url + 'api/get_product',
				data: {
					search_name: search_name,
					cat_code: cat_code
				},
				success: function(data){
					$('.loading-container').hide();
					card_list_all = JSON.parse(data);
					cards_list = card_list_all;
					load_datatable(cards_list);
					load_galleryview(cards_list);
				}
			});
			return;
		}

		$.ajax({
			type: 'POST',
			url: base_url + 'api/get_all_products',
			success: function(data){
				$('.loading-container').hide();
				card_list_all = JSON.parse(data);
				cards_list = card_list_all;
				$('.checkbox-group [data-value="all"]').prop('checked', true);
				load_datatable(cards_list);
				load_galleryview(cards_list);
			}
		});
	}
	$(window).on( 'orientationchange', function(){
		if($('#card-list-container').html() != ''){
			load_datatable(cards_list);
		}
	} );

	function truncate_name(input) {
	   if (input.length > 18)
	      return input.substring(0,18) + '...';
	   else
	      return input;
	};

	function load_galleryview(list){
		$('.load_gallery_view').hide();
		var disp = '';
		if(gallery_count == 0) disp += '<div style="margin-bottom: 10px;    display: block;width: 100%;"><input type="checkbox" name="show_stock" class="show_stock" style="margin-left: 10px"/> Show In Stock Items Only</div>';
		if(gallery_count >= list.length) return;
		while(gallery_count < list.length && gallery_count != gallery_max_count) {
			var card = list[gallery_count];
			var img_src = getCardImage(card);
			var card_name = '';
			var price = card.regular_price ?  card.regular_price : card.foil_price; 
			if(card.rarity=='T'){
				if(card.type.includes('Basic Land')) {
					card_name = card.name + ' (' + card.collector_number + ') (' + card.set_code +')';
				} else {
					card_name = card.name;
				}
			} else {
				if(card.type.includes('Basic Land')) {
					card_name = card.name + ' (' + card.collector_number + ') (' + card.set_code +')';
				} else {
					card_name = card.name;
				}
			} 
			var tmp_card_name = card_name
			card_name = truncate_name(card_name);
			disp += '<div class=" col-xs-3 col-md-2 col-sm-3 galley_view_container">';
			if(card.regular_price){
				disp += '<div class="card-image image-container"><a href="javascript:void(0);"><img class="lazy" data-original="'+ img_src +'" style="width:100%;" alt="'+card_name+'" ><div class="middle"><div class="preview_button"><i class="fa fa-search"></i></div></div></a></div>';
			} else{
				disp += '<div class="card-image image-container"><a href="javascript:void(0);"><img class="lazy" data-original="'+ img_src +'" style="width:100%;" alt="'+card_name+'" ><div class="middle"><div class="preview_button"><i class="fa fa-search"></i></div></div></a><div class="after"></div></div>';
			}
			disp += '<div class="tab-content">';
			disp += '<div class="tab-pane tab-pane-border active card-price text-center">';
			disp += '<div class="card-name"><a href="'+ base_url +'cards/view/'+ card.id +'"><span title="'+tmp_card_name+'">'+ card_name +'</span></a></div>'
			disp += '<div class="card-set-code">'+ card.set_code +'</div>'
			if(rate){
				disp += '<div class="card-text-price">'+ price +'฿ <br><span class="sub_currency">('+parseFloat(price*rate).toFixed(2)+' '+currency+')</span></div>'
			} else {
				disp += '<div class="card-text-price">'+ price +'฿</div>'
			}
			//disp += '<div class="card-info">'+ card.fully_handled  + '</div>';
			disp += '<div>';
			//disp += '<ul class="nav nav-tabs" role="tablist"><li class="active"><a href="#">NM</a></li></ul>';
			
			if(card.stock > 0){
				if(cart_data[card.id]) { 
	  			disp += '<div class="add-cart-wrapper" data-id="'+card.id+'" data-index="'+gallery_count+'"><button class="btn btn-default add_to_cart" data-id="'+ card.id +'" data-target="card-gallery-quantity-content-'+ gallery_count +'" data-default-text="Add to Cart" data-toggle="popover" data-container="body" data-placement="top" data-html="true">' + cart_data[card.id].quantity + ' in Cart</button></div>'; 
	  		} else {
	    		disp += '<div class="add-cart-wrapper" data-id="'+card.id+'" data-index="'+gallery_count+'"><button class="btn btn-primary add_to_cart" data-id="'+ card.id +'" data-target="card-gallery-quantity-content-'+ gallery_count +'" data-default-text="Add to Cart" data-toggle="popover" data-container="body" data-placement="top" data-html="true">Add to Cart</button></div>';
	    	}

	  		disp += '<div id="card-gallery-quantity-content-'+ gallery_count +'" class="hide add-cart-wrapper">';
	  		disp += '<div class="text-center"> <span class="label-quantity">Select Qty</span> </div>';
	  		disp += '<div class="cart-quantity-content" data-container="card-quantity-content-'+ gallery_count +'" data-index="'+gallery_count+'" data-id="'+ card.id +'" style="width: 150px; min-height: 100px;">';
	  		for(var count = 0; count <= card.stock; count ++){
	  			if(card.rarity=='U' || card.rarity=='R' || card.rarity=='M'){
    				if(count <= 8)
    					disp += '<button style="width: 27%; margin: 5px 3%; height: 35px;" data-value="'+count+'">'+count+'</button>';	
    			} else {
    				if(count <= 24)
    					disp += '<button style="width: 27%; margin: 5px 3%; height: 35px;" data-value="'+count+'">'+count+'</button>';	
    			}
	  			        			
	  		}
	  		disp += '</div>';
	  		disp += '</div>';
	  	} else {
	    	disp += '<div><span class="out-of-stock">Out of Stock</span></div>';
	    }

			disp += '</div>';
			disp += '</div>';
			disp += '</div>';
			disp += '</div>';
			gallery_count++;
		}
		gallery_max_count += 24;
		if(gallery_count < list.length - 1) {
			$('.load_gallery_view').show();
		}

		$('#card-gallery-container .row').append(disp);
		$("#card-gallery-container [data-toggle=popover]").each(function(i, obj) {

			$(this).popover({
			  html: true,
			  content: function() {
			    var target = $(this).data('target');
			    return $('#' + target).html();
			  }
			});

		});
		gallery_load = false;
		$(function() {
	    	$("img.lazy").lazyload();
	    });
	}

	$(window).scroll(function() {
	   if($(window).scrollTop() + $(window).height() >= $(document).height() && !gallery_load) {
	   		if($('#card-gallery-container').hasClass('active')){
		   		gallery_load = true;
		      load_galleryview(cards_list);
		    }
	   }
	});

	$(document).on('click', '.load_gallery_view', function(){
		gallery_load = true;
	  load_galleryview(cards_list);
	});

	function load_datatable(list){
		
		$('#card-list-container').html(card_table_html);
		$('.table-card-list').DataTable({
			"searching": true,
			"lengthChange": true,
			"oLanguage": {
	       sLengthMenu: "Show _MENU_",
	    },
			drawCallback: function(){
        $("img.lazy").lazyload();
       	setTimeout(function() {
	       		 $("#card-list-container [data-toggle=popover]").each(function(i, obj) {
						$(this).popover({
						  html: true,
						  content: function() {
						    var target = $(this).data('target');
						    return $('#' + target).html();
						  }
						});
					});
	       		},500)
		   },
			data: list,
			'aoColumnDefs': [
            { 'sType': 'currency', 'aTargets': [4] }     // In this case 5th column will be sorted on currency basis.
         ],
	    "bDestroy": true,
	    "bStateSave" : false,  
			order: [[ 4, "desc" ]],
			"lengthMenu": [50, 100],
	    columns: [
	        { 
	        	data: 'id',
	        	searchable: false,
	        	bSortable: false,
	        	render: function(data, type, row, meta) {
	        		var img_src = getCardImage(row);
	        		if(row.regular_price){
		        		var disp = '<div class="card-image image-container" style="display: inline-block"><a href="javascript:void(0);"><img class="lazy" data-original="'+ img_src +'"  alt="'+ row.name +'"></a></div>';
		        	} else {		        		
		        		var disp = '<div class="card-image image-container" style="display: inline-block"><a href="javascript:void(0);"><img class="lazy" data-original="'+ img_src +'"  alt="'+ row.name +'"></a><div class="after"></div></div>';
		        	}
	        		return disp;
	        	}
	        },
	        { 
	        	data: 'name',
	        	render: function(data, type, row, meta) {
	        		if(row.rarity=='T'){
	        			if(row.type.includes('Basic Land')) {
	        				var disp = '<div class="card-name"><a href="'+ base_url +'cards/view/'+ row.id +'">' + data + ' (' + row.collector_number + ') (' + row.set_code +')'+ '</a>'
			        		// disp += ' (' + row.set_code + ')';
			        		disp += '</div>';
			        		disp += '<div class="card-type"><span>'+ row.type.replace('?','—') +'</span></div>'
			        		return disp;
	        			} else {
	        				var disp = '<div class="card-name"><a href="'+ base_url +'cards/view/'+ row.id +'">' + data + ' (' + row.set_code +')'+ '</a>'
			        		// disp += ' (' + row.set_code + ')';
			        		disp += '</div>';
			        		disp += '<div class="card-type"><span>'+ row.type.replace('?','—') +'</span></div>'
			        		return disp;
	        			}
	        		} else {
	        			if(row.type.includes('Basic Land')) {
	        				var disp = '<div class="card-name"><a href="'+ base_url +'cards/view/'+ row.id +'">' + data + ' (' + row.collector_number + ') (' + row.set_code +')'+ '</a>'
			        		// disp += ' (' + row.set_code + ')';
			        		disp += '</div>';
			        		disp += '<div class="card-type"><span>'+ row.type.replace('?','—') +'</span></div>'
			        		return disp;
	        			} else {
		        			var disp = '<div class="card-name"><a href="'+ base_url +'cards/view/'+ row.id +'">' + data + ' (' + row.set_code +')'+ '</a>'
			        		// disp += ' (' + row.set_code + ')';
			        		disp += '</div>';
			        		disp += '<div class="card-type"><span>'+ row.type.replace('?','—') +'</span></div>'
			        		return disp;
			        	}
	        		}
	        		
	        	}
	        },
	        { 
	        	data: 'set_code',
	        	bSortable: false,
	        	render: function(data, type, row, meta){
	        		if(data.toLowerCase() == "waraa")
	        			data = "war"
	        		
	        		var rarity = row.rarity;
	        		if(rarity == "U") {
	        			rarity = "uncommon"
	        		} else if (rarity == "C") {
	        			rarity = "common"
	        		} else if (rarity == "R") {
	        			rarity = "rare"
	        		} else if (rarity == "M") {
	        			rarity = "mythic"
	        		}
	        		var disp = '<i class="ss ss-'+ data.toLowerCase() +' ss-2x ss-'+rarity+'"></i>';
	        		return disp;
	        	} 
	        },
	      //   { 
	      //   	data: 'manacost',
	      //   	searchable: false,
	      //   	bSortable: false,
	      //   	render: function(data) {
	      //   		var disp = '';
	      //   		var newStr = data.replace(/[}{]/g, '');

							// for(var count = 0; count < newStr.length; count++) {
							// 	disp += '<i class="mana-'+ newStr[count].toLowerCase() +'"></i>';
							// }
							
	      //   		return disp;
	      //   	} 
	      //   },
	        { 
	        	data: 'fully_handled',
	        	searchable: false,
	        	bSortable: false,
	        	render: function(data, type, row, meta ) {
	        		return data ? "NM" : 'NM';
	        	}
	        },
	        { 
	        	data: 'price',
	        	searchable: false,
	        	render: function(data, type, row, meta){
	        		return '<b>'+data+'฿</b>'+'<br>('+parseFloat(data*rate).toFixed(2)+' '+currency+')';
	        	}
	        },
	        { 
	        	data: 'id',
	        	searchable: false,
	        	bSortable: false,
	        	render: function(data, type, row, meta) {
	        		if(row.stock > 0){
	        			if(cart_data[row.id]) { 
		        			var disp = '<div class="add-cart-wrapper" data-id="'+row.id+'" data-index="'+meta.row+'"><button class="btn btn-default add_to_cart" data-id="'+ row.id +'" data-target="card-quantity-content-'+ meta.row +'" data-default-text="Add to Cart" data-toggle="popover" data-container="body" data-placement="left" data-html="true">' + cart_data[row.id].quantity + ' in Cart</button>'; 
		        		} else {
			        		var disp = '<div class="add-cart-wrapper" data-id="'+row.id+'" data-index="'+meta.row+'"><button class="btn btn-primary add_to_cart" data-id="'+ row.id +'" data-target="card-quantity-content-'+ meta.row +'" data-default-text="Add to Cart" data-toggle="popover" data-container="body" data-placement="left" data-html="true">Add to Cart</button>';
			        	}
		        		disp += '</div>';

		        		disp += '<div id="card-quantity-content-'+ meta.row +'" class="hide add-cart-wrapper">';
		        		disp += '<div class="text-center"> <span class="label-quantity">Select Qty</span> </div>';
		        		disp += '<div class="cart-quantity-content" data-container="card-quantity-content-'+ meta.row +'" data-index="'+meta.row+'" data-id="'+ row.id +'" style="width: 150px; min-height: 100px;">';
		        		for(var count = 0; count <= row.stock; count ++){
		        			if(row.rarity=='U' || row.rarity=='R' || row.rarity=='M'){
		        				if(count <= 8)
		        					disp += '<button style="width: 27%; margin: 5px 3%; height: 35px;" data-value="'+count+'">'+count+'</button>';	 
		        			} else {
		        				if(count <= 24)
		        					disp += '<button style="width: 27%; margin: 5px 3%; height: 35px;" data-value="'+count+'">'+count+'</button>';	
		        			}
		        			       			
		        		}
		        		disp += '</div>';
		        		disp += '</div>';

		        	} else {
		        		var disp = '<div><span class="out-of-stock">Out of Stock</span></div>';
		        	}
		        	return disp;
	        	}
	        }
	    ]
		});

		$('.dataTables_length').append('<input type="checkbox" name="show_stock" class="show_stock" style="margin-left: 10px"/> Show In Stock Items Only');

		$("#card-list-container [data-toggle=popover]").each(function(i, obj) {
			$(this).popover({
			  html: true,
			  content: function() {
			    var target = $(this).data('target');
			    return $('#' + target).html();
			  }
			});

		});
	}

	$(document).on("click", ".paginate_button, .cards_table_list th:not(.sorting_disabled)", function() {
		enable_popover();
	});

	$(document).on("click", ".show_stock", function() {
		show_stock = !show_stock;
		sort_product();
	});

	function enable_popover() {
		$('.popover').remove();
		$("#card-list-container [data-toggle=popover]").each(function(i, obj) {

			$(this).popover({
			  html: true,
			  content: function() {
			    var target = $(this).data('target');
			    return $('#' + target).html();
			  }
			});

		});
	}

	$(document).on('click', '.table-card-list td:not(.active-row), .table-card-list th, .dataTables_paginate, .dataTables_info, .dataTables_filter', function(){
		$('.add-cart-wrapper').removeClass('active');
	});

	$(document).on('click', '.cart-quantity-content button', function(){
		var value = $(this).data('value');
		var index = $(this).closest('.cart-quantity-content').data('index');
		var id = $(this).closest('.cart-quantity-content').data('id');
		var add_cart_button = $('.add_to_cart[data-id="'+ id +'"]');
		var default_text = add_cart_button.data('default-text');

		if(cards_list[index]['id'] == id) {
			if(value == 0){
				delete cart_data[cards_list[index].id];
			} else {
				cart_data[cards_list[index].id] = {
					'card': cards_list[index], 
					'quantity': value
				};
			}
			var data = cart_data;
			cart_list = [];
			for (i in data) {
	  		var broken_img_src = base_url + "assets/images/no-image-slide.jpg";
	  		var img_src = data[i].card.set_code && data[i].card.collector_number && data[i].card.regular_price != "" ? 'http://staging.sabaicards.com/assets/images/regular_cards/' + data[i].card.set_code + '/' + pad(data[i].card.collector_number, 3) + '.jpg' : (data[i].card.set_code && data[i].card.collector_number && data[i].card.foil_price != "" ? 'http://staging.sabaicards.com/assets/images/regular_cards/' + data[i].card.set_code + '/' + pad(data[i].card.collector_number, 3) + '.jpg' : broken_img_src);
				data[i].card.img_src = img_src;
				data[i]['variation'] = parseFloat(data[i].card.price) == parseFloat(data[i].card.regular_price) ? 'Regular' : 'Foil';
			  cart_list.push(data[i]);
			}

			$('.add-cart-wrapper').removeClass('active');

			$.ajax({
				type: 'POST',
				url: base_url + 'api/add_item_to_cart',
				data: {
					card_id: cards_list[index].id,
					quantity: value
				},
				success: function(data) {
					$('.loading-container').hide();
					var output = JSON.parse(data);
					
					if(output.status == 'success'){
						cart_list = output.cart_list;
						
						if(value == 0){
							add_cart_button.removeClass('btn-default');
							add_cart_button.addClass('btn-primary');
							add_cart_button.html(default_text);
						} else {
							add_cart_button.addClass('btn-default');
							add_cart_button.removeClass('btn-primary');
							add_cart_button.html(value + ' in Cart');
						}
						bus.$emit('refresh_cart', cart_list);
					} else {
						toastr.error(output.message);
					}
				}
			})
		}
	});

	$('.card-image img').on("error", function () {
		$(this).attr('src', "assets/images/no-image-slide.jpg");
	});

	$(document).on('change', '.filter-section input', function(){
		var type_value = $(this).data('value');
		var data_name = $(this).attr('name');
		$('.mobile-filter-section a[data-name="' + data_name + '"]').removeClass('active');
		$('.mobile-filter-section a[data-name="' + data_name + '"][data-value="' + type_value + '"]').addClass('active');
		sort_product();
	});

	$(document).on('click', '.mobile-filter-section a', function(){
		var type_value = $(this).data('value');
		var data_name = $(this).data('name');
		$('.mobile-filter-section a[data-name="' + data_name + '"]').removeClass('active');
		$(this).addClass('active');
		$('.filter-section input[name="' + data_name + '"][data-value="' + type_value + '"]').prop('checked', true);
		$('.filter-section input[name="' + data_name + '"][data-value="' + type_value + '"]').trigger('change');
	});

	function sort_product() {
		$('#card-list-container').html('');
		$('#myDiv').html('');
		$('.loading-container').show();
		$('#card-gallery-container .row').html('');
		var array_filter = [];
		var filter_data = {};

		$('.filter-section input').each(function(){
			if(this.checked) {
				var filter = $(this).closest('.checkbox-group').data('value');
				array_filter[filter] = array_filter[filter] ? array_filter[filter] : [];
				filter_data[filter] = $(this).data('value');

				if(filter == 'color') {
					var filter_value = $(this).data('value');
					filter_value = filter_value.toLowerCase();

					switch(filter_value){
						case 'white':
						 array_filter[filter].push('W');
						 break;
						case 'blue':
						 array_filter[filter].push('U');
						 break;
						case 'black':
						 array_filter[filter].push('B');
						 break;
						case 'red':
						 array_filter[filter].push('R');
						 break;
						case 'green':
						 array_filter[filter].push('G');
						 break;
						case 'colorless':
						 array_filter[filter].push('Colorless');
						 break;
						case 'multi':
						 array_filter[filter].push('Multi');
						 break;
						case 'all':
						 array_filter[filter].push('all');
						 break;
					}
				} else if(filter == 'type') {
					var filter_value = $(this).data('value');
					filter_value = filter_value.toLowerCase();
					array_filter[filter].push(filter_value);
				} else if(filter == 'rarity') {
					var filter_value = $(this).data('value');
					filter_value = filter_value.toLowerCase();

					if(filter_value == 'mythic') {
						array_filter[filter].push('M');
					} else if(filter_value == 'rare') {
						array_filter[filter].push('R');
					} else if(filter_value == 'uncommon') {
						array_filter[filter].push('U');
					} else if(filter_value == 'common') {
						array_filter[filter].push('C');
					} else if(filter_value.toLowerCase() != 'all') {
						array_filter[filter].push('T');
					} else if(filter_value.toLowerCase() == 'all') {
						array_filter[filter].push('all');
					}

				} else if(filter == 'variation') {
					var filter_value = $(this).data('value');
					filter_value = filter_value.toLowerCase();
					array_filter[filter].push(filter_value);					
				}
				
			}
		});

		array_filter['rarity'] = array_filter['rarity'] ? array_filter['rarity'].indexOf('all') > -1 ? [] : array_filter['rarity'] : [];
		array_filter['color'] = array_filter['color'] ? array_filter['color'].indexOf('all') > -1 ? [] : array_filter['color'] : [];
		array_filter['type'] = array_filter['type'] ? array_filter['type'].indexOf('all') > -1 ? [] : array_filter['type'] : [];
		array_filter['variation'] = array_filter['variation'] ? array_filter['variation'].indexOf('all') > -1 ? [] : array_filter['variation'] : ['all'];

		$.ajax({
			type: 'POST',
			url: base_url + 'api/sort_product',
			data: {
				search_name: search_name,
				cat_code: cat_code,
				filter_rarity: JSON.stringify(array_filter['rarity']),
				filter_color: JSON.stringify(array_filter['color']),
				filter_type: JSON.stringify(array_filter['type']),
				filter_variation: JSON.stringify(array_filter['variation']),
				filter_data: JSON.stringify(filter_data),
				show_stock: show_stock
			},
			success: function(data) {
				cards_list = JSON.parse(data);
				cards_list = _.filter(cards_list, function(el){
					el.price = parseFloat(el.price);
					return el;
				})
				cards_list = _.sortByOrder(cards_list, ['price'], ['desc']);
				gallery_count = 0; 
				gallery_max_count = 24;
				$('.loading-container').hide();
				if(cards_list.length == 0) {
					var no_data_disp = '<center><div><img src="'+base_url+'/assets/images/logo7.png" style="width:50%"></div><div class="text-center" style="font-size:20px; font-weight:bold; margin-bottom: 15px">No cards available</div><a href="#" style="border: 1px solid #00b8f9;background: #05a9e2; color: white; padding: 10px 25px; text-decoration:none;" data-toggle="modal" data-target="#myModal">Try other categories</a></center>';
					$('#myDiv').html(no_data_disp);
					$('#card-list-container').html('');
					$('#card-list-container').css('display', 'none');
					$('#card-gallery-container').css('display', 'none');

					$('.load_gallery_view').hide();
					return;
				}

				$('#card-list-container').css('display', '');
				$('#card-gallery-container').css('display', '');
				load_datatable(cards_list);
				load_galleryview(cards_list);
				if(show_stock) $('.show_stock').prop('checked', show_stock);
			}
		});
	}

	$(document).on('click','.card-image a', function(e){
	    e.preventDefault();
	    var imgLink = $(this).children('img').attr('src');
	    $('.mask').html('<div class="img-box"><img src="'+ imgLink +'"><a class="close">&times;</a>');
	    $('.mask').addClass('is-visible fadein').on('animationend', function(){
	      $(this).removeClass('fadein is-visible').addClass('is-visible');
	    });
	    $('.close').on('click', function(){
	      $(this).parents('.mask').addClass('fadeout').on('animationend', function(){
	        $(this).removeClass('fadeout is-visible')
	      });
	    });
	    $('.is-visible').on('click', function(){
	      $(this).addClass('fadeout').on('animationend', function(){
	        $(this).removeClass('fadeout is-visible')
	      });
	    });
	});

	$("a[href='#card-list-container']").on("click",function(){
		if(cards_list.length > 0){
		 $('html, body').animate({
	        scrollTop: $("#myDiv").offset().top
	    }, 1000);
		}
	})
});