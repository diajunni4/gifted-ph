$(document).ready(function(){

	

	$('.subscribe_form').submit(function(){

		

		$.post( $(this).attr('action'), $(this).serialize(), function(data){

			if(data.error)

			{

				$('.newsletter_result').append('<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>'+data.error+'</div>');

			}

			else

			{

				$('.subscribe_form input').val('');

				$('.newsletter_result').append('<div class="alert alert-info"><a class="close" data-dismiss="alert">×</a>'+data.message+'</div>');

			}

			

			setTimeout(function(){

				$('.newsletter_result').html('');

			},5000);

			

			$('.subscribe_button').removeClass('disabled');

		}, 'json');

				

		return false;

	});

	

	

	/***** START HOMEPAGE *****/	

	$('.btn-gift-option, .send-gift-nav').click(function(e){	

		e.preventDefault();

		$('#gift-option-popup').modal('show');		

	});

	

	$('.btn-redeem-option').click(function(){

		location.assign(site_url + "redeem-gift");			

	});

	

	$('.btn-how-it-works').click(function(){

		location.assign(site_url + "secure/register");			

	});

	

	$('#option-merchant').click(function(){

		location.assign(site_url + "gift-suggestions");		

	});

	

	$('#option-universal').click(function(){

		location.assign(site_url + "gifted-money-check");		

	});

	

    $(".help-pop").tooltip({html : true}); 

	

  

	$('#search_open').click(function()

	{

		if( $('#search_term').val() == '')

		{

			$('.search_nav input').toggle('fast').focus();

		}

		else

		{

			$('#nav_search_form').submit();

		}

	}); 

    

    

    

    /***** END HOMEPAGE *****/	

	

	

	

	

	

	/***** START GREETING CARD *****/	

	// will be used later to set selected radio

	//var is_checked = $("input:radio[name='card_type']").is(":checked")) 

    		

	//1st step we need them to select a greeting card design

	$('.card-rad').click(function(){

	

		var main_card = $(this).attr('data-card');

		$('#main-card').hide();

		$('#main-card').attr('src', main_card);	

		$('#row-recipient, #row-preview, #dynamic-details, #row-action').removeClass('hide-me').fadeIn('slow');	

		

		$('#main-card').removeClass('hide-me').fadeIn();		

	});



	$('#subcategory').change(function(){

	

		var subcategory = $(this).val();

		$('#subcategory-detail').html(subcategory);

	});

	

	$('.name-box').keyup(function(){

	

		var salute = new Array();		

		

		

		$.each($('.name-box'), function(){		   

		   

			if( $(this).val() != '')

			{

			  var sort     = $(this).attr('data-sort');				  

		      salute[sort] = $(this).val();

			}

		   

		});		

		

		if(salute.length > 0)

		{

			$('#salutation-preview').removeClass('hide-me');

			$('#dedication-preview').removeClass('hide-me');

			$('#salutation-preview').html('Dear ' + salute.join(' ') + ',').fadeIn();		

		}

		else

		{

			$('#salutation-preview').addClass('hide-me');

			$('#dedication-preview').addClass('hide-me');

		}		

		 

	});

		

	$('#message').keyup(function(event){	

	

	    var msg = $(this).val();

			    	    

	    if(msg != '')

	    {

	     		    

	    	//lets check if message is still within the allowed area

			var max_height  = 129 ;	    

		   

		 	

		    $('#message-content').html(msg.replace(new RegExp('\n','g'), '<br />'));

	    	

	    	// lets check again if it exceeds the line, because line breaks can happen after render

	    	curr_height = $('#message-content').height();

	    	

	    	console.log(curr_height);

	    	if(curr_height > max_height)

		    {		    	

		    	msg = msg.substring(0,msg.length - 1);

		    	$(this).val(msg);

		    	$('#message-content').html(msg.replace(new RegExp('\n','g'), '<br />'));		    			    	

		    	alert('Your message exceeds the maximum allowed space!');    	

		    }	    	

	    	

	    		

	     	$('#message-content').removeClass('hide-me');

	     	

	     	$('#dedication-preview').removeClass('hide-me');

	     	

	     	

	    }

	    else

	    {

	    	$('#message-content').addClass('hide-me');

	    	$('#dedication-preview').addClass('hide-me');

	    }

	    

	  

	    

	});

	

	

	$('.btn-submit-greeting').click(function(){

		

		var errors = '';

		

		$.each($('.fld-required'), function(){		   

		   

			if( $(this).val() == '')

			{

			   var f_name = $(this).attr('data-label');

			   errors += f_name + " is required!\n";

			   

			   $(this).addClass('form-error');

			   

			}

		   	else

		   	{

		   		// lets check if type is email		   		

		   		if( $(this).attr('name') == 'email')

		   		{

		   			if( validateEmail($(this).val()) )

		   			{		   				

		   				$(this).removeClass('form-error');		   				

		   			}

		   			else

		   			{

		   				errors += "A valid email is required!\n";

		   				$(this).addClass('form-error');		   				

		   			}

		   		}

		   		else

		   		{

		   			$(this).removeClass('form-error');

		   		}		   		

		   		

		   	}			

			

		});

		

		if(errors != '')

		{

			alert("Please correct the errors below\n\n" + errors);

			$(window).scrollTop($('#row-recipient').position().top);			

		}

		else

		{

			$('#frm-greeting-card').submit();

		}		

		

	});

	

	function validateEmail(email){ 

      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; 

      return re.test(email); 

	}

	

	/***** END GREETING CARD *****/	

	

	

	

	$('.btn-logpop').click(function(){

		$('#redirect').val('cart/greeting_card');		

	});

	

	$('.btn-login-pop').click(function(){

		$('#login-popup').modal('show');	

	});

	

	

	/**** start cart related  ****/

	$('.btn-wishlist').click(function(e){

		var pid = $(this).attr('data-id');

		

		$.post(site_url + "ajax/wishlist", { pid : pid },function(response){

	      var obj_response = $.parseJSON(response);

	      

	      if(obj_response.exist == 0)

	      {

	      	alert('Product was successfully added to your wishlist');

	      }

	      else

	      {

	      	alert('Product is already on your wishlist');

	      }	      

	      

	    });

		

	});

	

	$('.btn-wishlist-delete').click(function(e){

		

	    

	    e.preventDefault();

		var pid = $(this).attr('data-id');

		

		var ans = confirm('are you sure you want to remove this from your wishlist?');



		if(ans == true)

		{			

		    //$(this).closest('tr').fadeOut('slow'); 			

			$.post(site_url + "ajax/wishlist_remove", { pid : pid },function(data){

		      if(data == 1)

		      {

		      	

		        $('#view-wishlist #wrow_'+pid).fadeOut(600).remove();

		        alert('The item was successfully removed from your wishlist');

		        		       		      

		        var rowCount = $('#view-wishlist >tbody >tr').length;

		        if(rowCount <=0)

		        {

		      	   $('#view-wishlist >tbody').append('<tr><td colspan="4" align="center"><span class="red"><b>No Items in your wishlist</b></span></td></tr>').fadeIn();

		        }

		      }	     

		    });

	    }   

			

	});

	/**** end cart related  ****/

	

	/**** dashboard related ****/	

	var l_hash = location.hash;

	

	if(l_hash == '')

	{

	

		$('#my-account-tab a[data-toggle="tab"]').on('shown.bs.tab', function () {       

	        localStorage.setItem('my_account_lastTab', $(this).attr('href'));

	    });

		

	    var my_account_lastTab = localStorage.getItem('my_account_lastTab');

	    if (my_account_lastTab) {

	        $('a[href=' + my_account_lastTab + ']').tab('show');

	    }

	    else

	    {

	        // Set the first tab if cookie do not exist

	        $('a[data-toggle="tab"]:first').tab('show');

	    }

	 }

	 else

	 {

	 	 $('a[href=' + l_hash + ']').tab('show');

	 	 $(window).scrollTop(0);

	 }     

	  	

	 /*

	 $('.modal').on('show.bs.modal', function() {

	    $(this).find('.modal-dialog').css({

	        'margin-top': function () {

	            return (($(window).outerHeight() / 2) - ($(this).outerHeight() / 2));

	        }

	    });

 	 });

	 */	

	 

	 $('.accept-alphanum').bind('keypress', function (event) {

	    var regex = new RegExp("^[a-zA-Z0-9\b]+$");

	    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

	    if (!regex.test(key)) {

	       event.preventDefault();

	       return false;

	    }

	 });

	 

	 $('.accept-num').bind('keypress', function (event) {

	    var regex = new RegExp("^[0-9\b]+$");

	    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

	    if (!regex.test(key)) {

	       event.preventDefault();

	       return false;

	    }

	});
	
		
	//table overflow swipe
	var cb_table = 0;
	$('.table-responsive').each(function(){

		$(this).wrap('<div id="table_overflow'+cb_table+'" class="table_overflow" style="margin:0 -15px;padding:0 15px"><div></div></div>');

		if( $('#table_overflow'+cb_table+' table').width()  > $('#table_overflow'+cb_table).width() )
		{ 
			$(this).parent().prepend('<div class="swipe_alert"></div>');

			var $this = '';
			var $this = $('#table_overflow'+cb_table+' .swipe_alert');
			
			 $this.touchwipe({		 
				min_move_x: 10,
				min_move_y: 0,
				wipeLeft: function() {  $this.fadeOut(); },
				wipeRight: function() { $this.fadeOut(); },
				preventDefaultEvents: false 
			}); 
		}
		cb_table++;
	});
	$(this).on('mousedown mouseenter', '.swipe_alert', function(){
		$(this).remove();
	});	 



});







