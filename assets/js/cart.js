$(document).ready(function(){
	check_purrcoins();
	$(document).on('click', '.checkout-cart, .tmp_btn_checkout', function(){

		$('.loading_screen').show();
		$.ajax({
      type: "POST",
      url: base_url + "api/check_cart",
      data: {
        id: ''
      },
      dataType:"json",
      success: function(data){
    	 if(data.length > 0) {
					$('.loading_screen').hide();
          $('#outOfStockModal').modal('show');

          $('#outOfStockModal .table tbody').html('');
          data.forEach(function(el){
            var append_html = '<tr>'
              + '<td style="color:red">' + el.card_name + '</td>'
              + '<td>' + el.card.set_name + '</td>'
              + '<td>' + el.quantity + '</td>'
              + '<td>' + el.card.stock + '</td>'
              + '</tr>';
            $('#outOfStockModal .table tbody').append(append_html);
          });
          return;
        }

				var credits = $('input[name="purrcoins"]').val();
				var coupon = $('input[name="coupon_code"]').val();

				$.ajax({
					type: 'POST',
					url: base_url + "api/apply_checkout",
					data: {
						credits: credits,
						coupon: coupon
					}, success: function(data) {
						data = JSON.parse(data);
						if(data.status == 'Success'){
						 window.location.href = base_url + 'checkout';
						 return;
						} else {
							toastr.error(data.message);
						}
						$('.loading_screen').hide();
					}
				});
			}
		});

	});

	function check_purrcoins(){
		var points_used = parseFloat($('input[name="purrcoins"]').val());

		if(points_used <= credits) {
			var new_rebate = rebate_points - points_used;
			new_rebate = new_rebate < 0 ? 0 : new_rebate;
			$('.rebate_points').html(parseFloat(new_rebate).toFixed(2));
		}
	}

	$(document).on('keyup', 'input[name="purrcoins"]', function(){
		var points_used = parseFloat($('input[name="purrcoins"]').val());
		$(this).removeClass('error_field');

		if(points_used <= credits) {
			var new_rebate = rebate_points - points_used;
			new_rebate = new_rebate < 0 ? 0 : new_rebate;
			$('.rebate_points').html(parseFloat(new_rebate).toFixed(2));
		} else {
			$(this).addClass('error_field');
			$('.rebate_points').html(rebate_points);
		}

		if($('input[name="purrcoins"]').val() == '') $(this).removeClass('error_field');
	});

	$(document).on('click', '.remove_item', function(){
		var id = $(this).data('id');

		$.ajax({
			type: "POST",
			url: base_url + "api/remove_item_cart",
			data: {
				id: id
			},
			success: function(data) {
				var data = JSON.parse(data);
				cart_list = data.cart_list;
				var sub_total = 0;
				var rebate_points = 0;

				for(var count = 0; count < cart_list.length; count++) {
					var price = cart_list[count]['card']['regular_price'] ? cart_list[count]['card']['regular_price'] : cart_list[count]['card']['foil_price'];
					sub_total += (cart_list[count]['quantity'] * price);
					if(cart_list[count]['card']['category'] == 'single cards'){
						rebate_points += (cart_list[count]['quantity'] * price) * single_card_rebate;
					} else {
						rebate_points += (cart_list[count]['quantity'] * price) * other_card_rebate;
					}
				}
				rebate_points = rebate_points.toFixed(2);

				$('.order_subtotal').html(sub_total);
				$('.rebate_points').html(rebate_points);

				$('tr[data-id="'+id+'"]').remove();
				if(cart_list.length <= 0) $('.cart-container').html('Your cart is currently empty');
			}
		});
	});

	function check_add_to_cart(id, value){
		var add_cart_button = $('.add_to_cart[data-id="'+ id +'"]');
		var default_text = add_cart_button.data('default-text');
		if(value == 0){
			add_cart_button.closest('tr').remove();
		} else {
			add_cart_button.addClass('btn-default');
			add_cart_button.removeClass('btn-primary');
			add_cart_button.html(value + ' in Cart');
		}
	}

	$(document).on('click', '.cart-quantity-content-container button', function(){
		$('[data-original-title]').popover('hide');
		var value = $(this).data('value');
		var add_cart_container = $(this).closest('.cart-quantity-content-container');
		var index = -1;
		var id = add_cart_container.data('id');
		var add_cart_button = $('[data-target="' + add_cart_container.data('container') + '"]');
		var sub_total = 0;
		rebate_points = 0;

		$.ajax({
			type: 'POST',
			url: base_url + 'api/add_item_to_cart',
			data: {
				card_id: id,
				quantity: value
			},
			success: function(data) {
				$('.loading_screen').hide();
				var output = JSON.parse(data);
					
				if(output.status == 'success'){
					cart_list = output.cart_list;
					check_add_to_cart(id, value);
		
					if(value == 0){
						cart_list.splice(index, 1);
					}

					$('.add-cart-wrapper').removeClass('active');

					for(var count = 0; count < cart_list.length; count++) {
						var price = cart_list[count]['card']['regular_price'] ? cart_list[count]['card']['regular_price'] : cart_list[count]['card']['foil_price'];
						sub_total += (cart_list[count]['quantity'] * price);
						if(cart_list[count]['card']['category'] == 'single cards'){
							rebate_points += (cart_list[count]['quantity'] * price) * single_card_rebate;
						} else {
							rebate_points += (cart_list[count]['quantity'] * price) * other_card_rebate;
						}
						var card_total_price = (cart_list[count]['quantity'] * price);
						$('tr[data-id="'+cart_list[count]['card']['id']+'"] .card_total_amount').html(card_total_price);
					}
					rebate_points = rebate_points.toFixed(2);

					$('.order_subtotal').html(sub_total);
					$('.rebate_points').html(rebate_points);
				} else {
					toastr.error(output.message);
				}				
			}
		})
	
	});
});