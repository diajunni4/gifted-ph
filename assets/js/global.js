function getCookie(name) {
	var value = "; " + document.cookie;
	var parts = value.split("; " + name + "=");
	if (parts.length == 2) return parts.pop().split(";").shift();
}
$(window).scroll(function () {
	var scroll = $(window).scrollTop();
	//>=, not <=
	if (scroll < 10) {
		//clearHeader, not clearheader - caps H
		$(".navbar").removeClass("main_navbar");
	} else {
		$(".navbar").addClass("main_navbar");
	}
}); //missing );
function addProduct(target) {
	// this is just a product placeholder
	var productAdded = '<li class="cd-cart__product"><div class="cd-cart__image"><a href="#0"><img src="assets/img/product-preview.png" alt="placeholder"></a></div><div class="cd-cart__details"><h3 class="truncate"><a href="#0">Product Name</a></h3><span class="cd-cart__price">$25.99</span><div class="cd-cart__actions"><a href="#0" class="cd-cart__delete-item">Delete</a><div class="cd-cart__quantity"><label for="cd-product-' + productId + '">Qty</label><span class="cd-cart__select"><select class="reset" id="cd-product-' + productId + '" name="quantity"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option></select><svg class="icon" viewBox="0 0 12 12"><polyline fill="none" stroke="currentColor" points="2,4 6,8 10,4 "/></svg></span></div></div></div></li>';
	cartList.insertAdjacentHTML('beforeend', productAdded);
};

function checkImage(imageSrc, good, bad) {
	var img = new Image();
	img.onload = good;
	img.onerror = bad;
	img.src = imageSrc;
}

$(document).on('click', ".qty_btn", function (e) {
	console.log($(this).html());
	$id = $(this).attr("data-test");
});


function trimString(s) {
	var l = 0,
		r = s.length - 1;
	while (l < s.length && s[l] == ' ') l++;
	while (r > l && s[r] == ' ') r -= 1;
	return s.substring(l, r + 1);
}

function compareObjects(o1, o2) {
	var k = '';
	for (k in o1)
		if (o1[k] != o2[k]) return false;
	for (k in o2)
		if (o1[k] != o2[k]) return false;
	return true;
}

function itemExists(haystack, needle) {
	for (var i = 0; i < haystack.length; i++)
		if (compareObjects(haystack[i], needle)) return true;
	return false;
}


function notify2(header, mes, mes_type) {
	return swal({
		title: header,
		html: mes,
    type: mes_type,
    confirmButtonColor: '#bfa255',
	});
}

function loading() {
	swal({
		title: 'Please wait...',
		allowOutsideClick: false,
		allowEscapeKey: false
	});
	swal.showLoading();
}

function close_loading() {
	swal.close();
}

function confirm_alert(title, text, type) {
	return swal({
		title: title,
		text: text,
		type: type,
		showCancelButton: true,
		confirmButtonColor: '#bfa255',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	});
}






// $('html').on('click', function(e) {
// 	if (typeof $(e.target).data('original-title') == 'undefined' &&
// 		!$(e.target).parents().is('.popover.in')) {
// 	  $('[data-original-title]').popover('hide');
// 	} else {
// 		$('[data-original-title]:not([data-id="'+ $(e.target).data('id') +'"])').popover('hide');
// 	}
//  });
