/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
//window._ = require('lodash');

import Vue from 'vue';
//import App from './App.vue'
import VueAxios from 'vue-axios';
import axios from 'axios';
//import CartComponent from './components/CartComponent.vue'
import Products from './components/Products.vue';
import ViewProduct from './components/ViewProduct.vue';
import Checkout from './components/Checkout.vue';
import Orders from './components/Orders.vue';
import Contact from './components/Contact.vue';
import Validate from './components/Validate.vue';
import Redeem from './components/Redeem.vue';
import Footer from './components/Footer.vue';
import Vuelidate from 'vuelidate'
import 'core-js/fn/string/includes';
import "core-js/es7/array";
import "core-js/es7/object";
import { DataTables, DataTablesServer } from 'vue-data-tables';
//import VueDataTables from 'vue-data-tables';
import ElementUI from 'element-ui';
import './element-variables.scss';
import 'core-js/fn/string/includes';
import "core-js/es7/array";
import "core-js/es7/object";
import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
import ReadMore from 'vue-read-more';
Vue.use(ElementUI);
// import DataTables and DataTablesServer separately
Vue.use(DataTables);
Vue.use(DataTablesServer);
Vue.use(Vuelidate)

// import DataTables and DataTablesServer together

//Vue.use(VueDataTables);
 Vue.use(ReadMore);
 Vue.component('main-footer', Footer);
 Vue.component('products', Products);
Vue.component('checkout', Checkout);
Vue.component('orders', Orders);
Vue.component('redeem', Redeem);
Vue.component('validate', Validate);
Vue.component('contact', Contact);
Vue.component('view-product', ViewProduct);

window.axios = axios;

Vue.use(VueAxios, axios);
//Vue.component('sabai-cart', CartComponent);

const app = new Vue({}).$mount('#app');
