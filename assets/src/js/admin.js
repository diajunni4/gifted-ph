//Vue
import Vue from 'vue'
//router
import VueRouter from 'vue-router'
//components
import Admin_Dashboard from './Admin_Dashboard.vue'
import Suggestions from 'v-suggestions'
//import 'v-suggestions/dist/v-suggestions.css' // you can import the stylesheets also (optional)

window.Vue = Vue;
//axios
import VueAxios from 'vue-axios'
import axios from 'axios'
import Dashboard from './admin/Dashboard.vue'

import CKEditor from '@ckeditor/ckeditor5-vue';



//Users
import View_Users from './admin/Users/View.vue'
import Add_Users from './admin/Users/Add.vue'
import Edit_Users from './admin/Users/Edit.vue'

//Wallet
//import View_Orders from './admin/Orders/View.vue'
import List_Wallet from './admin/Wallet/List.vue'

//Orders
import View_Orders from './admin/Orders/View.vue'
import List_Orders from './admin/Orders/List.vue'

//Brands
import View_Brands from './admin/Brands/List.vue'

//Vouchers
import View_Vouchers from './admin/Vouchers/List.vue'

//Audit trails
import View_Audit from './admin/Audit/View.vue'

//Company trails
import List_Company from './admin/Company/List.vue'
import Edit_Company  from './admin/Company/Edit.vue'
import Add_Company  from './admin/Company/Add.vue'

//Categories
import View_Categories from './admin/Categories/View.vue'
import Add_Categories from './admin/Categories/Add.vue'
import Edit_Categories from './admin/Categories/Edit.vue'

//Loyalty Program
import View_Loyalty from './admin/Loyalty/View.vue'
import Edit_Loyalty from './admin/Loyalty/Edit.vue'

//Producs
import View_Products from './admin/Products/View.vue'
import Edit_Products from './admin/Products/Edit.vue'
import Add_Products from './admin/Products/Add.vue'

//Coupons 
import View_Coupons from './admin/Coupons/View.vue'
import Edit_Coupons from './admin/Coupons/Edit.vue'
import Add_Coupons from './admin/Coupons/Add.vue'

//Feedbacks
import View_Feedbacks from './admin/Feedbacks/View.vue'
import Edit_Feedbacks from './admin/Feedbacks/Edit.vue'

//Reports
import View_Reports from './admin/Reports/View.vue'


import Navbar from './admin/Header.vue'
import Footer from './admin/Footer.vue'
import Sidebar from './admin/Sidebar.vue'
import VueCkeditor from 'vue-ckeditor2';

import { DataTables, DataTablesServer } from 'vue-data-tables';
import VueDataTables from 'vue-data-tables';
import ElementUI from 'element-ui';
import './element-variables.scss';
import 'core-js/fn/string/includes';
import "core-js/es7/array";
import "core-js/es7/object";
import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
Vue.use(ElementUI);
// import DataTables and DataTablesServer separately
Vue.use(DataTables);
Vue.use(DataTablesServer);
// import DataTables and DataTablesServer together

Vue.use(VueDataTables);
Vue.use( CKEditor );

Vue.use(VueCkeditor);

window.axios = axios
Vue.use(VueAxios, axios)
Vue.use(VueRouter);
Vue.use(Suggestions);
Vue.component('vue-ckeditor',VueCkeditor);
Vue.component('admin_header', Navbar);
Vue.component('admin_footer', Footer);
Vue.component('admin_sidebar', Sidebar);
/*export const resumeBus = new Vue();*/

const routes = [
	{
		name: '/',
		path: '/',
		component: Dashboard
	},
	{
		name: '/view_users',
		path: '/view_users',
		component: View_Users
	},
	{
		name: '/add_users',
		path: '/add_users',
		component: Add_Users
	},
	,
	{
		name: '/view_company_clients',
		path: '/view_company_clients',
		component: List_Company
	},
	{
		name: '/edit_company',
		path: '/edit_company/:id',
		component: Edit_Company
	},
	{
		name: '/add_company',
		path: '/add_company',
		component: Add_Company
	},
	{
		name: '/edit_user',
		path: '/edit_user/:id',
		component: Edit_Users
	},
	{
		name: '/view_wallet',
		path: '/view_wallet',
		component: List_Wallet
	},
	{
		name: '/view_orders',
		path: '/view_orders',
		component: List_Orders
	},
	{
		name: '/view_orders/:id',
		path: '/view_orders/:id',
		component: View_Orders
	},
	{
		name: '/view_categories',
		path: '/view_categories',
		component: View_Categories
	},
	{
		name: '/view_brands',
		path: '/view_brands',
		component: View_Brands
	},
	{
		name: '/add_category',
		path: '/add_category',
		component: Add_Categories
	},
	{
		name: '/edit_category',
		path: '/edit_category/:id',
		component: Edit_Categories
	},
	{
		name: '/add_products',
		path: '/add_products',
		component: Add_Products
	},
	{
		name: '/edit_loyalty',
		path: '/edit_loyalty/:id',
		component: Edit_Loyalty
	},
	{
		name: '/view_feedbacks',
		path: '/view_feedbacks',
		component: View_Feedbacks
	},
	{
		name: '/edit_product',
		path: '/edit_product/:id',
		component: Edit_Products
	},
	{
		name: '/view_products',
		path: '/view_products',
		component: View_Products
	},
	{
		name: '/view_reports',
		path: '/view_reports',
		component: View_Reports
	},
	{
		name: '/view_vouchers',
		path: '/view_vouchers',
		component: View_Vouchers
	},
	{
		name: '/view_audit',
		path: '/view_audit',
		component: View_Audit
	},
	{
		name: '/view_coupons',
		path: '/view_coupons',
		component: View_Coupons
	},
	{
		name: '/edit_coupon',
		path: '/edit_coupon/:id',
		component: Edit_Coupons,
	},
	{
		name: '/add_coupon',
		path: '/add_coupon/',
		component: Add_Coupons,
	},
	/*,
	{
		name: '/add_work_history',
		path: '/add_work_history',
		component: Add_Work_History
	},
	{
		name: '/view_work_history',
		path: '/view_work_history',
		component: View_Work_History
	},
	{
		name: '/edit_work_history',
		path: '/edit_work_history/:id',
		component: Edit_Work_History,
	},
	{
		name: '/add_prof_summary',
		path: '/add_prof_summary',
		component: Add_Prof_Summary
	},
	{
		name: '/view_prof_summary',
		path: '/view_prof_summary',
		component: View_Prof_Summary
	},
	{
		name: '/edit_prof_summary',
		path: '/edit_prof_summary/:id',
		component: Edit_Prof_Summary,
	}*/
];
const router = new VueRouter({
	routes,
	linkExactActiveClass: "active" // active class for *exact* links.,
	//,mode: 'history'
});


new Vue({
    el: '#app',
    router,
    render: app => app(Admin_Dashboard)
});