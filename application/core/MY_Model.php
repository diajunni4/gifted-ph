<?php
class MY_Model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
	}


	public function fetch($table, $where = "", $limit = "", $offset = "", $order = "")
	{
		if (!empty($where)) {
			$this->db->where($where);
		}
		if (!empty($limit)) {
			if (!empty($offset)) {
				$this->db->limit($limit, $offset);
			} else {
				$this->db->limit($limit);
			}
		}
		if (!empty($order)) {
			$this->db->order_by($order);
		}

		$query = $this->db->get($table);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return FALSE;
		}
	}

	public function fetch_data($table, $where = "", $limit = "", $offset = "", $order = "", $group = "")
	{
		if (!empty($where)) {
			$this->db->where($where);
		}
		if (!empty($limit)) {
			if (!empty($offset)) {
				$this->db->limit($limit, $offset);
			} else {
				$this->db->limit($limit);
			}
		}
		if (!empty($order)) {
			$this->db->order_by($order);
		}
		if (!empty($group)) {
			$this->db->group_by($group);
		}

		$query = $this->db->get($table);
		return $query->result();
	}


	public function fetch_tag($tag, $table, $where = "", $limit = "", $offset = "", $order = "")
	{
		if (!empty($where)) {
			$this->db->where($where);
		}
		if (!empty($limit)) {
			if (!empty($offset)) {
				$this->db->limit($limit, $offset);
			} else {
				$this->db->limit($limit);
			}
		}
		if (!empty($order)) {
			$this->db->order_by($order);
		}
		$this->db->select($tag);
		$this->db->from($table);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return FALSE;
		}
	}

	public function fetch_tag_array($tag, $table, $where = "", $limit = "", $offset = "", $order = "")
	{
		if (!empty($where)) {
			$this->db->where($where);
		}
		if (!empty($limit)) {
			if (!empty($offset)) {
				$this->db->limit($limit, $offset);
			} else {
				$this->db->limit($limit);
			}
		}
		if (!empty($order)) {
			$this->db->order_by($order);
		}
		$this->db->select($tag);
		$this->db->from($table);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return FALSE;
		}
	}


	public function fetch_tag_row($tag, $table, $where = "", $limit = "", $offset = "", $order = "")
	{
		if (!empty($where)) {
			$this->db->where($where);
		}
		if (!empty($limit)) {
			if (!empty($offset)) {
				$this->db->limit($limit, $offset);
			} else {
				$this->db->limit($limit);
			}
		}
		if (!empty($order)) {
			$this->db->order_by($order);
		}
		$this->db->select($tag);
		$this->db->from($table);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return FALSE;
		}
	}

	public function fetch_tag_row_like($tag, $table, $where = "", $limit = "", $offset = "", $order = "")
	{
		if (!empty($where)) {
			$this->db->like($where);
		}
		if (!empty($limit)) {
			if (!empty($offset)) {
				$this->db->limit($limit, $offset);
			} else {
				$this->db->limit($limit);
			}
		}
		if (!empty($order)) {
			$this->db->order_by($order);
		}
		$this->db->select($tag);
		$this->db->from($table);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return FALSE;
		}
	}
	public function fetch_tag2($tag, $table, $where = "")
	{
		$query = $this->db->select($tag)->from($table)->where($where)->get()->row();
		return $query->$tag;
	}

	public function insert($table, $data)
	{
		date_default_timezone_set("Asia/Manila");
		$t=time();
		$data["created_at"] = date("Y-m-d h:m:s",$t);
		$data["updated_at"] = date("Y-m-d h:m:s",$t);
		$result = $this->db->insert($table, $data);
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function insert_batch($table, $data)
	{
		
		$result = $this->db->insert_batch($table, $data);
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}


	public function update($table, $data, $where = "")
	{
		$t=time();
		$data["updated_at"] = date("Y-m-d h:m:s",$t);
		if ($where != "") {
			$this->db->where($where);
		}
		$result = $this->db->update($table, $data);
		if ($this->db->affected_rows()) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function delete($table, $where = "")
	{
		if ($where != "") {
			$this->db->where($where);
		}
		$result = $this->db->delete($table);
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function check_exist($table, $filter)
	{
		return $this->db->select('id')->from($table)->where($filter)->get()->num_rows();
	}

	public function count_rows($table, $where = "")
	{
		if ($where != "") {
			$this->db->where($where);
		}
		return $this->db->select("id")->from($table)->get()->num_rows();
	}
}
