<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Global_model');

		$data['data']['categories'] = $this->Global_model->fetch('menu');
	}
}