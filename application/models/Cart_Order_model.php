<?php
class Cart_Order_model extends MY_Model{

	public function __construct(){
		parent::__construct();
	}

	public function insert_data($data) {
		$t=time();
		$data["created_at"] = date("Y-m-d h:m:s",$t);
		$data["updated_at"] = date("Y-m-d h:m:s",$t);
		$result = $this->db->insert('orders',$data);
		if ($result) {
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}

	public function update_data($id, $data) {
		$t=time();
		$data["updated_at"] = date("Y-m-d h:m:s",$t);
		$this->db->where('id', $id);
		$result = $this->db->update('orders', $data);
		if ($result) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function get($order_number = null) {
		if($order_number == null) $query = $this->db->get('orders');
		else $query = $this->db->get_where( 'orders', array('order_number' => $order_number) );

		return $query->result_array();
	}

}