<?php
class Product_model extends MY_Model{

	public function __construct(){
		parent::__construct();
   }
   
   public function get_products_per_restaurant($filter) {
      $query = $this->db->select('a.*,b.drinks_code,b.drinks,b.drinks_price,b.default_softdrink ')
      ->from('products as a')
      ->join('drinks_inclusion b','b.drinks_code = a.drinks_inclusion','left')
      ->where($filter)
      ->get()
      ->result();
      return $query;
   }

   public function get_restaurant_products_best_seller($filter) {
      $query = $this->db->select('a.*,b.drinks_code,b.drinks,b.drinks_price,b.default_softdrink ')
      ->from('products as a')
      ->join('drinks_inclusion b','b.drinks_code = a.drinks_inclusion','left')
      ->where($filter)
      ->order_by('tags','desc')
      ->get()
      ->result();
      return $query;
   }

}