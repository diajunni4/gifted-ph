<?php
class Cart_model extends MY_Model{

	public function __construct(){
		parent::__construct();
   }
   
   public function get_order($order_key,$email){
     $query = $this->db->select('*')->from('tmp_orders');

     if($order_key !='' && $email != '') {
      $this->db->where('order_key',$order_key);
      $this->db->or_where("email",$email);
      
     } elseif($email != '') {
      $this->db->where('email',$email);
     } else {
      $this->db->where('order_key',$order_key);
     }
     $this->db->order_by("id","desc");
     return $query->get()->row();
   }

   public function get_view_order($order_key,$email){
    $query = $this->db->select('*')->from('orders');
     $this->db->where('order_key',$order_key);
     $this->db->where("email",$email);
     $this->db->order_by("id","desc");
     return $query->get()->row();
   }

   public function update_promo($promo_code) {
    $query = "update promo_codes set promo_codes.limit = promo_codes.limit - 1 where promo_codes.code = '$promo_code'";
    return $this->db->query($query);
   }

   public function get_discount($email) {
     $query = $this->db->select('a.company,b.discounts')->from('users a')
     ->join('company_clients b','b.id = a.company','left')
     ->where('a.email',$email)->get();

     return $query->row();
   }
}