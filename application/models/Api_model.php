<?php
class Api_model extends MY_Model{

	public function __construct(){
		parent::__construct();
	}

   public function get_voucher_details ($voucher_code) {
      $query = $this->db->select('a.voucher_code,b.*,c.firstname,c.lastname,d.name as brand_name,d.how_to_redeem,d.about,d.image,
      e.name as product_name,e.terms_and_condition')->from('vouchers a')
      ->join('orders b','b.order_number = a.order_number','left')
      ->join('users c','c.email = b.email','left')
      ->join('brands d', 'd.brand_id = b.brand_id','left')
      ->join('products e','e.id =   b.product_id','left')
      ->where('voucher_code',$voucher_code)->get();
      return $query->row();
   }

   public function get_order_details($order_number) {
      $query = $this->db->select("a.*,b.image,b.image,b.about,b.how_to_redeem,b.name as brand_name,c.firstname,c.lastname,c.email,c.contact_number, e.name as product_name,
      b.about,e.excerpt,e.description as product_description, e.terms_and_condition")
      ->from('orders a')
      ->join('brands b', 'b.brand_id = a.brand_id')
      ->join('users c','c.email = a.email','left')
      ->join('products e','e.id =   a.product_id','left')
      ->where('a.order_number',$order_number)->get();
      return $query->row();
   }

   public function get_product_details($filter) {
      $query = $this->db->select("a.*,b.name")
      ->from('products a')
      ->join('brands b', 'b.brand_id = a.brand_id')
      ->where($filter)->get();
      return $query->row();
   }

   public function get_orders() {
      $query = $this->db->select("a.*,b.image,b.image,b.about,b.how_to_redeem,b.name as brand_name,c.firstname,c.lastname,c.email,c.contact_number, e.name as product_name,
      b.about,e.excerpt,e.description as product_description, e.terms_and_condition")
      ->from('orders a')
      ->join('brands b', 'b.brand_id = a.brand_id')
      ->join('users c','c.email = a.email','left')
      ->join('products e','e.id =   a.product_id','left')
      ->get();
      return $query->result();
   }


   
   public function get_brand_subcategory($brand_id) {
      $query = $this->db->select('b.tags')->from('brands a')
      ->join('categories b',' b.name = a.category','left')
      ->where('a.brand_id',$brand_id)->get();

      return $query->row();
   }

   public function search_brand($brand){
      $query = $this->db->select('name,slug')->from('brands')->like('name',$brand,'both')->get();
      return $query->result();
   }
}