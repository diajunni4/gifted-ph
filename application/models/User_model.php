<?php
class User_model extends MY_Model{

	public function __construct(){
		parent::__construct();
	}

	public function get_all_users() {
		$query = $this->db->select('a.*')->from('users a')->limit(5000)->order_by('created_at desc')
		//->join('(select email, sum(total) as total from orders where status = "Completed" and mode = "live" group by email) x','x.email = a.email','left')
		->get();
		return $query->result();
	}

	public function get_user_info($filter) {
		$query = $this->db->select("a.*,b.credits,SUM(c.price) as total_price_spent")->from("users a")
		->join("user_credit b","b.email = a.email","left")
		->join('cart_order c','c.email = a.email','left')
		->where($filter)
		->where('c.admin_status',3)
		->get();
		return $query->result();
	}
}