<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class users_api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		if(is_logged_admin() == 0){
			redirect(base_url());
		}
	}
	
	public function get_users() {
		$data['users'] = $this->User_model->get_all_users('users');
		echo json_encode($data);
	}


	public function add()
	{
		$response = ["message"=>"success"];
		$user_data = (array) post('user_data');
		$user_data['password'] = hash_password($user_data['password']);
		$this->User_model->insert('users',$user_data);
		echo json_encode($response);
	}

	public function get_user(){
		$email = clean_data(rawurldecode(get('q')));
		$filter = ["email"=>$email];
		$data["user"] = $this->User_model->fetch_tag_row('*','users',$filter);
		$data['companies'] = $this->User_model->fetch_tag('id,company_name','company_clients');
		echo json_encode($data);
	}

	public function edit()
	{
		$response = ["message"=>"success"];
		$wallet = post('add_wallet');
		
		$user_data = json_decode(post('user_data'));
		$email = clean_data(post('email'));
		if(isset($user_data->password)) {
			$user_data->password = hash_password($user_data->password);
		}
		$new_wallet = $user_data->wallet + $wallet;
		$user_data->wallet = $new_wallet;
		
		$user_data = (array) $user_data;
		$filter = ["email"=>$email]; 
		$this->User_model->update('users',$user_data,$filter);
		
		echo json_encode($response);
	}

	public function delete()
	{
		$response = ["message"=>"success"];
		$email = clean_data(post('email'));
		$filter = ["email"=>$email]; 
		$this->User_model->delete('users',$filter);
		echo json_encode($response);
	}

	public function changeStatus(){
		$response = ["message"=>"success"];
		$email = clean_data(post('email'));
		$status = clean_data(post('status'));
		$status = !$status;
		$data = ["active"=>$status];
		$filter = ["email"=>$email]; 
		$this->User_model->update('users',$data,$filter);
		echo json_encode($response);
	}
}