<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class vouchers_api extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Categories_model');
		$this->load->model('Global_model');
		if (is_logged_admin() == 0) {
			redirect(base_url());
		}
	}

	public function get_vouchers()
	{
		$data['vouchers'] = $this->Global_model->fetch('vouchers', '', 1000, '', 'created_at desc');
		echo json_encode($data);
	}


	public function import_vouchers()
	{
		$brand_id = post('brand_id');
		$product_id = post('product_id');
		$response = ["message" => "success"];
		$voucher_data = [];
		$bulk_order_key = random_code(15);
		if (!empty($_FILES['file']['name'])) {
			if (!empty($_FILES['file']['name'])) {
				$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				if ($_FILES['file']['name'] && ($ext == "xls" || $ext == "xlsx")) {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
					if ($ext == "xlsx") {
						$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
					}

					$spreadsheet = $reader->load($_FILES['file']['tmp_name']);
					$worksheet  = $spreadsheet->getActiveSheet();
					$highestRow = $worksheet->getHighestRow();
					date_default_timezone_set('Asia/Manila');
					$t = time();
					$created_at = date("Y-m-d h:m:s", $t);
					$voucher_data = array();
					for ($row = 2; $row <= $highestRow; ++$row) {
						$voucher_code = $spreadsheet->getActiveSheet()->getCell('A' . $row)->getValue();
						$voucher_notes = $spreadsheet->getActiveSheet()->getCell('B' . $row)->getValue();
						$data = array("voucher_code" => $voucher_code, "voucher_notes" => $voucher_notes, "brand_id" => $brand_id,"product_id"=>$product_id,"created_at" => $created_at);
						$voucher_data[] = $data;
					}

					$status = $this->Global_model->insert_batch("vouchers", $voucher_data);
					if (!$status) {
						$response["message"] = "Duplicate Voucher Code";
					}
					$this->Global_model->insert('bulk_order_keys', ["bulk_order_key"=>$bulk_order_key,"type"=>"vouchers"]);
				}
			} else {
				$response["message"] = "failed";
			}
		} else {
			$response["message"] = "failed";
		}
		echo json_encode($response);
	}



	function upload_file($path, $new_name, $input)
	{
		// define parameters
		$config['upload_path'] = $path;
		$config['allowed_types'] = 'xls|csv|xlsx';
		$config['max_size'] = '0';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['file_name'] = $new_name;
		$this->load->library('upload');
		$this->upload->initialize($config);
		// upload the image
		if ($this->upload->do_upload($input)) {
			return "success";
		} else {
			return $this->upload->file_type;
		}
	}


	public function add()
	{
		$response = ["message" => "success"];
		$category_data = json_decode(post('category_data'));
		//die(print_r($category_data));
		$this->Categories_model->insert('menu', $category_data);
		$row = $this->Categories_model->fetch_tag_row('id', 'menu', '', '1', '', 'id desc');
		$response['id'] = $row->id;
		echo json_encode($response);
	}

	public function get_menu()
	{
		$id = clean_data(rawurldecode(get('q')));
		$filter = ["id" => $id];
		$data["menu"] = $this->Categories_model->fetch_data('menu', $filter);
		echo json_encode($data);
	}

	public function edit()
	{
		$response = ["message" => "success"];
		$sub_category = post('sub_category');
		$sub_category_chi = post('sub_category_chi');
		$sub_category_code = post('sub_category_code');
		$sub_category_main_code = post('sub_category_main_code');
		$id = clean_data(post('id'));
		$data = ["sub_category" => $sub_category, "sub_category_chi" => $sub_category_chi, "sub_category_code" => $sub_category_code, "sub_category_main_code" => $sub_category_main_code];
		$filter = ["id" => $id];
		$this->Categories_model->update('menu', $data, $filter);
		echo json_encode($response);
	}

	public function delete()
	{
		$id = clean_data(post('id'));
		$filter = ["id" => $id];
		$this->Categories_model->delete('coupons', $filter);
		echo json_encode($response);
	}

	public function changeStatus()
	{
		$id = clean_data(post('id'));
		$status = clean_data(post('status'));
		$status = !$status;
		$data = ["status" => $status];
		$filter = ["id" => $id];
		$this->Categories_model->update('coupons', $data, $filter);
		echo json_encode($response);
	}
}
