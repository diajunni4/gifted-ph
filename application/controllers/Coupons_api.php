<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class coupons_api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Global_model');
		if(is_logged_admin() == 0){
			redirect(base_url());
		}
	}
	
	public function get_all_coupons() {
		$data['coupons'] = $this->Global_model->fetch('promo_codes');
		echo json_encode($data);
	}


	public function add()
	{
		$response = ["message"=>"success"];
		$coupons_data = post('coupons_data');
		//die(print_r($category_data));
		$this->Global_model->insert('promo_codes',$coupons_data);
		echo json_encode($response);
	}

	public function get_coupon(){
        $id = clean_data(rawurldecode(get('q')));
		$filter = ["id"=>$id];
		$data["coupon"] = $this->Global_model->fetch_data('promo_codes',$filter);
		echo json_encode($data);
	}

	public function edit()
	{
        $response = ["message"=>"success"];
        $getData = post('coupons_data');

        $code = $getData["code"];
		$limit = $getData["limit"];
		$discount = $getData["discount"];
		$id = clean_data(post('id'));
		$data = ["code" => $code, "amount" => $discount, "limit" => $limit];
		$filter = ["id"=>$id]; 
		$this->Global_model->update('promo_codes',$data,$filter);
	
		echo json_encode($response);
	}

	public function delete()
	{
		$id = clean_data(post('id'));
		$filter = ["id"=>$id]; 
		$this->Global_model->delete('promo_codes',$filter);
		echo json_encode($response);
	}

	public function changeStatus(){
		$id = clean_data(post('id'));
		$status = clean_data(post('status'));
		$status = !$status;
		$data = ["status"=>$status];
		$filter = ["id"=>$id]; 
		$this->Global_model->update('promo_codes',$data,$filter);
		echo json_encode($response);
	}
}