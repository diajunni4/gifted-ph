<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class privacy_policy extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model("Global_model");
	}

	public function index() {
		$this->load->view('templates/privacy_policy_template');
	}
}