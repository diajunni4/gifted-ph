<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Dompdf\Dompdf;

use Dompdf\Options;

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class orders_api extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Cart_Order_model');
		$this->load->model('Global_model');
		$this->load->model("Api_model");
	}

	public function get_order_details()
	{
		$order_number = get('order_number');
		if (!$order_number) show_404();

		$data = $this->Cart_Order_model->get($order_number);
		$filter = ["a.id" => $data[0]['product_id']];
		$data['product_details'] = $this->Api_model->get_product_details($filter);
		$filter = ["order_number" => $data[0]['order_number']];
		$data['vouchers'] = $this->Global_model->fetch('vouchers', $filter);
		echo json_encode($data);
	}

	public function update_voucher_notes()
	{
		$response = ["message" => "error"];
		$voucher_code = clean_data(post('voucher_code'));
		$voucher_notes = clean_data(post('voucher_notes'));
		$filter = ["voucher_code" => $voucher_code];
		$data = ["voucher_notes" => $voucher_notes];
		$status = $this->Global_model->update('vouchers', $data, $filter);
		if ($status)
			$response["message"] = "success";

		echo json_encode($response);
	}



	public function resend_vouchers()
	{
		$this->load->model('Api_model');
		$response = ["message" => "error"];
		$order_number = post('order_number');
		$vouchers = post('voucher_codes');
		$filter = ['order_number' => $order_number];
		$order_data = $this->Api_model->get_order_details($order_number);
		$lazada_main_order_reference = $order_data->lazada_main_order_reference ? json_decode($order_data->lazada_main_order_reference) : "";
		$about = $order_data->about;
		$how_to_redeem = $order_data->how_to_redeem;
		$promo_terms = $order_data->terms_and_condition;
		$description = $order_data->product_description;
		//die(print_r($lazada_main_order_reference->customer_first_name));
		$email_data = [];
		$email = "";
		if ($lazada_main_order_reference != "") {
			$email = $order_data->email;
			$email_data = [
				"lastname" => getLastName(trim($lazada_main_order_reference->customer_first_name)),
				"total" => $order_data->total,
				"order_number" => $order_number,
				"vouchers" => $vouchers,
				"promo_terms" => $promo_terms,
				"description" => $description,
				"about" => $about,
				"how_to_redeem" => $how_to_redeem,
				"image" => $order_data->image
			];
		} else {
			$gift_to_details = json_decode($order_data->gift_to_details);
			$email = $gift_to_details->email_address;
			if (isset($gift_to_details->first_name)) {
				$email_data = [
					"name" => $gift_to_details->name,
					"total" => $order_data->total,
					"order_number" => $order_number,
					"vouchers" => $vouchers,
					"promo_terms" => $promo_terms,
					"description" => $description,
					"about" => $about,
					"how_to_redeem" => $how_to_redeem,
					"image" => $order_data->image
				];
			} else {
				$email_data = [
					"name" => $order_data->firstname . ' ' . $order_data->lastname,
					"total" => $order_data->total,
					"order_number" => $order_number,
					"vouchers" => $vouchers,
					"promo_terms" => $promo_terms,
					"description" => $description,
					"about" => $about,
					"how_to_redeem" => $how_to_redeem,
					"image" => $order_data->image
				];
			}
		}


		$isSent = $this->sendVoucherEmail($email_data, $order_number, $email);
		if ($isSent) {
			foreach ($vouchers as $voucher) {
				$date = date("Y-m-d h:i:sa");
				$this->Global_model->update('vouchers', ["date_sent" => $date], ["voucher_code" => $voucher]);
			}
			$response["message"] = "success";
		}

		echo json_encode($response);

		// $isSent = $this->sendVoucherEmail($email_data, $new_order_number, $email);
		// echo $isSent;
	}

	public function send_mail_voucher($order_number, $vouchers, $email)
	{
		$this->load->model('Api_model');
		$response = ["message" => "error"];
		$filter = ['order_number' => $order_number];
		$order_data = $this->Api_model->get_order_details($order_number);
		$about = $order_data->about;
		$how_to_redeem = $order_data->how_to_redeem;
		$promo_terms = $order_data->terms_and_condition;
		$description = $order_data->product_description;
		//die(print_r($lazada_main_order_reference->customer_first_name));
		$email_data = [];
		$email = "";

		$gift_to_details = json_decode($order_data->gift_to_details);
		$email = $gift_to_details->email_address;
		if (isset($gift_to_details->first_name)) {
			$email_data = [
				"name" => $gift_to_details->name,
				"total" => $order_data->total,
				"order_number" => $order_number,
				"vouchers" => $vouchers,
				"promo_terms" => $promo_terms,
				"description" => $description,
				"about" => $about,
				"how_to_redeem" => $how_to_redeem,
				"image" => $order_data->image
			];
		} else {
			$email_data = [
				"name" => $order_data->firstname . ' ' . $order_data->lastname,
				"total" => $order_data->total,
				"order_number" => $order_number,
				"vouchers" => $vouchers,
				"promo_terms" => $promo_terms,
				"description" => $description,
				"about" => $about,
				"how_to_redeem" => $how_to_redeem,
				"image" => $order_data->image
			];
		}


		$isSent = $this->sendVoucherEmail($email_data, $order_number, $email);
		if ($isSent) {
			foreach ($vouchers as $voucher) {
				$date = date("Y-m-d h:i:sa");
				$this->Global_model->update('vouchers', ["date_sent" => $date], ["voucher_code" => $voucher]);
			}
			$response["message"] = "success";
		}

		return $response;
	}


	private function get_order_price_info($order_data)
	{
		$email = get_email();
		$brand_id = $order_data['brand_id'];


		$product_id = $order_data['product_id'];
		//number of quantity
		$quantity = $order_data['quantity'];
		$filter = ["id" => $product_id];
		//get product details
		$product_details = $this->Global_model->fetch_tag_row('*', 'products', $filter);
		$payment_fee = $product_details->payment_fee / 100;
		$payment_vat = $product_details->payment_vat / 100;


		$filter = ['brand_id' => $brand_id];
		$brand_details = $this->Global_model->fetch_tag_row('merchant_fee,tax_merchant_vat,affiliate_commission,voucher_character_length', 'brands', $filter);
		$merchant_fee = $brand_details->merchant_fee / 100;
		$tax_merchant_vat = $brand_details->tax_merchant_vat / 100;
		$order_price_details['voucher_character_length'] = $brand_details->voucher_character_length;

		//customer total
		//get the price of the voucher
		$price = $product_details->saleprice > 0 ?  $product_details->saleprice :  $product_details->price;
		$order_price_details['sub_total'] = ($price * $quantity);
		$order_price_details['delivery_charge'] = $product_details->free_shipping;
		$order_price_details['service_fee'] = $order_price_details['sub_total'] * $merchant_fee;
		$order_price_details['tmp_sub_total'] = ($price * $quantity) + $order_price_details['service_fee'] + $order_price_details['delivery_charge'];
		$order_price_details['vat'] = 0;
		$order_price_details['vatable_sales'] = 0;
		$order_price_details['vatable_sales'] = round(($order_price_details['tmp_sub_total'] + $order_price_details['service_fee'] + $order_price_details['delivery_charge']) - ($price * $quantity), 2);
		if ($order_price_details['vatable_sales'] >= 0) {
			$order_price_details['vat'] = round($order_price_details['vatable_sales'] * $payment_vat, 2);
		}

		$order_price_details['total'] = round($order_price_details['tmp_sub_total'] + $order_price_details['vat'], 2);
		//compute for merchant total
		if ($payment_fee > 0) {
			$order_price_details['merchant_fee_total'] = $order_price_details['tmp_sub_total'] * $merchant_fee;
			$order_price_details['merchant_fee_vat_sales'] = round($order_price_details['merchant_fee_total'] / 1.12, 2);
			$order_price_details['merchant_fee_vat'] = $order_price_details['merchant_fee_total'] - $order_price_details['merchant_fee_vat_sales'];
			$order_price_details['merchant_total'] = $order_price_details['tmp_sub_total'] - $order_price_details['merchant_fee_total'];
			$order_price_details['withholding_tax'] = round($order_price_details['merchant_fee_vat_sales'] * 0.02, 2);
			$order_price_details['merchant_total_and_witholding'] = round($order_price_details['merchant_total'] - $order_price_details['withholding_tax'], 2);
		}

		//reference database
		$order_price_details['merchant_fee'] = $brand_details->merchant_fee;
		$order_price_details['tax_merchant_vat'] = $brand_details->tax_merchant_vat;
		$order_price_details['payment_fee'] = $product_details->payment_fee;
		$order_price_details['payment_vat'] = $product_details->payment_vat;
		return $order_price_details;
	}

	public function import_orders()
	{
		$response = ["message" => "success"];
		// //get order price details
		$email = get_email();
		// $payment_method = clean_data(post('payment_method'));
		$bulk_order_key = random_code(15);
		if (!empty($_FILES['file']['name'])) {
			$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
			if ($_FILES['file']['name'] && ($ext == "xls" || $ext == "xlsx")) {
				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
				if ($ext == "xlsx") {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				}

				$spreadsheet = $reader->load($_FILES['file']['tmp_name']);
				$worksheet  = $spreadsheet->getActiveSheet();
				$highestRow = $worksheet->getHighestRow();
				$new_order = $new_voucher = [];
				for ($row = 2; $row <= $highestRow; $row++) {
					$brand_id =  $spreadsheet->getActiveSheet()->getCell('A' . $row)->getValue();
					$product_id = $spreadsheet->getActiveSheet()->getCell('b' . $row)->getValue();
					$greeting_card_id = $spreadsheet->getActiveSheet()->getCell('C' . $row)->getValue();
					$greetings = new stdClass();
					$greetings->sub_category  = $spreadsheet->getActiveSheet()->getCell('D' . $row)->getValue();
					$sending_option = $spreadsheet->getActiveSheet()->getCell('E' . $row)->getValue();
					$quantity = $spreadsheet->getActiveSheet()->getCell('F' . $row)->getValue();
					$greetings->name = $spreadsheet->getActiveSheet()->getCell('G' . $row)->getValue();
					$greetings->email_address = $spreadsheet->getActiveSheet()->getCell('H' . $row)->getValue();
					$greetings->contact_number = $spreadsheet->getActiveSheet()->getCell('I' . $row)->getValue();
					$greetings->from_name = $spreadsheet->getActiveSheet()->getCell('J' . $row)->getValue();
					$greetings->message = $spreadsheet->getActiveSheet()->getCell('K' . $row)->getValue();
					$admin_notes = $spreadsheet->getActiveSheet()->getCell('L' . $row)->getValue();
					$default_status = "Completed";
					$filter = ['brand_id' => $brand_id];
					$brand_details = $this->Global_model->fetch_tag_row('voucher_character_length', 'brands', $filter);
					$voucher_character_length = $brand_details->voucher_character_length;
					$vouchers = [];
					$voucher_data = array();
					$tmp_quantity = $quantity;
					$orders = [];
					$tmp_order_numbers = [];
					$last_loop = false;
					if ($highestRow == 2 && $tmp_quantity < 10) {
						$last_loop = true;
					}

					do {
						$order_number = generateOrderNumber();
						$check_order_number = in_array($order_number, $tmp_order_numbers);
					} while ($check_order_number);
					array_push($tmp_order_numbers, $order_number);


					//voucher generation
					for ($x = 1; $x <= $tmp_quantity; $x++) {
						$voucher_code = random_code($voucher_character_length);
						$filter = ['brand_id' => $brand_id, "order_number" => "", "is_redeemed" => 0];
						$voucher_row = $this->Global_model->fetch_tag_row('voucher_code', 'vouchers', $filter);
						if (!empty($voucher_row)) {
							$voucher_code = $voucher_row->voucher_code;
							$update_voucher_data = ["order_number" => $order_number];
							$this->Global_model->update('vouchers', $update_voucher_data, ["voucher_code" => $voucher_code]);
						} else {
							do {
								$filter =  ["voucher_code" => $voucher_code];
								$check_exist = $this->Global_model->check_exist("vouchers", $filter);
							} while ($check_exist > 0);

							$voucher_data[] = ["voucher_code" => $voucher_code, "order_number" => $order_number, "brand_id" => $brand_id, "bulk_order_key" => $bulk_order_key, "product_id" => $product_id];
						}
						array_push($vouchers, $voucher_code);
						if ($x == 10 || $x == $tmp_quantity) {
							$order_data = ["brand_id" => $brand_id, "product_id" => $product_id, "quantity" => $x];
							$order_price_details = $this->get_order_price_info($order_data);
							$sub_total = $order_price_details['sub_total'];
							$delivery_charge = $order_price_details['delivery_charge'];
							$vatable_sales = $order_price_details['vatable_sales'];
							$vat = $order_price_details['vat'];
							$merchant_fee_total = isset($order_price_details['merchant_fee_total']) ? $order_price_details['merchant_fee_total'] : 0;
							$merchant_fee_vat_sales = isset($order_price_details['merchant_fee_vat_sales']) ? $order_price_details['merchant_fee_vat_sales'] : 0;
							$merchant_fee_vat = isset($order_price_details['merchant_fee_vat']) ? $order_price_details['merchant_fee_vat'] : 0;
							$merchant_total = isset($order_price_details['merchant_total']) ?  $order_price_details['merchant_total'] : 0;
							$withholding_tax = isset($order_price_details['withholding_tax']) ? $order_price_details['withholding_tax'] : 0;
							$merchant_total_and_witholding = isset($order_price_details['merchant_total_and_witholding']) ? $order_price_details['merchant_total_and_witholding'] : 0;
							$merchant_fee = isset($order_price_details['merchant_fee']) ? $order_price_details['merchant_fee'] : 0;
							$tax_merchant_vat = isset($order_price_details['tax_merchant_vat']) ? $order_price_details['tax_merchant_vat'] : 0;
							$payment_fee = isset($order_price_details['payment_fee']) ? $order_price_details['payment_fee'] : 0;
							$payment_vat = isset($order_price_details['payment_vat']) ? $order_price_details['payment_vat'] : 0;
							$service_fee = isset($order_price_details['service_fee']) ? $order_price_details['service_fee'] : 0;
							$total = isset($order_price_details['total']) ? $order_price_details['total'] : 0;
							$billing_address = new stdClass();
							$shipping_address = new stdClass();

							//voucher per order
							$new_voucher = $vouchers;
							$new_order =  [
								"order_number" => $order_number,
								"product_id" => $product_id,
								"greeting_card_id" => $greeting_card_id,
								"quantity" => $x,
								"brand_id" => $brand_id,
								"sub_total" => $sub_total,
								"delivery_charge" => $delivery_charge,
								"total" => $total,
								"payment_method" => "bank_transfer",
								"vouchers" => json_encode($vouchers),
								"gift_to_details" => json_encode($greetings),
								"billing_address" => json_encode($billing_address),
								"shipping_address" => json_encode($shipping_address),
								"delivery_option" => "send_email",
								"email" => $email,
								"merchant_fee" => $merchant_fee,
								"tax_merchant_vat" => $tax_merchant_vat,
								"payment_fee" => $payment_fee,
								"payment_vat" => $payment_vat,
								"vatable_sales" => $vatable_sales,
								"service_fee" => $service_fee,
								"vat" => $vat,
								"merchant_fee_total" => $merchant_fee_total,
								"merchant_fee_vat_sales" => $merchant_fee_vat_sales,
								"merchant_fee_vat" => $merchant_fee_vat,
								"merchant_total" => $merchant_total,
								"status" => $default_status,
								"withholding_tax" => $withholding_tax,
								"merchant_total_and_witholding" => $merchant_total_and_witholding,
								"bulk_order_key" => $bulk_order_key,
								"sending_option" => $sending_option,
								"admin_notes" => $admin_notes ? $admin_notes : ""
							];

							if ($sending_option == "dont_send") {
								$orders[] = $new_order;
							}

							if ($sending_option == "send_email_greeeting") {
								$this->Global_model->insert('orders', $new_order);
								//$this->send_mail_gc($order_number, $new_voucher, $greetings->email_address);
							} elseif ($sending_option == "send_email_voucher") {
								$this->Global_model->insert('orders', $new_order);
								$this->send_mail_voucher($order_number, $new_voucher, $greetings->email_address);
							} elseif ($sending_option == "send_sms") {
								//sms api
								$this->Global_model->insert('orders', $new_order);
							}
							break;
						}
					}
					if ($sending_option == "dont_send") {
						$this->Global_model->insert_batch('orders', $orders);
					}

					if (!empty($voucher_data)) {
						$this->Global_model->insert_batch('vouchers', $voucher_data);
					}
				}
				$this->Global_model->insert('bulk_order_keys', ["bulk_order_key"=>$bulk_order_key]);
				$response["message"] = "success";
				$response["bulk_order_key"] = $bulk_order_key;
			} else {
				$response["message"] = "failed";
			}
		} else {
			$response["message"] = "failed";
		}
		//$this->Widgets_model->insert('banners',$file_data);
		echo json_encode($response);
	}

	private function upload_file($path, $new_name, $input)
	{
		// define parameters
		$config['upload_path'] = $path;
		$config['allowed_types'] = 'xls|xl|xlsx';
		$config['max_size'] = '0';
		$config['max_width'] = '0';
		$config['max_height'] = '0';
		$config['file_name'] = $new_name;
		$this->load->library('upload');
		$this->upload->initialize($config);
		// upload the image
		if ($this->upload->do_upload($input)) {
			return "success";
		} else {
			return $this->upload->file_type;
		}
	}

	public function generate_template()
	{
		$brand_id = get('brand_id');
		
		$row = $this->Global_model->fetch_tag_row('name','brands',["brand_id"=>$brand_id]);
		$brand_name = $row->name;
		$product_id = get('product_id');
		$row = $this->Global_model->fetch_tag_row('name','products',["id"=>$product_id]);
		$product_name = $row->name;
		$sending_option = get('sending_option');
		$selected_sub_category = get('selected_sub_category');
		$selected_greeting_card_id = get('selected_greeting_card_id');
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Brand ID');
		$sheet->setCellValue('B1', 'Product ID');
		$sheet->setCellValue('C1', 'Greeting Card ID');
		$sheet->setCellValue('D1', 'Recommended Gift Item');
		$sheet->setCellValue('E1', 'Sending Option');
		$sheet->setCellValue('F1', 'Quantity');
		$sheet->setCellValue('G1', 'Recipient Name');
		$sheet->setCellValue('H1', 'Recipient Email');
		$sheet->setCellValue('I1', 'Recipient Contact Number');
		$sheet->setCellValue('J1', 'Gift is From');
		$sheet->setCellValue('K1', 'Message');
		$sheet->setCellValue('L1', 'Admin Notes');
		$sheet->setCellValue('M1', 'Brand Name');
		$sheet->setCellValue('N1', 'Product Name');
		$sheet->setCellValue('A2', $brand_id);
		$sheet->setCellValue('B2', $product_id);
		$sheet->setCellValue('C2', $selected_greeting_card_id);
		$sheet->setCellValue('D2', $selected_sub_category);
		$sheet->setCellValue('E2', $sending_option);
		$sheet->setCellValue('M2', $brand_name);
		$sheet->setCellValue('N2', $product_name);
		$writer = new Xlsx($spreadsheet);

		$filename = 'bulk_order_template';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output'); // download file 	
	}

	public function create_orders()
	{
		$response = ["message" => "failed"];
		$brand_id = post('brand_id');
		$product_id = post('product_id');
		$admin_notes = post('admin_notes');
		$default_status = post('default_status');
		$greetings = post('gift_to_details');
		$sending_option = post('sending_option');
		$greeting_card_id = $greetings['greeting_card_id'];
		$quantity = post('quantity');
		$filter = ['brand_id' => $brand_id];
		$brand_details = $this->Global_model->fetch_tag_row('voucher_character_length', 'brands', $filter);
		$voucher_character_length = $brand_details->voucher_character_length;

		// //get order price details
		$email = get_email();
		// $payment_method = clean_data(post('payment_method'));
		$bulk_order_key = random_code(15);

		$vouchers = [];
		$voucher_data = [];
		$tmp_quantity = $quantity;
		$orders = [];
		$tmp_order_numbers = [];
		$last_loop = false;
		//generate vouchers and orders
		do {
			do {
				$order_number = generateOrderNumber();
				$check_order_number = in_array($order_number, $tmp_order_numbers);
			} while ($check_order_number);
			array_push($tmp_order_numbers, $order_number);

			for ($x = 1; $x <= $tmp_quantity; $x++) {
				$voucher_code = random_code($voucher_character_length);
				$filter = ['brand_id' => $brand_id, "order_number" => "", "is_redeemed" => 0];
				$row = $this->Global_model->fetch_tag_row('voucher_code', 'vouchers', $filter);
				if (!empty($row)) {
					$voucher_code = $row->voucher_code;
					$update_voucher_data = ["order_number" => $order_number];
					$this->Global_model->update('vouchers', $update_voucher_data, ["voucher_code" => $voucher_code]);
				} else {
					do {
						$filter =  ["voucher_code" => $voucher_code];
						$check_exist = $this->Global_model->check_exist("vouchers", $filter);
					} while ($check_exist > 0);

					$voucher_data[] = ["voucher_code" => $voucher_code, "order_number" => $order_number, "brand_id" => $brand_id, "bulk_order_key" => $bulk_order_key, "product_id" => $product_id];
				}
				array_push($vouchers, $voucher_code);

				$new_voucher = "";
				if ($tmp_quantity < 10) {
					$last_loop = true;
				}
				if ($x == 10 || ($last_loop && $x == $tmp_quantity)) {
					$order_data = ["brand_id" => $brand_id, "product_id" => $product_id, "quantity" => $x];
					$order_price_details = $this->get_order_price_info($order_data);
					$sub_total = $order_price_details['sub_total'];
					$delivery_charge = $order_price_details['delivery_charge'];
					$vatable_sales = $order_price_details['vatable_sales'];
					$vat = $order_price_details['vat'];
					$merchant_fee_total = isset($order_price_details['merchant_fee_total']) ? $order_price_details['merchant_fee_total'] : 0;
					$merchant_fee_vat_sales = isset($order_price_details['merchant_fee_vat_sales']) ? $order_price_details['merchant_fee_vat_sales'] : 0;
					$merchant_fee_vat = isset($order_price_details['merchant_fee_vat']) ? $order_price_details['merchant_fee_vat'] : 0;
					$merchant_total = isset($order_price_details['merchant_total']) ?  $order_price_details['merchant_total'] : 0;
					$withholding_tax = isset($order_price_details['withholding_tax']) ? $order_price_details['withholding_tax'] : 0;
					$merchant_total_and_witholding = isset($order_price_details['merchant_total_and_witholding']) ? $order_price_details['merchant_total_and_witholding'] : 0;
					$merchant_fee = isset($order_price_details['merchant_fee']) ? $order_price_details['merchant_fee'] : 0;
					$tax_merchant_vat = isset($order_price_details['tax_merchant_vat']) ? $order_price_details['tax_merchant_vat'] : 0;
					$payment_fee = isset($order_price_details['payment_fee']) ? $order_price_details['payment_fee'] : 0;
					$payment_vat = isset($order_price_details['payment_vat']) ? $order_price_details['payment_vat'] : 0;
					$service_fee = isset($order_price_details['service_fee']) ? $order_price_details['service_fee'] : 0;
					$total = isset($order_price_details['total']) ? $order_price_details['total'] : 0;
					$billing_address = new stdClass();
					$shipping_address = new stdClass();

					//voucher per order
					$new_voucher = $vouchers;
					$tmp_vouchers = json_encode($vouchers);

					$new_order = [
						"order_number" => $order_number,
						"product_id" => $product_id,
						"greeting_card_id" => $greeting_card_id,
						"quantity" => $x,
						"brand_id" => $brand_id,
						"sub_total" => $sub_total,
						"delivery_charge" => $delivery_charge,
						"total" => $total,
						"payment_method" => "bank_transfer",
						"vouchers" => $tmp_vouchers,
						"gift_to_details" => json_encode($greetings),
						"billing_address" => json_encode($billing_address),
						"shipping_address" => json_encode($shipping_address),
						"delivery_option" => "send_email",
						"email" => $email,
						"merchant_fee" => $merchant_fee,
						"tax_merchant_vat" => $tax_merchant_vat,
						"payment_fee" => $payment_fee,
						"payment_vat" => $payment_vat,
						"vatable_sales" => $vatable_sales,
						"service_fee" => $service_fee,
						"vat" => $vat,
						"merchant_fee_total" => $merchant_fee_total,
						"merchant_fee_vat_sales" => $merchant_fee_vat_sales,
						"merchant_fee_vat" => $merchant_fee_vat,
						"merchant_total" => $merchant_total,
						"status" => $default_status,
						"withholding_tax" => $withholding_tax,
						"merchant_total_and_witholding" => $merchant_total_and_witholding,
						"bulk_order_key" => $bulk_order_key,
						"sending_option" => $sending_option,
						"admin_notes" => $admin_notes
					];

					if ($sending_option == "dont_send") {
						$orders[] = $new_order;
					}


					if ($sending_option == "send_email_greeeting") {
						$this->Global_model->insert('orders', $new_order);
						$this->send_mail_gc($order_number, $new_voucher, $greetings['email_address']);
					} elseif ($sending_option == "send_email_voucher") {
						$this->Global_model->insert('orders', $new_order);
						$this->send_mail_voucher($order_number, $new_voucher, $greetings['email_address']);
					} elseif ($sending_option == "send_sms") {
						//sms api
						$this->Global_model->insert('orders', $new_order);
					}

					$vouchers = [];
					$tmp_quantity -= $x;
					//reset loop
					if ($tmp_quantity < 10) {
						$last_loop = true;
					}
					break;
				}
			}
		} while ($tmp_quantity > 0);

		if ($sending_option == "dont_send") {
			$this->Global_model->insert_batch('orders', $orders);
		}
		if (!empty($voucher_data)) {
			$this->Global_model->insert_batch('vouchers', $voucher_data);
		}
		$this->Global_model->insert('orders', ["bulk_order_key"=>$bulk_order_key]);
		echo json_encode(["bulk_order_key" => $bulk_order_key]);
		//echo json_encode($response);
	}

	public function cancel_bulk_orders() {
		$response = ["messsage"=>"failed"];
		$bulk_order_key  = clean_data(post('bulk_order_key'));
		$filter = ["bulk_order_key"=>$bulk_order_key];
		$check_exist = $this->Global_model->check_exist("orders", $filter);
		if($check_exist && $bulk_order_key != "") {
			$this->Global_model->update('orders',["status"=>"Cancelled"],$filter);
			$this->Global_model->update('orders',["status"=>0],$filter);
			$response["message"] = "success";
		}
		
		echo json_encode($response);
	}

	public function send_mail_gc($order_number, $vouchers, $email)
	{
		$data['order_details'] = $this->Api_model->get_order_details($order_number);
		//die(print_r($data));
		$greetings_template = $this->load->view('pages/pdfs/greeting_cards', $data, true);
		require_once(APPPATH . 'third_party/dompdf/autoload.inc.php');
		require_once(APPPATH . 'third_party/dompdf/lib/html5lib/Parser.php');
		require_once(APPPATH . 'third_party/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php');
		require_once(APPPATH . 'third_party/dompdf/lib/php-svg-lib/src/autoload.php');
		require_once(APPPATH . 'third_party/dompdf/src/Autoloader.php');
		// instantiate and use the dompdf class
		$dompdf = new Dompdf();
		$dompdf->loadHtml($greetings_template);
		$options = new Options();
		$options->setIsRemoteEnabled(true);
		$dompdf->setOptions($options);
		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'portrait');

		// Render the HTML as PDF
		$dompdf->render();
		//$dompdf->stream("asdsad" . ".pdf", array('compress' => 0,"Attachment" => true));
		$output = $dompdf->output();
		$response = ["message" => "error"];
		$filter = ['order_number' => $order_number];
		$order_data = $this->Api_model->get_order_details($order_number);
		$about = $order_data->about;
		$how_to_redeem = $order_data->how_to_redeem;
		$promo_terms = $order_data->terms_and_condition;
		$description = $order_data->product_description;
		//die(print_r($lazada_main_order_reference->customer_first_name));
		$email_data = [];
		$email = "";

		$gift_to_details = json_decode($order_data->gift_to_details);
		$email = $gift_to_details->email_address;
		if (isset($gift_to_details->first_name)) {
			$email_data = [
				"name" => $gift_to_details->name,
				"total" => $order_data->total,
				"order_number" => $order_number,
				"vouchers" => $vouchers,
				"promo_terms" => $promo_terms,
				"description" => $description,
				"about" => $about,
				"how_to_redeem" => $how_to_redeem,
				"image" => $order_data->image
			];
		} else {
			$email_data = [
				"name" => $order_data->firstname . ' ' . $order_data->lastname,
				"total" => $order_data->total,
				"order_number" => $order_number,
				"vouchers" => $vouchers,
				"promo_terms" => $promo_terms,
				"description" => $description,
				"about" => $about,
				"how_to_redeem" => $how_to_redeem,
				"image" => $order_data->image
			];
		}

		$isSent = $this->sendVoucherEmail($email_data, $order_number, $email, $output);
		if ($isSent) {
			foreach ($vouchers as $voucher) {
				$date = date("Y-m-d h:i:sa");
				$this->Global_model->update('vouchers', ["date_sent" => $date], ["voucher_code" => $voucher]);
			}
			$response["message"] = "success";
		}
		return $response;
	}


	public function send_notification_message()
	{
		$email_notification = post('email_notification');
		$message = $email_notification['message'];
		$subject = $email_notification['subject'];
		$receipient = $email_notification['receipient'];
		$email_body = $this->load->view('emails/email_notification', $email_notification, true);
		$email_content = [
			'from' => 'info@gifted.ph',
			'from_name' => "Gifted.PH",
			'to' => $receipient,
			'subject' => $subject,
			'message' => $email_body
		];

		$isSent = sendMailCIMailer($email_content);
		if ($isSent) {
			echo json_encode(["message" => "success"]);
		} else {
			echo json_encode(["message" => "error"]);
		}
	}

	function sendVoucherEmail($email_data, $new_order_number, $email, $output = "")
	{
		$email_body = $this->load->view('emails/gift_card_simplified', $email_data, true);
		$email_content = [
			'from' => 'info@gifted.ph',
			'from_name' => "Gifted.PH",
			'to' => $email,
			'subject' => 'A message from Gifted.PH: You have received a gift! - #' . $new_order_number,
			'message' => $email_body,
			'attachment' => $output
		];
		if ($output != "") {
			$isSent = sendMainGreetingCard($email_content);
		} else {
			$isSent = sendMailCIMailer($email_content);
		}

		return $isSent;
	}

	public function get_orders()
	{
		//$data['orders'] = $this->Global_model->fetch_tag_array('*', 'orders', $sort, "", "", 'FIELD(status, "Pending", "Processing", "Completed","Refunded","Cancelled"), created_at desc');
		$data['orders'] = $this->Api_model->get_orders();
		echo json_encode($data);
	}

	public function change_status()
	{
		$status = post('status');
		$id = post('id');
		$data = array('status' => $status);
		// if($status == '3') {
		// 	$data['status'] = 1;
		// 	$row = $this->Global_model->fetch_tag_row("invoice_number,points_earned,payment_mode,email","cart_order",array('id' => $id));
		// 	$invoice_number = $row->invoice_number;
		// 	$points_earned = $row->points_earned;
		// 	$payment_mode = $row->payment_mode;
		// 	$email = $row->email;
		// 	audit_purrcoins("Earn purrcoins","Order ".$invoice_number." has been completed through ".$payment_mode,"purrcoins collected",$points_earned,$email);
		// }

		//audit("Order Status Changed","Order ".$invoice_number." status changed to ".$status_name,"change order status","success");

		$this->Global_model->update('orders', $data, array('id' => $id));
	}

	public function update_order()
	{
		$order_data = post('order_data');
		$data = [
			"admin_notes" => $order_data['admin_notes'],
			"deposit_slip" => $order_data['deposit_slip'],
			"status" => $order_data['status'],
			"courier_details" => json_encode($order_data['courier_details'])
		];
		$order_number = post('order_number');
		$status = $this->Global_model->update('orders', $data, array('order_number' => $order_number));
		if ($status)
			echo json_encode(["message" => "success"]);
		else
			echo json_encode(["message" => "error"]);
	}
}
