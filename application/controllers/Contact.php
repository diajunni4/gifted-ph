<?php
defined('BASEPATH') or exit('No direct script access allowed');

class contact extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model("Global_model");
	}

	public function index()
	{
		$this->load->view('templates/contact_template');
	}

	function send_message()
	{
		$email_data = post('send_email');
		$email_content = [
			'from' => $email_data['email_address'],
			'from_name' => $email_data['first_name'] . ' ' . $email_data['last_name'],
			'to' => "merchantsupport@gifted.ph",
			'subject' => $email_data['subject'],
			'message' => $email_data['message']
		];

		$isSent = sendMailCIMailer($email_content);
		$response["message"] = "success";
		return $response;
	}
}
