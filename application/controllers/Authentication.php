<?php
defined('BASEPATH') or exit('No direct script access allowed');

class authentication extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model("Global_model");
		$this->load->model("User_model");
	}

	public function index()
	{
		if (is_logged()) {
			redirect(base_url('cart/summary'));
		} else {
			$data["redirect_url"] = get('redirect') ? clean_data(get('redirect')) : "";
			$data["content"] = "pages/authentication";
			$this->load->view('templates/authentication_template', $data);
		}
	}

	public function login()
	{
		if ($_POST) {
			$response = array("message" => "Invalid email or password.");
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|trim');
			if ($this->form_validation->run() == TRUE) {
				$email = strtolower(clean_data(post('email')));
				$password =  sha1(clean_data(post('password')));
				$filter = ["email" => $email, "active" => "1"];
				$user_details = $this->User_model->fetch_tag_row("password", "users", $filter);
				if (empty($user_details)) {
					$response["message"] = "Invalid email or passsword.";
				} else {
					$user_password = $user_details->password;
					if ($password == $user_password) {
						$response["message"] = 'success';
						$response["url"] = base_url();
						if (clean_data(post('redirect')) == "checkout") {
							$response["url"] = base_url('checkout');
						}
						$ses_key = encrypt($email);
						$ses_pass = encrypt($password);
						$sessdata = array('ses_key'  => $ses_key);
						$remember_me = clean_data(post('remember_me'));

						$this->session->set_userdata($sessdata);
						if ($remember_me == "remember") {
							setcookie('ses_key', $ses_key, time() + 2592000, "/");
							setcookie('ses_pass', $ses_pass, time() + 2592000, "/");
						}
						//audit("login", "customer", "authentication", "");
					} else {
						$filter = array("active" => "1", "role" => 'admin');
						$user_details = $this->User_model->fetch_tag_row("password", "users", $filter);
						if (!empty($user_details)) {
							$user_password = $user_details->password;
							if ($password ==  $user_password) {
								$response["message"] = 'success';
								$response["url"] = base_url();
								if (clean_data(post('redirect')) == "checkout") {
									$response["url"] = base_url('checkout');
								}
								$ses_key = encrypt($email);
								$sessdata = array('ses_key'  => $ses_key);
								$this->session->set_userdata($sessdata);
								//audit("login", "customer login via admin", "authentication", "");
							}
						}
					}
				}
			} else {
				$response["message"] = "Invalid email or password.";
			}
			echo json_encode($response);
		}
	}


	public function register()
	{
		if ($_POST) {
			$response = array("message" => "error");
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]|trim', array(
				'is_unique' => 'Email already exists.'
			));
			$this->form_validation->set_rules('firstname', 'First name', 'required|trim');
			$this->form_validation->set_rules('lastname', 'Last name', 'required|trim');
			$this->form_validation->set_rules('contact_number', 'Contact number', 'required|trim', array(
				'is_unique' => 'Contact number is required.'
			));
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|trim', array(
				'min_length' => 'Password should be at least 6 or more.'
			));
			$this->form_validation->set_rules('cf_password', 'Confirm Password', 'required|min_length[6]|matches[password]');
			if ($this->form_validation->run() == TRUE) {
				$password = hash_password(clean_data(post('password')));
				$firstname = clean_data(post('firstname'));
				$lastname = clean_data(post('lastname'));
				$email = strtolower(clean_data(post('email')));
				$contact_number = clean_data(post('contact_number'));
				$data = array("email" => $email, "password" => $password, "reg_type" => "Manual", 'contact_number' => $contact_number, 'firstname' => $firstname, 'lastname'=>$lastname);
				$this->User_model->insert("users", $data);
				//set session
				$ses_key = encrypt($email);
				$sessdata = array('ses_key'  => $ses_key, 'authentication_status' => 'registered');
				$this->session->set_userdata($sessdata);
				$response["message"] = "success";
				$response["url"] = base_url();

				if (clean_data(post('redirect')) == "checkout") {
					$response["url"] = base_url('checkout');
				}
			} else {
				$response["message"] = validation_errors();
			}
			echo json_encode($response);
		}
	}

	public function logout_session()
	{
		session_destroy();
	}
	public function check_login()
	{
		echo is_logged();
	}

	public function reset_password()
	{
		$res = array("message" => "Invalid request", "token" => "", "url" => "");
		if ($_POST) {
			$this->form_validation->set_rules('password', 'Password', 'required|trim|xss_clean|max_length[20]|min_length[8]');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|trim|xss_clean|max_length[20]|min_length[8]|matches[password]');
			if ($this->form_validation->run() == TRUE) {
				$password = hash_password(clean_data(post("password")));
				$id = clean_data($this->session->fp_id);
				$key = clean_data($this->session->fp_code);
				$expiration = time() - 1800;
				$filter = array("exp_date < " => $expiration, "id" => $id);
				$this->User_model->delete("password_key_verify", $filter);
				$filter = array(
					"code" => $key,
					"id" => $id,
					"exp_date > " => $expiration
				);
				$check_exist = $this->User_model->check_exist("password_key_verify", $filter);
				if ($check_exist > 0) {
					$filter = array("id" => $id);
					$res['message'] = "Invalid Account";
					$user_data = $this->User_model->fetch_tag_row("id", "users", $filter);
					if (!empty($user_data)) {
						$sess_id = encrypt($id);
						$res['message'] = "success";
						$res["url"] = base_url();

						$sessdata = array('id'  => $sess_id);
						$this->session->set_userdata($sessdata);
						$filter = array("id" => $id);

						$data = array("active" => 1, 'password' => $password);
						$this->User_model->update("users", $data, $filter);

						//delete token
						$filter = array("id " => $id);
						$this->User_model->delete("password_key_verify", $filter);

						//remove session
						$this->session->unset_userdata('fp_id');
						$this->session->unset_userdata('fp_code');
					}
				} else {
					$res["url"] = base_url();
					$res['message'] = "The token is invalid. Please request again.";
				}
			} else {
				$res['message'] = validation_errors();
			}
		}
		echo json_encode($res);
	}

	public function forgot_password()
	{
		$id = clean_data(decrypt($this->uri->segment(3)));
		$code = clean_data($this->uri->segment(4));

		$expiration = time() - 1800;
		$filter = array("exp_date < " => $expiration);
		$this->User_model->delete("password_key_verify", $filter);

		$filter = array(
			"code" => $code,
			"id" => $id,
			"exp_date > " => $expiration
		);

		$check_exist = $this->User_model->check_exist("password_key_verify", $filter);
		if ($check_exist) {
			$data['data']['categories'] = $this->Global_model->fetch('menu');
			$sessdata = array('fp_id'  => $id, 'fp_code' => $code);
			$this->session->set_userdata($sessdata);
			$data["content"] = "pages/forgot_password";
			$this->load->view("templates/authentication_template", $data);
		} else {
			redirect(base_url());
		}
	}

	public function send_forgot_password()
	{
		$res = array("message" => "Invalid Request", "token" => "", "url" => "");
		if ($_POST) {
			$email = strtolower(clean_data(post('email')));
			$filter = array("email" => $email, "active" => 1);
			$user = $this->User_model->fetch_tag_row('*', 'users', $filter);
			if ($user) {
				$res["message"] = "success";
				$id = $user->id;
				$this->generate_password_key($id, $email);
				audit("Forgot Password", "Reset Password Request", "success", "", $id);
			} else {
				$res["message"] = "Invalid account.";
			}
		}
		echo json_encode($res);
	}

	public function generate_password_key($id, $email)
	{
		$code = substr(md5(microtime()), rand(0, 26), 5);
		$hash_code = $code;
		$id = encrypt($id);
		$link = base_url() . "Authentication/forgot_password/" . $id . '/' . $hash_code;
		$content = '
			<h1>Pabile.store</h1><br><br>
			<h2>Hi ' . $email . ',</h2>
			<p>It looks like you requested a new password. If you did not request to have your password reset, 
			you can safely ignore this email.</p>
			<p>We assure you that your customer account is safe</p>
			<p>
			<a href="' . $link . '">Click the link here to reset your password</a>
			</p>';

		$status = false;
		$expiration = time();
		$email_content = array("message" => $content, "from" => "support@pabile.store", "from_name" => "Pabile", "to" => $email, "subject" => "Forgot Password");
		$status = sendMailCIMailer2($email_content);
		if ($status) {
			$data = array("code" => $code, "id" => $id, "email" => $email, "exp_date" => $expiration);
			$this->User_model->insert('password_key_verify', $data);
		}
		return $status;
	}


	public function logout()
	{
		//audit("logout", "customer", "authentication", "");
		session_destroy();
		if (isset($_COOKIE['ses_pass'])) {
			unset($_COOKIE['ses_pass']);
			setcookie('ses_pass', '', time() - 3600, '/'); // empty value and old timestamp
		}
		if (isset($_COOKIE['ses_key'])) {
			unset($_COOKIE['ses_key']);
			setcookie('ses_key', '', time() - 3600, '/'); // empty value and old timestamp
		}
		redirect(base_url());
	}
}
