<?php
defined('BASEPATH') or exit('No direct script access allowed');

class cart extends CI_Controller
{

   public function __construct()
   {
      parent::__construct();
      date_default_timezone_set('Asia/Manila');
      $this->load->model("Global_model");
      $this->load->model("Cart_model");
   }

   public function summary()
   {
      if (is_logged()) {
         $this->load->view('templates/cart_summary_template');
      } else {
         redirect(base_url());
      }
   }

   public function get_cart()
   {
      $order_key = isset($this->session->order_key) ? decrypt($this->session->order_key) : "";
      $email = get_email();
      $data['product_details'] = $this->Cart_model->get_order($order_key, $email);
      $data["additional_information"] = "";
      $data['order_price_details'] = 0;
      if (!empty($data['product_details'])) {
         $order_key = $data['product_details']->order_key ? $data['product_details']->order_key : "";
         $data["voucher"] = $data["product_details"]->voucher;
         if($data["voucher"]!= ""){
            $filter = ["voucher_code"=>$data["voucher"]];
            $row = $this->Global_model->fetch_tag_row('*','vouchers',$filter);
            $data["voucher_info"] = $row->additional_information;
         }
         
         $this->session->set_userdata('order_key', encrypt($order_key));
         $data["additional_information"] = $data['product_details']->additional_information ? $data['product_details']->additional_information : "";
         $data['order_price_details'] = $data['product_details']->order ? $this->get_order_price($data['product_details']->order) : 0;
      }

      echo json_encode($data);
   }

   public function check_order_key()
   {
      echo $order_key = isset($this->session->order_key) ? decrypt($this->session->order_key) : "";
   }

   public function save_order()
   {
      $order = post('order_details');
      $tmp_order = post('order_details');
      $order_price_details = $this->get_order_price($tmp_order);
      $order_key = isset($this->session->order_key) ? decrypt($this->session->order_key) : random_code(8);

      if (!isset($this->session->order_key)) {
         do {
            $filter =  ["order_key" => $order_key];
            $check_exist = $this->Global_model->check_exist("tmp_orders", $filter);
         } while ($check_exist > 0);
      }


      $count_rows = $this->Global_model->count_rows('tmp_orders');
      $count_rows = $count_rows ? $count_rows : 1;
      //$invoice_number = invoice_num($count_rows, 6, "PBL2019-");

      $email = get_email();
      if (isset($this->session->order_key)) {
         $data = ["order" => $order, "order_key" => $order_key];
         $filter =  ["order_key" => $order_key, "email" => $email];
         $this->Global_model->update('tmp_orders', $data, $filter);
      } elseif ($email != '') {
         $data = ["order" => $order];
         $filter =  ["email" => $email];
         $check_exist = $this->Global_model->check_exist("tmp_orders", $filter);
         if ($check_exist) {
            $this->Global_model->update('tmp_orders', $data, $filter);
         } else {
            $data = ["order" => $order, "order_key" => $order_key];
            $data['email'] = $email;
            $this->session->set_userdata('order_key', encrypt($order_key));
            $this->Global_model->insert('tmp_orders', $data);
         }
      } else {
         $data = ["order" => $order, "order_key" => $order_key];
         $data['email'] = $email;
         $this->session->set_userdata('order_key', encrypt($order_key));
         $this->Global_model->insert('tmp_orders', $data);
      }
      echo json_encode(['order_price_details' => $order_price_details]);
   }

   function save_cart_summary()
   {

      $order_key = isset($this->session->order_key) ? decrypt($this->session->order_key) : '';
      $email = get_email();
      $additional_information = clean_data(post('additional_information'));
      $voucher = clean_data(post('voucher_code'));
      if ($voucher != "") {
         $filter = ["voucher_code" => $voucher];
         $row = $this->Global_model->fetch_tag_row('*', 'vouchers', $filter);
         if (!empty($row)) {
            $limit = $row->limit;
            $minimum_order = $row->minimum_order;

            $data['product_details'] = $this->Cart_model->get_order($order_key, $email);
            $data['order_price_details'] = $data['product_details']->order ? $this->get_order_price($data['product_details']->order) : 0;
            $sub_total = $data['order_price_details']["sub_total"];
            
            if ($limit > 0 && $sub_total >= $minimum_order) {
               $data = ["additional_information" => $additional_information, "voucher" => $voucher];
               $filter = ["email" => $email, "order_key" => $order_key];
               $status = $this->Global_model->update('tmp_orders', $data, $filter);
               echo json_encode(["message" => "voucher applied"]);
            } elseif ($sub_total < $minimum_order) {
               echo json_encode(["message" => "less minimum","minimum_order"=>$minimum_order]);
            } elseif ($sub_total >= $minimum_order && $limit < 1) {
               echo json_encode(["message" => "voucher limit"]);
            }
            else {
               echo json_encode(["message" => "failed"]);
            }
         } else {
            echo json_encode(["message" => "failed"]);
         }
      } else {
         $data = ["additional_information" => $additional_information];
         $filter = ["email" => $email, "order_key" => $order_key];
         $status = $this->Global_model->update('tmp_orders', $data, $filter);
         echo json_encode(["message" => "success"]);
      }
   }

   function get_order_price($order)
   {
      $order_price_details = ["sub_total" => 0, "delivery_fee" => 0, "total" => 0];
      if (!empty($order)) {
         $order = json_decode($order);
         $cities = [];
         $restaurant_codes = [];
         foreach ($order as $key) {
            $order_price_details["sub_total"] += $key->price;
            array_push($restaurant_codes, $key->restaurant_code);
         }
         $unique_cities = array_unique($restaurant_codes);
         foreach ($unique_cities as $value) {
            $filter = ['restaurant_code' => $value];
            $row = $this->Global_model->fetch_tag_row('city', 'restaurants', $filter);
            array_push($cities, $row->city);
         }

         $delivery_fee = 25;

         if (in_array("San Pablo", $cities) && in_array("Rizal", $cities)) {
            $delivery_fee = 50;
            $tmp = array_count_values($cities);
            $cnt = $tmp["San Pablo"];
            if ($cnt > 1) {
               for ($x = 1; $x < $cnt; $x++) {
                  $delivery_fee += 10;
               }
            }

            $cnt = $tmp["Rizal"];
            if ($cnt > 0) {
               for ($x = 0; $x < $cnt; $x++) {
                  $delivery_fee += 10;
               }
            }
         } elseif (in_array("San Pablo", $cities)) {
            $delivery_fee = 50;
            $tmp = array_count_values($cities);
            $cnt = $tmp["San Pablo"];
            if ($cnt > 1) {
               for ($x = 1; $x < $cnt; $x++) {
                  $delivery_fee += 10;
               }
            }
         } elseif (in_array("Rizal", $cities)) {
            $tmp = array_count_values($cities);
            $cnt = $tmp["Rizal"];
            if ($cnt > 1) {
               for ($x = 1; $x < $cnt; $x++) {
                  $delivery_fee += 10;
               }
            }
         }
         $order_price_details["delivery_fee"] = $delivery_fee;
         $order_price_details["total"] = $order_price_details["delivery_fee"] + $order_price_details["sub_total"];
      }

      return $order_price_details;
   }

   function get_order_price2()
   {
      $order = post('product_details');
      $order_price_details = ["sub_total" => 0, "delivery_fee" => 0, "total" => 0];
      if (!empty($order)) {

         $order = json_decode($order);
         $cities = [];
         $restaurant_codes = [];
         foreach ($order as $key) {
            $order_price_details["sub_total"] += $key->price;
            array_push($restaurant_codes, $key->restaurant_code);
         }
         $unique_cities = array_unique($restaurant_codes);
         foreach ($unique_cities as $value) {
            $filter = ['restaurant_code' => $value];
            $row = $this->Global_model->fetch_tag_row('city', 'restaurants', $filter);
            array_push($cities, $row->city);
         }

         $delivery_fee = 25;

         if (in_array("San Pablo", $cities) && in_array("Rizal", $cities)) {
            $delivery_fee = 50;
            $tmp = array_count_values($cities);
            $cnt = $tmp["San Pablo"];
            if ($cnt > 1) {
               for ($x = 1; $x < $cnt; $x++) {
                  $delivery_fee += 10;
               }
            }

            $cnt = $tmp["Rizal"];
            if ($cnt > 0) {
               for ($x = 0; $x < $cnt; $x++) {
                  $delivery_fee += 10;
               }
            }
         } elseif (in_array("San Pablo", $cities)) {
            $delivery_fee = 50;
            $tmp = array_count_values($cities);
            $cnt = $tmp["San Pablo"];
            if ($cnt > 1) {
               for ($x = 1; $x < $cnt; $x++) {
                  $delivery_fee += 10;
               }
            }
         } elseif (in_array("Rizal", $cities)) {
            $tmp = array_count_values($cities);
            $cnt = $tmp["Rizal"];
            if ($cnt > 1) {
               for ($x = 1; $x < $cnt; $x++) {
                  $delivery_fee += 10;
               }
            }
         }
         $order_price_details["delivery_fee"] = $delivery_fee;
         $order_price_details["total"] = $order_price_details["delivery_fee"] + $order_price_details["sub_total"];
      }
      echo json_encode(array("order_price_details" => $order_price_details));
   }
}
