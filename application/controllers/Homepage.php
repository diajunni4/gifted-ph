<?php
defined('BASEPATH') or exit('No direct script access allowed');

class homepage extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model("Global_model");
	}

	public function index()
	{
		$this->load->view('templates/homepage_template');
	}

	public function how_it_works()
	{
		$filter = ["is_featured"=>1,"is_active"=>1];
		$data['featured_brands'] = $this->Global_model->fetch('brands',$filter);
		$this->load->view('templates/how_it_works_template',$data);
	}

	public function check_login_status()
	{
		print_r($_SESSION);
		print_r($_COOKIE);
		die(var_dump(is_logged()));
	}

	public function faq()
	{
		$this->load->view('templates/faq_template');
	}
	
}
