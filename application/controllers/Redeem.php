<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Redeem extends CI_Controller
{
   public function __construct()
   {
      parent::__construct();
      date_default_timezone_set('Asia/Manila');
      $this->load->model("Global_model");
   }

   public function index()
   {
      $this->load->view('templates/redeem_template');
   }

   public function validate_voucher()
   {
      $response = ["message" => "success"];
      $voucher_code = clean_data(post('voucher_code'));
      $redemption_branch_name = clean_data(post('redemption_branch_name'));
      $filter = ["voucher_code" => $voucher_code];
      $row = $this->Global_model->fetch_tag_row('is_redeemed', 'vouchers', $filter);
      if (empty($row)) {
         $response = ["message" => "Invalid Voucher"];
      } else {
         $is_redeemed = $row->is_redeemed;
         if ($is_redeemed == 1) {
            $response = ["message" => "Voucher is already been redeemed"];
         } else {
            $data = ["is_redeemed" => 1,"date_redeemed"=>date("Y-m-d h:i:sa"),"redemption_branch_name"=>$redemption_branch_name];
            $this->Global_model->update('vouchers', $data, $filter);
         }
      }
      echo json_encode($response);
   }
}
