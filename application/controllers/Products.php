<?php
defined('BASEPATH') or exit('No direct script access allowed');

class products extends CI_Controller
{

   public function __construct()
   {
      parent::__construct();
      date_default_timezone_set('Asia/Manila');
      $this->load->model("Global_model");
   }

   public function index()
   {
      $this->load->view('templates/products_template');
   }

   public function get_products()
   {
      $order_by = "is_best_seller desc";
      $filter = ["enabled" => 1];
      $data['products'] = $this->Global_model->fetch('products', $filter, "", "", $order_by);
      echo json_encode($data);
   }

   public function get_brands()
   {
      $order_by = "is_featured desc";
      $filter = ["is_active" => 1];
      $data['brands'] = $this->Global_model->fetch_tag('name,image,is_featured,,location,slug,brand_id,category,is_egc','brands', $filter, "", "", $order_by);
      echo json_encode($data);
   }

   public function get_categories()
   {
      $filter = ["status" => 1];
      $data['categories'] = $this->Global_model->fetch('categories', $filter);
      echo json_encode($data);
   }


   public function view_product($slug)
   {
      $data['is_logged'] = is_logged();
      $filter = ["slug" => $slug];
      $data['brands'] = $this->Global_model->fetch_tag_row('*','brands', $filter);
      if(!empty($data['brands']))
      $this->load->view('templates/product_template', $data); 
      else 
         redirect(base_url());
   }

   public function get_product()
   {
      $slug = clean_data(post('slug'));
      $filter = ['slug' => $slug];
      $data['product'] = $this->Global_model->fetch_tag_row('*', 'products', $filter);
      echo json_encode($data);
   }

   public function get_products_by_brand_id()
   {
      $brand_id = clean_data(post('brand_id'));
      $filter = ['brand_id' => $brand_id];
      $data['products'] = $this->Global_model->fetch('products', $filter);
      echo json_encode($data);
   }

   public function get_brand()
   {
      $slug = clean_data(post('slug'));
      $filter = ['slug' => $slug];
      $data['brand'] = $this->Global_model->fetch_tag_row('*', 'brands', $filter);
      echo json_encode($data);
   }

   public function get_brand_and_products()
   {
      $slug = clean_data(post('slug'));
      $filter = ['slug' => $slug];
      $data['brand'] = $this->Global_model->fetch_tag_row('*', 'brands', $filter);
      $filter = ['brand_id' => $data['brand']->brand_id];
      $data['products'] = $this->Global_model->fetch('products', $filter);

      echo json_encode($data);
   }
}
