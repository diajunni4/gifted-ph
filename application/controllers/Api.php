<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Dompdf\Dompdf;

use Dompdf\Options;

class api extends CI_Controller
{

   public function __construct()
   {
      parent::__construct();
      date_default_timezone_set('Asia/Manila');
      $this->load->model("Global_model");
      $this->load->model("Api_model");
   }

   public function check_voucher()
   {
      $voucher_code = get('voucher_code');
      $redemption_branch_name = get('redemption_branch_name');
      $filter = ["voucher_code" => $voucher_code];
      $row = $this->Global_model->fetch_tag_row('*', "vouchers", $filter);
      $response = ["message" => "Invalid Voucher"];
      if (!empty($row)) {
         if ($row->is_redeemed == 0) {
            //if valid yung voucher
            $date = date('Y-m-d H:i:s');
            $update_data = ["is_redeemed" => 1, 'date_redeemed' => $date, "redemption_branch_name" => $redemption_branch_name];
            $filter = ["voucher_code" => $voucher_code];
            $this->Global_model->update('vouchers', $update_data, $filter);
            $response["message"] = "valid";
            $response["voucher_details"] = json_encode($row);
         } else {
            $response["message"] = "Voucher Already Redeemed";
         }
      }

      echo json_encode($response);
   }

   public function test($voucher)
   {
      $data['voucher_details'] = $this->Api_model->get_voucher_details($voucher);
      //die(print_r($data));
      $voucher_template = $this->load->view('pages/pdfs/voucher', $data);
      // require_once(APPPATH . 'third_party/dompdf/autoload.inc.php');
      // require_once(APPPATH . 'third_party/dompdf/lib/html5lib/Parser.php');
      // require_once(APPPATH . 'third_party/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php');
      // require_once(APPPATH . 'third_party/dompdf/lib/php-svg-lib/src/autoload.php');
      // require_once(APPPATH . 'third_party/dompdf/src/Autoloader.php');
      // // instantiate and use the dompdf class
      // $dompdf = new Dompdf();
      // $dompdf->loadHtml($voucher_template);

      // // (Optional) Setup the paper size and orientation
      // $dompdf->setPaper('A4', 'portrait');

      // // Render the HTML as PDF
      // $dompdf->render();

      // // Output the generated PDF to Browser
      // //$dompdf->stream();
      // $dompdf->stream($voucher . ".pdf", array('compress' => 0));
   }

   public function get_all_canned_messages() {
      $data['canned_messages'] = $this->Global_model->fetch('canned_messages');
      echo json_encode($data);
   }

   public function get_brands() {
      $searchText = clean_data(get('searchText'));
      $data['brands'] = $this->Api_model->search_brand($searchText);
		echo json_encode($data);
	}

   public function get_subcategories() {
      $brand_id = clean_data(post('brand_id'));
      $row = $this->Api_model->get_brand_subcategory($brand_id);
      $data['sub_categories'] = [];
      if(!empty($row)) {
         $data['sub_categories'] = explode(';',$row->tags);
      }
      echo json_encode($data);
   }

   public function update_brands_egc() {
      $physical_gc = $this->Global_model->fetch('products',['category'=>"Physical GC"]);
      foreach($physical_gc as $item) {
         $brand_id = $item->brand_id;
         $this->Global_model->update('brands',["is_egc"=>0],['brand_id'=>$brand_id]);
      }
   }

   public function downloadVoucher($voucher)
   {
      $data['voucher_details'] = $this->Api_model->get_voucher_details($voucher);
      //die(print_r($data));
      $voucher_template = $this->load->view('pages/pdfs/voucher', $data, true);
      require_once(APPPATH . 'third_party/dompdf/autoload.inc.php');
      require_once(APPPATH . 'third_party/dompdf/lib/html5lib/Parser.php');
      require_once(APPPATH . 'third_party/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php');
      require_once(APPPATH . 'third_party/dompdf/lib/php-svg-lib/src/autoload.php');
      require_once(APPPATH . 'third_party/dompdf/src/Autoloader.php');
      // instantiate and use the dompdf class
      $dompdf = new Dompdf();
      $dompdf->loadHtml($voucher_template);
      $options = new Options();
      $options->setIsRemoteEnabled(true);
      $dompdf->setOptions($options);
      // (Optional) Setup the paper size and orientation
      $dompdf->setPaper('A4', 'portrait');

      // Render the HTML as PDF
      $dompdf->render();

      // Output the generated PDF to Browser
      //$dompdf->stream();
      $dompdf->stream($voucher . ".pdf", array('compress' => 0));
   }

   public function downloadGreetingCard($order_number)
   {
      $data['order_details'] = $this->Api_model->get_order_details($order_number);
      //die(print_r($data));
      $greetings_template = $this->load->view('pages/pdfs/greeting_cards', $data, true);
      require_once(APPPATH . 'third_party/dompdf/autoload.inc.php');
      require_once(APPPATH . 'third_party/dompdf/lib/html5lib/Parser.php');
      require_once(APPPATH . 'third_party/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php');
      require_once(APPPATH . 'third_party/dompdf/lib/php-svg-lib/src/autoload.php');
      require_once(APPPATH . 'third_party/dompdf/src/Autoloader.php');
      // instantiate and use the dompdf class
      $dompdf = new Dompdf();
      $dompdf->loadHtml($greetings_template);
      $options = new Options();
      $options->setIsRemoteEnabled(true);
      $dompdf->setOptions($options);
      // (Optional) Setup the paper size and orientation
      $dompdf->setPaper('A4', 'portrait');

      // Render the HTML as PDF
      $dompdf->render();

      // Output the generated PDF to Browser
      //$dompdf->stream();
      $dompdf->stream($order_number . ".pdf", array('compress' => 0));
   }

   public function test2($order_number)
   {
      $data['order_details'] = $this->Api_model->get_order_details($order_number);
      //die(print_r($data));
      $voucher_template = $this->load->view('pages/pdfs/greeting_cards', $data);
   }
}
