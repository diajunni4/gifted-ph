<?php
defined('BASEPATH') or exit('No direct script access allowed');

class wallet extends CI_Controller
{

   public function __construct()
   {
      parent::__construct();
      date_default_timezone_set('Asia/Manila');
      $this->load->model("Global_model");
      if (!is_logged()) {
         redirect(base_url());
      }
   }

   public function index()
   {
      $this->load->view('templates/wallet_template');
   }

   public function request_load()
   {
      $response = [];
      try {
         $email = get_email();
         $order_key = random_code(8);
         do {
            $filter =  ["order_key" => $order_key];
            $check_exist = $this->Global_model->check_exist("wallet_order", $filter);
         } while ($check_exist > 0);
         $amount = clean_data(post('amount'));
         $full_address = clean_data(post('full_address'));
         $payment_method = clean_data(post('payment_method'));

         $transfer_to = clean_data(post('payment_method'));
         $data = ["order_key"=>$order_key,'email' => $email, 'amount' => $amount, "full_address" => $full_address, "payment_method" => $payment_method,'tranfer_to',$transfer_to];
         $this->Global_model->insert('wallet_order', $data);
         $response = array('message' => "success");
      } catch (Exception $e) {
         $response = array('message' => "failed", 'error_msg' => $e->getMessage());
      }
      echo json_encode($response);
   }

   public function cancel_order()
   {
         $order_key = clean_data(post('order_key'));
         $email = get_email();
         $filter = ["order_key"=>$order_key,'email'=>$email];
         $data = ["status"=>"Cancelled"];
         $status = $this->Global_model->update('wallet_order',$data,$filter);
         echo json_encode(["message"=>$status]);
     
   }

   public function get_user_wallet_orders()
   {
      $email = get_email();
      $data = [];
      if ($email) {
         $filter = ["email" => $email];
         $data['orders'] = $this->Global_model->fetch('wallet_order', $filter, "", "", "created_at desc");
      }
      echo json_encode($data);
   }
}
