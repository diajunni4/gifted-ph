<?php
defined('BASEPATH') or exit('No direct script access allowed');

class checkout extends CI_Controller
{

   public function __construct()
   {
      parent::__construct();
      date_default_timezone_set('Asia/Manila');
      $this->load->model("Global_model");
      $this->load->model("Cart_model");
      if (!is_logged()) {
         redirect(base_url());
      }
   }

   public function index()
   {
      if ($this->session->tmp_order_details) {
         $tmp_order_details = $this->session->tmp_order_details;
         $new_tmp_order_details = [];
         //reassign the session array
         foreach ($tmp_order_details as $key => $value) {
            foreach ($value as $new_key => $new_value) {
               $new_tmp_order_details[$new_key] = $new_value;
            }
         }

         $data['brand_name'] = $new_tmp_order_details['brand_name'];
         $data['brand_slug'] = $new_tmp_order_details['brand_slug'];

         $this->load->view('templates/checkout_template', $data);
      } else {
         redirect(base_url());
      }
   }

   public function test_order_details()
   {
      print_r($this->session->tmp_order_details);
   }

   public function apply_promo()
   {
      $order_price_details['message'] = "error";
      $promo = clean_data(post('promo'));
      $filter = ["code" => $promo, "status" => 1, "limit >" => 0];
      $row = $this->Global_model->fetch_tag_row('amount', 'promo_codes', $filter);
      if (!empty($row)) {
         $amount = $row->amount;
         $order_price_details = $this->get_product_checkout_info($amount, $promo);
         $order_price_details['message'] = "success";
      }

      echo json_encode($order_price_details);
   }

   public function get_product_checkout_info($promo = 0, $promo_code = "")
   {
      $tmp_order_details = $this->session->tmp_order_details;
      $new_tmp_order_details = [];
      //reassign the session array
      foreach ($tmp_order_details as $key => $value) {
         foreach ($value as $new_key => $new_value) {
            $new_tmp_order_details[$new_key] = $new_value;
         }
      }
      $brand_id = $new_tmp_order_details['brand_id'];
      $email = get_email();
      $row  =  $this->Cart_model->get_discount($email);
      $order_price_details['discount_percent'] = 0;
      $discount = 0;
      if (!empty($row)) {
         if ($row->discounts != "" && $row->company != "") {
            $discounts = json_decode($row->discounts);
            //find discount
            $result = current(array_filter($discounts, function ($e) use ($brand_id) {
               return $e->id == $brand_id;
            }));
            $discount = $result->discount / 100;
            $order_price_details['discount_percent'] =  $result->discount;
         }
      }

      $product_id = $new_tmp_order_details['product_id'];
      //get product details
      $filter = ['id' => $product_id];
      $product_details = $this->Global_model->fetch_tag_row('*', 'products', $filter);
      $order_price_details['product_category'] = $product_details->category;
      $product_name = $product_details->name;
      $payment_fee = $product_details->payment_fee / 100;
      $payment_vat = $product_details->payment_vat / 100;


      $filter = ['brand_id' => $brand_id];
      $brand_details = $this->Global_model->fetch_tag_row('merchant_fee,tax_merchant_vat,affiliate_commission', 'brands', $filter);
      $merchant_fee = $brand_details->merchant_fee / 100;
      $tax_merchant_vat = $brand_details->tax_merchant_vat / 100;
      $order_price_details['affiliate_commission'] = $brand_details->affiliate_commission;

      //number of quantity
      $quantity = $new_tmp_order_details['quantity'];
      //get the price of the voucher
      $price = $product_details->saleprice > 0 ?  $product_details->saleprice :  $product_details->price;

      $order_price_details['delivery_charge'] = $product_details->free_shipping;
      $order_price_details['service_fee'] = ($price * $quantity) * $merchant_fee;
      $order_price_details['sub_total'] = $price * $quantity;
      $order_price_details['discount'] = $order_price_details['sub_total'] * $discount;
      $order_price_details['tmp_sub_total'] = ($price * $quantity) - $promo + $order_price_details['service_fee'] + $order_price_details['delivery_charge'] - $order_price_details['discount'];
      $order_price_details['vat'] = 0;
      $order_price_details['vatable_sales'] = round($order_price_details['tmp_sub_total'] - ($price * $quantity), 2);
      if ($order_price_details['vatable_sales'] >= 0) {
         $order_price_details['vat'] = round($order_price_details['vatable_sales'] * $payment_vat, 2);
      }

      $order_price_details['quantity'] = $quantity;
      $order_price_details['product_name'] = $product_name;

      //customer total
      $order_price_details['total'] = round($order_price_details['tmp_sub_total']  + $order_price_details['vat'], 2);
      $order_price_details['promo'] = $promo;
      $order_price_details['promo_code'] = $promo_code;
      //merchant total
      if ($payment_fee > 0) {
         $order_price_details['merchant_fee'] = $order_price_details['tmp_sub_total'] * $merchant_fee;
         $order_price_details['merchant_fee_vat_sales'] = round($order_price_details['merchant_fee'] / 1.12, 2);
         $order_price_details['merchant_fee_vat'] = $order_price_details['merchant_fee'] - $order_price_details['merchant_fee_vat_sales'];
         $order_price_details['merchant_total'] = $order_price_details['tmp_sub_total'] - $order_price_details['merchant_fee'];
         $order_price_details['withholding_tax'] = round($order_price_details['merchant_fee_vat_sales'] * 0.02, 2);
         $order_price_details['merchant_total_and_witholding'] = round($order_price_details['merchant_total'] - $order_price_details['withholding_tax'], 2);
      }

      if ($promo > 0) {
         return $order_price_details;
      } else {
         echo json_encode($order_price_details);
      }
   }

   private function get_order_price_info($promo_code)
   {
      $promo = 0;
      if ($promo_code != "") {
         $filter = ["code" => $promo_code, "status" => 1, "limit >" => 0];
         $row = $this->Global_model->fetch_tag_row('amount', 'promo_codes', $filter);
         if (!empty($row) && $promo_code != "") {
            $promo = $row->amount;
         } else {
            return "invalid coupon";
         }
      }

      $tmp_order_details = $this->session->tmp_order_details;
      $new_tmp_order_details = [];
      //reassign the session array
      foreach ($tmp_order_details as $key => $value) {
         foreach ($value as $new_key => $new_value) {
            $new_tmp_order_details[$new_key] = $new_value;
         }
      }

      $email = get_email();
      $brand_id = $new_tmp_order_details['brand_id'];
      $row  =  $this->Cart_model->get_discount($email);
      $discount = 0;
      $order_price_details['discount_percent'] = 0;
      if ($row->discounts != "" && $row->company != "") {
         $discounts = json_decode($row->discounts);
         //find discount
         $result = current(array_filter($discounts, function ($e) use ($brand_id) {
            return $e->id == $brand_id;
         }));
         $discount = $result->discount / 100;
         $order_price_details['discount_percent'] =  $result->discount;
      }

      $product_id = $new_tmp_order_details['product_id'];
      //number of quantity
      $quantity = $new_tmp_order_details['quantity'];
      $filter = ["id" => $product_id];
      //get product details
      $product_details = $this->Global_model->fetch_tag_row('*', 'products', $filter);
      $payment_fee = $product_details->payment_fee / 100;
      $payment_vat = $product_details->payment_vat / 100;


      $filter = ['brand_id' => $brand_id];
      $brand_details = $this->Global_model->fetch_tag_row('merchant_fee,tax_merchant_vat,affiliate_commission,voucher_character_length', 'brands', $filter);
      $merchant_fee = $brand_details->merchant_fee / 100;
      $tax_merchant_vat = $brand_details->tax_merchant_vat / 100;
      $order_price_details['voucher_character_length'] = $brand_details->voucher_character_length;
      //customer total
      //get the price of the voucher
      $price = $product_details->saleprice > 0 ?  $product_details->saleprice :  $product_details->price;
      $order_price_details['sub_total'] = ($price * $quantity);
      $order_price_details['discount'] = $order_price_details['sub_total'] * $discount;
      $order_price_details['delivery_charge'] = $product_details->free_shipping;
      $order_price_details['service_fee'] = $order_price_details['sub_total'] * $merchant_fee;
      $order_price_details['tmp_sub_total'] = ($price * $quantity) - $promo + $order_price_details['service_fee'] + $order_price_details['delivery_charge'] - $order_price_details['discount'];
      $order_price_details['vat'] = 0;
      $order_price_details['vatable_sales'] = 0;
      $order_price_details['vatable_sales'] = round(($order_price_details['tmp_sub_total']) - ($price * $quantity), 2);
      if ($order_price_details['vatable_sales'] >= 0) {
         $order_price_details['vat'] = round($order_price_details['vatable_sales'] * $payment_vat, 2);
      }

      $order_price_details['total'] = round($order_price_details['tmp_sub_total'] + $order_price_details['vat'], 2);
      //compute for merchant total
      if ($payment_fee > 0) {
         $order_price_details['merchant_fee'] = $order_price_details['tmp_sub_total'] * $merchant_fee;
         $order_price_details['merchant_fee_vat_sales'] = round($order_price_details['merchant_fee'] / 1.12, 2);
         $order_price_details['merchant_fee_vat'] = $order_price_details['merchant_fee'] - $order_price_details['merchant_fee_vat_sales'];
         $order_price_details['merchant_total'] = $order_price_details['tmp_sub_total'] - $order_price_details['merchant_fee'];
         $order_price_details['withholding_tax'] = round($order_price_details['merchant_fee_vat_sales'] * 0.02, 2);
         $order_price_details['merchant_total_and_witholding'] = round($order_price_details['merchant_total'] - $order_price_details['withholding_tax'], 2);
      }

      //reference database
      $order_price_details['merchant_fee'] = $brand_details->merchant_fee;
      $order_price_details['tax_merchant_vat'] = $brand_details->tax_merchant_vat;
      $order_price_details['payment_fee'] = $product_details->payment_fee;
      $order_price_details['payment_vat'] = $product_details->payment_vat;
      $order_price_details['promo'] = $promo;
      $order_price_details['promo_code'] = $promo_code;
      $order_price_details['affiliate_commission'] = $brand_details->affiliate_commission;
      return $order_price_details;
   }

   public function get_tmp_order_details()
   {
      echo json_encode($this->session->tmp_order_details);
   }

   public function get_user_details()
   {
      $email = get_email();
      if ($email) {
         $filter = ["email" => $email, "isSaveAddress" => 1];
         $data["order_details"] = $this->Global_model->fetch_tag_row('billing_address,shipping_address,delivery_option,payment_method', 'orders', $filter, "", "", "id desc");
         $filter = ["email" => $email];
         $data["user_details"] = $this->Global_model->fetch_tag_row('wallet', 'users', $filter);
         $data['countries'] = $this->Global_model->fetch('countries');
      }
      echo json_encode($data);
   }



   public function proceed_order()
   {
      $promo_code = clean_data(post('promo'));
      //get order price details
      $order_price_details = $this->get_order_price_info($promo_code);
      $email = get_email();
      $payment_method = clean_data(post('payment_method'));
      $total = isset($order_price_details['total']) ? $order_price_details['total'] : 0;
      $response = [];
      if ($payment_method == "gifted_wallet") {
         $filter = ["email" => $email];
         $row = $this->Global_model->fetch_tag_row('wallet', 'users', $filter);
         $wallet = $row ? $row->wallet : 0;
         if ($wallet < $total) {
            $response['message'] = "Insufficient credits";
         }
      } else {
         if ($order_price_details != "invalid coupon") {
            $tmp_order_details = $this->session->tmp_order_details;
            $tmp_gift_to_details = $this->session->gift_to_details == null ? '' : $this->session->gift_to_details;
            $new_tmp_order_details = [];
            //reassign the session array
            foreach ($tmp_order_details as $key => $value) {
               foreach ($value as $new_key => $new_value) {
                  $new_tmp_order_details[$new_key] = $new_value;
               }
            }
            $voucher_character_length = $order_price_details['voucher_character_length'];
            $payment_method = clean_data(post('payment_method'));
            $tmp_billing_address = clean_data_arr(post('billing_address'));
            $tmp_shipping_information = clean_data_arr(post('shipping_information'));
            $send_later = clean_data(post('send_later'));
            $delivery_option = clean_data(post('delivery_option'));
            $isSaveAddress = clean_data(post('isSaveAddress'));
            $brand_id = $new_tmp_order_details['brand_id'];
            $product_id = $new_tmp_order_details['product_id'];
            $greeting_card_id = $new_tmp_order_details['greeting_card_id'];
            $quantity = $new_tmp_order_details['quantity'];
            $vouchers = [];
            $voucher_data = [];
            //order unique
            $order_number = generateOrderNumber();

            //generate vouchers 
            $filter = ["is_redeemed" => 0, "order_number" => ""];
            for ($x = 0; $x < $quantity; $x++) {
               $voucher_code = random_code($voucher_character_length);
               $row = $this->Global_model->fetch_tag_row('voucher_code', 'vouchers', $filter);
               if (!empty($row)) {
                  $voucher_code = $row->voucher_code;
                  $update_voucher_data = ["order_number" => $order_number];
                  $this->Global_model->update('vouchers', $update_voucher_data, ["voucher_code" => $voucher_code]);
               } else {
                  do {
                     $filter =  ["voucher_code" => $voucher_code];
                     $check_exist = $this->Global_model->check_exist("vouchers", $filter);
                  } while ($check_exist > 0);

                  $voucher_data[] = ["voucher_code" => $voucher_code, "order_number" => $order_number, "brand_id" => $brand_id];
               }
               array_push($vouchers, $voucher_code);
            }



            $sub_total = $order_price_details['sub_total'];
            $delivery_charge = $order_price_details['delivery_charge'];

            $vatable_sales = $order_price_details['vatable_sales'];
            $vat = $order_price_details['vat'];

            $merchant_fee_total = isset($order_price_details['merchant_fee_total']) ? $order_price_details['merchant_fee_total'] : 0;
            $merchant_fee_vat_sales = isset($order_price_details['merchant_fee_vat_sales']) ? $order_price_details['merchant_fee_vat_sales'] : 0;
            $merchant_fee_vat = isset($order_price_details['merchant_fee_vat']) ? $order_price_details['merchant_fee_vat'] : 0;
            $merchant_total = isset($order_price_details['merchant_total']) ?  $order_price_details['merchant_total'] : 0;
            $withholding_tax = isset($order_price_details['withholding_tax']) ? $order_price_details['withholding_tax'] : 0;
            $merchant_total_and_witholding = isset($order_price_details['merchant_total_and_witholding']) ? $order_price_details['merchant_total_and_witholding'] : 0;
            $merchant_fee = isset($order_price_details['merchant_fee']) ? $order_price_details['merchant_fee'] : 0;
            $tax_merchant_vat = isset($order_price_details['tax_merchant_vat']) ? $order_price_details['tax_merchant_vat'] : 0;
            $payment_fee = isset($order_price_details['payment_fee']) ? $order_price_details['payment_fee'] : 0;
            $payment_vat = isset($order_price_details['payment_vat']) ? $order_price_details['payment_vat'] : 0;
            $service_fee = isset($order_price_details['service_fee']) ? $order_price_details['service_fee'] : 0;
            $promo = $order_price_details['promo'];
            $promo_code = $order_price_details['promo_code'];
            $affiliate_commission = $order_price_details['affiliate_commission'];
            $discount = $order_price_details['discount'];
            $discount_percent = $order_price_details['discount_percent'];


            $status = false;
            $billing_address = new stdClass();
            $shipping_address = new stdClass();
            $gift_to_details = new stdClass();

            foreach ($tmp_billing_address as $tmp_arr => $tmp_new_arr) {
               $new_arr = $tmp_new_arr;
               foreach ($new_arr as $key => $value) {
                  $billing_address->$key = clean_data($value);
               }
            }

            foreach ($tmp_shipping_information as $tmp_arr => $tmp_new_arr) {
               $new_arr = $tmp_new_arr;
               foreach ($new_arr as $key => $value) {
                  $shipping_address->$key = clean_data($value);
               }
            }

            if (!empty($tmp_gift_to_details)) {
               foreach ($tmp_gift_to_details as $tmp_arr => $tmp_new_arr) {
                  $new_arr = $tmp_new_arr;
                  foreach ($new_arr as $key => $value) {
                     $gift_to_details->$key = clean_data($value);
                  }
               }
            }
            // $vatable_sales = $order_price_details['vatable_sales'];
            // $vat = $order_price_details['vat'];
            // $merchant_fee_total = $order_price_details['merchant_fee_total'];
            // $merchant_fee_vat_sales = $order_price_details['merchant_fee_vat_sales'];
            // $merchant_fee_vat = $order_price_details['merchant_fee_vat'];
            // $merchant_total = $order_price_details['merchant_total'];
            // $withholding_tax = $order_price_details['withholding_tax'];
            // $merchant_total_and_witholding = $order_price_details['merchant_total_and_witholding'];

            $order = [
               "order_number" => $order_number,
               "product_id" => $product_id,
               "greeting_card_id" => $greeting_card_id,
               "quantity" => $quantity,
               "brand_id" => $brand_id,
               "sub_total" => $sub_total,
               "delivery_charge" => $delivery_charge,
               "total" => $total,
               "payment_method" => $payment_method,
               "vouchers" => json_encode($vouchers),
               "gift_to_details" => json_encode($gift_to_details),
               "billing_address" => json_encode($billing_address),
               "shipping_address" => json_encode($shipping_address),
               "delivery_option" => $delivery_option,
               "send_later" => $send_later,
               "email" => $email,
               "merchant_fee" => $merchant_fee,
               "affiliate_commission" => $affiliate_commission,
               "tax_merchant_vat" => $tax_merchant_vat,
               "payment_fee" => $payment_fee,
               "payment_vat" => $payment_vat,
               "vatable_sales" => $vatable_sales,
               "service_fee" => $service_fee,
               "vat" => $vat,
               "merchant_fee_total" => $merchant_fee_total,
               "merchant_fee_vat_sales" => $merchant_fee_vat_sales,
               "merchant_fee_vat" => $merchant_fee_vat,
               "merchant_total" => $merchant_total,
               "withholding_tax" => $withholding_tax,
               "merchant_total_and_witholding" => $merchant_total_and_witholding,
               "promo" => $promo,
               "promo_code" => $promo_code,
               "discount" => $discount,
               "discount_percent" => $discount_percent,
               "isSaveAddress" => $isSaveAddress
            ];

            //$status =  $this->sendConfirmationEmail($order);
            $status = $this->Global_model->insert('orders', $order);
            if ($status) {
               //insert vouchers
               $this->Global_model->insert_batch('vouchers', $voucher_data);
               //deduct promo limit
               if ($promo_code) {
                  $this->Cart_model->update_promo($promo_code);
               }
               //gifted wallet deduction
               if ($payment_method == "gifted_wallet") {
                  $total_wallet = $wallet - $total;
                  $filter = ["email" => $email];
                  $update_wallet = ["wallet" => $total_wallet];
                  $this->Global_model->update('users', $update_wallet, $filter);
               }

               $this->session->unset_userdata('tmp_order_details');
               $this->session->unset_userdata('gift_to_details');
               $response['message'] = "success";
               $response['order_number'] = $order_number;
               //$email_status = $this->sendConfirmationEmail($order_number);
            } else {
               $response['message'] = "error";
            }
         } else {
            $response['message'] = "invalid coupon";
         }
      }

      echo json_encode($response);
   }

   private function sendConfirmationEmail($order_number)
   {
      $email = get_email();
      $this->load->model("Api_model");
      $data['order_details'] = $this->Api_model->get_order_details($order_number);
      $email_body = $this->load->view('emails/order_receipt', $data, true);
      $email_content = [
         'from' => 'info@gifted.ph',
         'from_name' => "Gifted.PH",
         'to' => $email,
         'subject' => 'Thank you for your order with Gifted! - #' . $order_number,
         'message' => $email_body
      ];

      $isSent = sendMailCIMailer($email_content);
      return $isSent;
   }

   function testsendEmail()
   {
      $order_number = "2020032415565765";
      $email = get_email();
      $this->load->model("Api_model");
      $data['order_details'] = $this->Api_model->get_order_details($order_number);
      $email_body = $this->load->view('emails/order_receipt', $data, true);
      $email_content = [
         'from' => 'info@gifted.ph',
         'from_name' => "Gifted.PH",
         'to' => "jonasdulay28@gmail.com",
         'subject' => 'Thank you for your order with Gifted! - #' . $order_number,
         'message' => $email_body
      ];

      $isSent = sendMailCIMailer($email_content);
      echo $isSent;
   }

   function testsendEmail2()
   {
      $order_number = "2020032415565765";
      $email = get_email();
      $this->load->model("Api_model");
      $data['order_details'] = $this->Api_model->get_order_details($order_number);
      $email_body = "wew";
      $email_content = [
         'from' => 'info@gifted.ph',
         'from_name' => "Gifted.PH",
         'to' => "jonasdulay28@gmail.com",
         'subject' => 'Thank you for your order with Gifted! - #' . $order_number,
         'message' => $email_body
      ];

      $isSent = sendMailCIMailer($email_content);
      echo $isSent;
   }

   function testEmail()
   {
      $order_number = "2020032317332579";
      $email = get_email();
      $this->load->model("Api_model");
      $data['order_details'] = $this->Api_model->get_order_details($order_number);
      $this->load->view('emails/order_receipt', $data);
   }
}
