<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class brands_api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Global_model');
		if(is_logged_admin() == 0){
			redirect(base_url());
		}
	}
	
	public function get_brands() {
      $data['brands'] = $this->Global_model->fetch('brands',"","","","is_active desc");
		echo json_encode($data);
	}


	public function add()
	{
		$response = ["message"=>"success"];
		$company_data = (array)  json_decode(post('company_data'));
      $company_data['discounts'] = json_encode($company_data['discounts']);
		$this->User_model->insert('company_clients',$company_data);
		echo json_encode($response);
   }
   
   public function get_company_discount() {
      $company_id = clean_data(post('company_id'));
      $filter = ["id"=>$company_id];
      $row = $this->Global_model->fetch_tag_row('discounts','company_clients',$filter);
      $data["discounts"] = $row->discounts;
      echo json_encode($data);
   }

	public function get_company(){
		$id = clean_data(rawurldecode(get('q')));
		$filter = ["id"=>$id];
      $data["company"] = $this->Global_model->fetch_tag_row('*','company_clients',$filter);
      $data["brands"] = $this->Global_model->fetch('brands');
      $filter = ["discounts !="=> "", "id !="=>$id];
      $data["companies"] = $this->Global_model->fetch_tag('id,company_name','company_clients',$filter);
		echo json_encode($data);
   }
   
   public function set_company() {
      $data["brands"] = $this->Global_model->fetch('brands');
      $filter = ["discounts !="=> ""];
      $data["companies"] = $this->Global_model->fetch_tag('id,company_name','company_clients',$filter);
		echo json_encode($data);
   }

	public function edit()
	{
		$response = ["message"=>"success"];
      $company_data = (array)  json_decode(post('company_data'));
      $company_data['discounts'] = json_encode($company_data['discounts']);
		$id = clean_data(post('id'));
		
		$filter = ["id"=>$id]; 
		$this->Global_model->update('company_clients',$company_data,$filter);
		
		echo json_encode($response);
	}

	public function delete()
	{
		$email = clean_data(post('email'));
		$filter = ["email"=>$email]; 
		$this->User_model->delete('users',$filter);
		echo json_encode($response);
	}

	public function changeStatus(){
		$email = clean_data(post('email'));
		$status = clean_data(post('status'));
		$status = !$status;
		$data = ["status"=>$status];
		$filter = ["email"=>$email]; 
		$this->User_model->update('users',$data,$filter);
		audit("logs","changeStatus: ".$email,json_encode($data),"success");
		echo json_encode($response);
	}
}