<?php
defined('BASEPATH') or exit('No direct script access allowed');

class orders extends CI_Controller
{

   public function __construct()
   {
      parent::__construct();
      date_default_timezone_set('Asia/Manila');
      $this->load->model("Global_model");
   }

   public function index()
   {
      //  die(generate_invoice_num(9,7,"PBL-"));
      if (is_logged()) {
         $data['content'] = "pages/orders";
         $this->load->view('templates/orders_template', $data);
      } else {
         redirect(base_url());
      }
   }

   public function save_order() {
      $selected_product_details = clean_data_arr(post('selected_product_details'));
      $gift_to_details = post('gift_to_details') == null ? [] : clean_data_arr2(post('gift_to_details'));
      $sessdata['gift_to_details'] = $gift_to_details;
      $sessdata['tmp_order_details'] = $selected_product_details;
      $this->session->set_userdata($sessdata);
      echo json_encode(['message'=>'success']);
   }

   // public function get_order()
   // {
   //    $this->load->model("Cart_model");
   //    $order_key = clean_data(post('order_key'));
   //    $email = get_email();
   //    $data['product_details'] = $this->Cart_model->get_view_order($order_key, $email);
   //    $data["voucher"] = $data["product_details"]->voucher;
   //    $order_key = $data['product_details']->order_key;
   //    $data["additional_information"] = $data['product_details']->additional_information;
   //    $data['order_price_details'] = $this->get_order_price($data['product_details']->order);
   //    // if ($order_key != '')
   //    //    $this->session->set_userdata('order_key', encrypt($order_key));
   //    echo json_encode($data);
   // }
   

   public function get_user_orders()
   {
      $email = get_email();
      $data = [];
      if ($email) {
         $filter = ["email" => $email];
         $data['orders'] = $this->Global_model->fetch('orders', $filter, "", "", "created_at asc");
         $row = $this->Global_model->fetch_tag_row('wallet', 'users', $filter);
         $data['wallet'] = $row->wallet;
      }
      echo json_encode($data);
   }

   public function view_order($order_number)
   {
      if (is_logged()) {
         $this->load->model("Api_model");
         $data['order_number'] = $order_number;
         $data['order_details'] = $this->Api_model->get_order_details($order_number);
         $data['content'] = "pages/order";
         $this->load->view('templates/orders_template', $data);
      } else {
         redirect(base_url());
      }
   }

   public function cancel_order()
   {
      $order_key = clean_data(post('order_key'));
      $email = get_email();
      $filter = ["order_key" => $order_key, 'email' => $email];
      $data = ["status" => "Cancelled"];
      $status = $this->Global_model->update('orders', $data, $filter);
      echo json_encode(["message" => $status]);
   }
}
