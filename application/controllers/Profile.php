<?php
defined('BASEPATH') or exit('No direct script access allowed');

class profile extends CI_Controller
{

   public function __construct()
   {
      parent::__construct();
      date_default_timezone_set('Asia/Manila');
      $this->load->model("Global_model");
      if (!is_logged()) {
         redirect(base_url());
      }
   }

   public function index()
   {

      $this->load->view('templates/profile_template');
   }

   public function get_user_details()
   {
      $email = get_email();
      $filter = ["email" => $email];
      $data['user_details'] = $this->Global_model->fetch_tag_row('*', 'users', $filter);
      echo json_encode($data);
   }

   public function change_password()
   {
      try {
         $current_password = clean_data(post('password'));
         $new_password = hash_password(post('new_password'));
         $email = get_email();
         $filter = ["email" => $email];
         $row = $this->User_model->fetch_tag_row('password', 'users', $filter);
         $existing_password = $row->password;
         if (!password_verify($current_password, $existing_password)) {
            $data = array('status' => "failed", 'error_msg' => 'Current password does not match with existing password.');
            echo json_encode($data);
            die;
         }

         $acc_password = array(
            'password' => $new_password
         );
         $this->User_model->update('users', $acc_password, $filter);

         $data = array('message' => "success", 'msg' => 'Password updated.');
      } catch (Exception $e) {
         $data = array('message' => "failed", 'error_msg' => $e->getMessage());
      }

      echo json_encode($data);
   }

   public function update_user_details()
   {
      $email = get_email();
      $name = clean_data(post('name'));
      $contact_number = clean_data(post('contact_number'));
      $filter = ["email" => $email];
      $data = ["name" => $name, "contact_number" => $contact_number];
      $status = $this->Global_model->update('users', $data, $filter);
      echo json_encode(["message" => $status]);
   }
}
