<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class greeting_cards extends CI_Controller {

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Manila');
		$this->load->model("Global_model");
	}

	public function get_greeting_cards() {
		$filter = ["status"=>1];
      $data['greeting_cards'] = $this->Global_model->fetch('greeting_cards',$filter);
      echo json_encode($data);
   }
}