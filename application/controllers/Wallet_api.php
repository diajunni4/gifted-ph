<?php
defined('BASEPATH') or exit('No direct script access allowed');

class wallet_api extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Cart_Order_model');
		$this->load->model('Global_model');
		if (is_logged_admin() == 0) {
			redirect(base_url());
		}
	}

	public function get_order_details()
	{
		$id = get('id');
		if (!$id) show_404();

		$data = $this->Cart_Order_model->get($id);
		echo json_encode($data);
	}

	public function get_orders()
	{
		$sort = array();
		$sort['mode'] = 'live';
		if (isset($_GET['status']) && $_GET['status'] != 'all') {
			$sort['admin_status'] = $_GET['status'];
		}
		if (isset($_GET['payment_status']) && $_GET['payment_status'] != 'all') {
			$sort['status'] = $_GET['payment_status'];
		}
		if (isset($_GET['payment_mode']) && $_GET['payment_mode'] != 'all') {
			if ($_GET['payment_mode'] == 'paypal') $sort['payment_mode'] = 'Paypal';
			else if ($_GET['payment_mode'] == 'bank_transfer') $sort['payment_mode'] = 'Bank Transfer';
			else if ($_GET['payment_mode'] == 'pickup') $sort['payment_mode'] = 'Pickup';
		}
		if (isset($_GET['mode']) && $_GET['mode'] != 'Live') {
			$sort['mode'] = 'Test';
		}

		if (empty($sort)) {
			$data['orders'] = $this->Global_model->get();
		} else {
			$data['orders'] = $this->Global_model->fetch_tag_array('*', 'wallet_order', $sort, "", "", 'FIELD(status, "Pending", "Processing", "Completed","Refunded","Cancelled"), created_at desc');
			$data['orders'] = $data['orders'] ? $data['orders'] : [];
			$data['sort'] = $sort;
		}
		echo json_encode($data);
	}

	public function change_status()
	{
		$status = post('status');
		$id = post('id');
		$data = array('status' => $status);

		$this->Global_model->update('wallet_order', $data, array('id' => $id));
	}
}
