<?php
defined('BASEPATH') or exit('No direct script access allowed');
header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");
require_once APPPATH . "/third_party/LazadaOpenSDK/LazopSdk.php";
date_default_timezone_set('Asia/Manila');
class open_lazada extends CI_Controller
{
   public function __construct()
   {
      parent::__construct();
      $this->load->model('Cart_Order_model');
      $this->load->model('Global_model');
      if (is_logged_admin() == 0) {
         redirect(base_url());
      }
   }

   public function index()
   {
      //dito isang row na order from lazada
      $orders_data = $this->getOrders();
      $order_item_id = [];
      if ($orders_data->count > 0) {
         $order =  $orders_data->orders[0];
         //276310456593230
         // $order_number = "277165738111030";
         //$order_number = $order->order_number;
         $order_number = "276504404111030";
         $filter = ["order_number" => $order_number];
         $check_exist = $this->Global_model->check_exist('lazada_order_number', $filter);
         if (!$check_exist) {
            $insert_data = ["order_number" => $order_number];
            $this->Global_model->insert('lazada_order_number',$insert_data);
            //contents of orders from lazada
            $order_items = $this->getOrderItems($order_number);
            $tmp_sku = [];

            //get all sku
            foreach ($order_items as $key => $item) {
               array_push($tmp_sku, $item->sku);
               array_push($order_item_id, $item->order_item_id);
            }


            //to get quantity per sku
            $quantity_per_sku = array_count_values($tmp_sku);

            //assigning of orders per sku
            foreach ($quantity_per_sku as $key => $value) {
               $sku = $key;
               $quantity = $value;
               $tmp_order_items = [];
               foreach ($order_items as $key2 => $item) {
                  if ($item->status == 'pending') {
                     if ($item->sku == $sku) {
                        array_push($tmp_order_items, $item);
                     }
                  }
               }
               $order_item = $tmp_order_items[0];

               $email = $order_item->digital_delivery_info;

               //creating of account
               $this->createAccount($order, $email);

               //creating of order
               $billing_address = $this->get_billing_address($order, $email);

               $shipping_address = $billing_address;
               $merchant_id = get_sku_merchant_id($sku);
               $denomination = get_sku_denomination($sku);

               $new_order_key = generateRandomString(7);
               $new_order_number = generateOrderNumber();
               $filter = ["voucher_merchant_id" => $merchant_id, "voucher_denomination" => $denomination, "order_number" => $order_number];
               $tmp_data = json_decode($this->get_gifted_vouchers($filter));
               $tmp_vouchers = $tmp_data->vouchers;
               $voucher_length = $tmp_data->voucher_length;
               $description = $tmp_data->product_description;
               $image = $tmp_data->brand_image;
               $vouchers = [];
               $gifted_vouchers = [];
               //generate vouchers 
               for ($x = 0; $x < $quantity; $x++) {
                  $voucher_code = "";
                  if (empty($tmp_vouchers[$x])) {
                     do {
                        $voucher_code = random_code($voucher_length);
                        $filter =  ["voucher_code" => $voucher_code];
                        $check_exist = $this->Global_model->check_exist("vouchers", $filter);
                     } while ($check_exist > 0);
                     $voucher_data = ["voucher_code" => $voucher_code, "order_key" => $new_order_key, "order_number" => $new_order_number];
                     //create voucher  
                     $this->Global_model->insert('vouchers', $voucher_data);
                  } else {
                     //from live db
                     $voucher_code = $tmp_vouchers[$x]->voucher_code;
                     array_push($gifted_vouchers,$voucher_code);
                     //update na agad pag ka kuha
                     //lagay order number
                  }
                  array_push($vouchers, $voucher_code);
               }
               $item_price = $order_item->item_price;
               $sub_total = $item_price * $quantity;
               $total = $item_price * $quantity;

               $promo_terms = $this->get_promo_terms($sku);

               $save_order = [
                  "order_key" => $new_order_key,
                  "order_number" => $new_order_number,
                  "product_id" => "",
                  "greeting_card_id" => 3,
                  "quantity" => $quantity,
                  "brand_id" => "",
                  "merchant_id" => $merchant_id,
                  "sub_total" => $sub_total,
                  "total" => $total,
                  "delivery_charge" => 0,
                  "payment_method" => "Lazada website",
                  "vouchers" => json_encode($vouchers),
                  "gift_to_details" => "",
                  "billing_address" => json_encode($billing_address),
                  "shipping_address" => json_encode($shipping_address),
                  "email" => $email,
                  "delivery_option" => "send_email",
                  "status" => "Paid - Lazada",
                  "lazada_reference" => json_encode($order_item),
                  "lazada_main_order_reference" => json_encode($order),
                  "promo_terms" => $promo_terms,
                  "description" => $description,
                  "image" => $image
               ];

               $this->Global_model->insert('orders', $save_order);

               $email_data = [
                  "lastname" => getLastName(trim($order->customer_first_name)),
                  "total" => $total,
                  "order_number" => $new_order_number,
                  "vouchers" => $vouchers,
                  "promo_terms" => $promo_terms,
                  "description" => $description,
                  "image" => $image
               ];

              $isSent = $this->sendVoucherEmail($email_data, $new_order_number, $email);
              echo $isSent;
               
               //update all vouchers from live db
               $imploded_gifted_vouchers = implode(',',$gifted_vouchers);
               if(!empty($gifted_vouchers)) {
                  $status = $this->updateLiveVouchers($imploded_gifted_vouchers,$new_order_number);
                  echo $status; 
               }
               
            }

            foreach ($order_item_id as $value) {
             //  $this->setStatusToReadyToShip($value);
            }
         } else {
            echo "order existed";
         }
      }
   }


   function sendVoucherEmail($email_data, $new_order_number, $email)
   {
      $email_body = $this->load->view('emails/gift_card_simplified', $email_data, true);
      $email_content = [
         'from' => 'info@gifted.ph',
         'from_name' => "Gifted.PH",
         'to' => $email,
         'subject' => 'A message from Gifted.PH: You have received a gift! - #' . $new_order_number,
         'message' => $email_body
      ];

      $isSent = sendMailCIMailer($email_content);
      return $isSent;
   }


   function get_promo_terms($sku)
   {
      $find_promo = strpos($sku, "promo");
      $promo_terms = '';
      $first_dash_pos = strpos($sku, "-");
      if ($first_dash_pos === false) {
         $first_dash_pos = strpos($sku, "_");
      }

      if ($find_promo === false) {
         $gifted_amount = substr($sku, $first_dash_pos + 1);
         $promo_terms = '';
      } else {
         $gifted_amount = substr($sku, $first_dash_pos + 1);
         $gifted_amount = str_replace("promo", "", $gifted_amount);
         $promo_terms = $this->getProductTerms($sku);
      }
      return $promo_terms;
   }
   function getProductTerms($seller_SKU)
   {

      $seller_SKU_array = array($seller_SKU);
      $seller_SKU_json = json_encode($seller_SKU_array);

      $c = new LazopClient('https://api.lazada.com.ph/rest', 101345, 'XI8WP3sCeyz5d8qOgkYpsBDt472VECrd');
      $request = new LazopRequest('/products/get', 'GET');
      $request->addApiParam('filter', 'all');
      $request->addApiParam('sku_seller_list', $seller_SKU_json);
      $accessToken = $this->refreshToken();
      //var_dump($c->execute($request, $accessToken));
      $results = $c->execute($request, $accessToken);

      $get_products_results = json_decode($results);


      $products = $get_products_results->data->products;

      $product =  $products[0]; // get first result only



      $product_skus = $product->skus[0]; // get skus
      $seller_promotion = $product_skus->package_content;

      //echo '<pre>',print_r($seller_promotion),'</pre>';

      return $seller_promotion;
   }

   public function test_email()
   {
      $email_content = [
         'from' => 'test@gifted.ph',
         'from_name' => "test",
         'to' => 'jonasdulay28@gmail.com',
         'subject' => 'Contact Us - ',
         'message' => "test sdfsdf"
      ];

      $isSent = sendMailCIMailer($email_content);
      echo $isSent;
   }

   function createAccount($order, $email)
   {
      $filter = ["email" => $email];
      $check_exist = $this->Global_model->check_exist('users', $filter);
      if ($check_exist == 0) {
         $customer_info = [
            'firstname'       => getFirstName(trim($order->customer_first_name)),
            'lastname'          => getLastName(trim($order->customer_first_name)),
            'contact_number'          => $order->address_billing->phone,
            "email" => $email,
            "password" =>  hash_password("123Qwe1!"),
            "reg_type" => "Lazada"
         ];

         $this->Global_model->insert('users', $customer_info);
      }
   }
   public function get_gifted_vouchers($filter)
   {
      $merchant_id = $filter['voucher_merchant_id'];
      $denomination = $filter['voucher_denomination'];
      $order_number = $filter['order_number'];
      $rows = httpGet("https://gifted.ph/api_voucher/getVouchersApi?merchant_id=" . $merchant_id . "&denomination=" . $denomination . "&order_number=" . $order_number . "");
      // $filter = ["voucher_merchant_id" => 219, "voucher_denomination"=>1,"voucher_order_id"=>0,"is_disabled"=>0];
      // $otherdb = $this->load->database('otherdb', TRUE);
      // $rows = $otherdb->select('*')->from('orders')->where($filter)->get()->result();
      return $rows;
   }

   public function updateLiveVouchers($vouchers,$order_number)
   {
      $rows = httpGet("https://gifted.ph/api_voucher/updateLiveVouchers?vouchers=" . $vouchers . "&order_number=" .$order_number);
      return $rows;
   }

   public function get_billing_address($order, $email)
   {
      $tmp_billing_address = $order->address_billing;
      $billing_address = new stdClass();
      $billing_address->first_name = getFirstName(trim($order->customer_first_name));
      $billing_address->last_name = getLastName(trim($order->customer_first_name));
      $billing_address->middle_initial = "";

      $billing_address->country = $tmp_billing_address->country;
      $billing_address->province = $tmp_billing_address->address3;
      $billing_address->address = $tmp_billing_address->address1 . " " .
         $tmp_billing_address->address2 . " " . $tmp_billing_address->address4 . " " . $tmp_billing_address->address5 . " " . $tmp_billing_address->address3;
      $billing_address->email =  $email;
      return $billing_address;
   }

   function setStatusToReadyToShip($order_item_id)
   {
      $results = array();
      if ($order_item_id) {
         $c = new LazopClient('https://api.lazada.com.ph/rest', 101345, 'XI8WP3sCeyz5d8qOgkYpsBDt472VECrd');
         $request = new LazopRequest('/order/rts');
         $request->addApiParam('delivery_type', 'dropship');
         $request->addApiParam('order_item_ids', '[' . $order_item_id . ']');
         $request->addApiParam('shipment_provider', 'DigitalGoods');
         $accessToken = $this->refreshToken();
         //var_dump($c->execute($request, $accessToken));
         $results = $c->execute($request, $accessToken);
      }
      //print_r($results);
      return $results;
   }


   public function index2()
   {

      //  258718577247839
      $order_number = "258494136448576";

      $orders_data = $this->getOrder($order_number);
      // echo $orders[0]->order_number;
      if (!empty($orders)) {
         $order_items = $this->getOrderItems($order_number);
         // $order_number = $orders[0]->order_number;
      }
      print_r($order_items);
   }

   function getOrderItems($order_number)
   {
      if ($order_number) {
         $c = new LazopClient('https://api.lazada.com.ph/rest', 101345, 'XI8WP3sCeyz5d8qOgkYpsBDt472VECrd');
         $request = new LazopRequest('/order/items/get', 'GET');
         $request->addApiParam('order_id', $order_number);
         $accessToken = $this->refreshToken();
         $response = $c->execute($request, $accessToken);
         $response_array = json_decode($response);
         return $response_array->data;
      }
   }

   function getOrder($order_number)
   {
      if ($order_number) {
         $c = new LazopClient('https://api.lazada.com.ph/rest', 101345, 'XI8WP3sCeyz5d8qOgkYpsBDt472VECrd');
         $request = new LazopRequest('/order/get', 'GET');
         $request->addApiParam('order_id', "258494136448576");
         $accessToken = $this->refreshToken();
         $response = $c->execute($request, $accessToken);
         $response_array = json_decode($response);
         //print_r($response_array);
         return $response_array->data;
      }
   }

   function getOrders($data = array())
   {
      $c = new LazopClient('https://api.lazada.com.ph/rest', 101345, 'XI8WP3sCeyz5d8qOgkYpsBDt472VECrd');
      $request = new LazopRequest('/orders/get', 'GET');
      $request->addApiParam('created_after', '2019-11-01T09:00:00+08:00');
      $request->addApiParam('status', 'pending');
      $request->addApiParam('limit', '1');
      $request->addApiParam('sort_direction', 'ASC');
      $request->addApiParam('sort_by', 'updated_at');

      $accessToken = $this->refreshToken();

      $response = $c->execute($request, $accessToken);

      $response_array = json_decode($response);
      //print_r($response_array->data);
      return $response_array->data;
   }

   function refreshToken()
   {
      $c = new LazopClient('https://auth.lazada.com/rest', 101345, 'XI8WP3sCeyz5d8qOgkYpsBDt472VECrd');
      $request = new LazopRequest('/auth/token/refresh');

      // $api_data = $this->Lazada_model->get_api(2);
      //print_r($api_data); $api_data->refresh_token

      $request->addApiParam('refresh_token', "50001000124b5gwUOCFpveVBhIRwNTEbZltf155c3685htatsjbFjw3rTw8Cx");
      $response = $c->execute($request);
      $response_array = json_decode($response);
      //print_r($response_array->refresh_token);
      $api = array(
         'connection_id' => 2,
         'refresh_token' => $response_array->refresh_token,
      );
      // $this->Lazada_model->save($api);
      //var_dump($response);

      if ($response_array->access_token == '') {
         $this->lazada_expired_token();
      }

      return $response_array->access_token;
   }

   function lazada_expired_token()
   {
      // $this->load->library('email');		
      // $config['mailtype'] = 'html';
      // $this->email->initialize($config);
      // $this->load->helper( array('dompdf', 'file') );

      // $this->email->to('rowin.mandia@gmail.com');
      // $this->email->from( config_item('email_from_email'), config_item('email_from_name'));
      // $this->email->subject('Lazada #2 Expired Token!');		
      // $email_body = "Expired token! https://auth.lazada.com/oauth/authorize?response_type=code&force_auth=true&redirect_uri=https://www.gifted.ph/open_lazada&client_id=101345";		
      // $this->email->message($email_body);
      // $this->email->send();
      // $this->email->clear(TRUE);
   }
}
