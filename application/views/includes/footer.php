<div class="footer">
  <div class="maxwidth">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-4">
          <div class="row">
            <div class="col-xs-12">
              <div class="sm">SITE MAP</div>
            </div>
            <div class="col-xs-4">
              <ul class="ftul">
                <li> <a href="<?php echo base_url()?>gift-suggestions">Home</a></li>
                <li> <a href="<?php echo base_url()?>secure/login">Log-in</a></li>
                <li> <a href="<?php echo base_url()?>secure/register">Sign-up</a></li>
                <li> <a href="<?php echo base_url()?>cart/view-cart">Cart</a></li>
                <li> <a href="<?php echo base_url()?>gift-suggestions">Send A Gift</a></li>
                <li> <a href="<?php echo base_url()?>how-it-works">How It Works</a></li>
              </ul>
            </div>
            <div class="col-xs-4">
              <ul class="ftul">
                <!--<li> <a href="<?php echo base_url()?>gift-suggestions">Why Gifted</a></li>-->
                <li> <a href="<?php echo base_url()?>about">About</a></li>
                <li> <a href="<?php echo base_url()?>products/ol">Browse Gifts</a></li>
                <li> <a href="<?php echo base_url()?>gift-suggestions">Gift Suggestions</a></li>
                <li> <a href="<?php echo base_url()?>press">Press</a></li>
                <li> <a href="<?php echo base_url()?>blog">Blog</a></li>
                <li> <a href="<?php echo base_url()?>privacy-policy">Privacy Policy</a></li>
              </ul>
            </div>
            <div class="col-xs-4">
              <ul class="ftul">
                <li> <a href="<?php echo base_url()?>contact-us">Contact Us</a></li>
                <li> <a href="<?php echo base_url()?>faq">FAQ</a></li>
                <li> <a href="<?php echo base_url()?>shipping-and-returns">Shipping and Returns</a></li>
                <li><a href="https://www.theaffiliategateway.asia/directory/category/gifts-flowers/giftedph">Affiliates</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="container-fluid">
            <div class="row">
              <div class="col-xs-12">
                <div class="sm">Featured In</div>
              </div>
              <div class="col-xs-4"><img class="ftimg" src="<?php echo theme_img('fhm.png') ?>" alt="FHM" /></div>
              <div class="col-xs-4"><img class="ftimg" src="<?php echo theme_img('abscbn.png') ?>" alt="ABS-CBN" /></div>
              <div class="col-xs-4"><img class="ftimg" src="<?php echo theme_img('rouge.png') ?>" alt="Rouge" /></div>
            </div>
            <div class="row">
              <div class="col-xs-4"><img class="ftimg" src="<?php echo theme_img('parenting.png') ?>" alt="Parenting" /></div>
              <div class="col-xs-4"><img class="ftimg" src="<?php echo theme_img('style.png') ?>" alt="Style" /></div>
              <div class="col-xs-4"><img class="ftimg" src="<?php echo theme_img('fhm.png') ?>" alt="FHM" /></div>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="text-center social-links"> <a href="https://twitter.com/gifted_ph"><img src="<?php echo theme_img('twitter.png') ?>" alt="Twitter" class="futllk" /></a>&nbsp;&nbsp; <a href="https://facebook.com/giftedphilippines"><img src="<?php echo theme_img('facebook.png')?>" alt="Facebook" class="futllk" /></a>&nbsp;&nbsp; <a href="http://instagram.com/gifted_ph"><img src="<?php echo theme_img('instagram.png')?>" alt="Instagram" class="futllk" /></a>&nbsp;&nbsp; <br />
            <div class="flus"> Follow Us </div>
            <div class="flus">
              <div class="fb-like" data-href="https://www.facebook.com/GiftedPhilippines" data-layout="box_count" data-action="like" data-show-faces="true" data-share="false"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- <img src="img/white-line-foot.png" alt="" class="white-line-foot" /> -->
</div>
<footer class="text-center cu-fo">
  <div class="cards">
    <div class="item"> <img src="<?php echo theme_img('master-card.png')?>" alt="Master Card" /> </div>
    <div class="item"> <img src="<?php echo theme_img('visa.png')?>" alt="Visa" /> </div>
    <div class="item"> <img src="<?php echo theme_img('bdo.png')?>" alt="BDO" /> </div>
    <div class="item"> <img src="<?php echo theme_img('paypal.png')?>" alt="Paypal" /> </div>
    <div class="item"> <img src="<?php echo theme_img('bpi.png')?>" alt="BPI" /> </div>
  </div> <span>Copyright <?php echo date('Y') ?> Gifted | The Gift Giving Company</span>
</footer><a class="tell-a-friend" href="mailto:?subject=Check this out from Gifted.PH&body=<?php echo base_url()?>gifted Here is the link for Gifted.PH" title="Tell a friend"></a>