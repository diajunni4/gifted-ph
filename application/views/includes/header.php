<div class="top-nav">
   <div class="maxwidth">
      <nav class="custom-top-nav">
         <ul class="">
            <li>
               <form action="<?php echo base_url()?>newsletters/subscribe" method="POST"><input type="text" name="email" id="emailBox" class="nav-search-input" placeholder="Input your Email" /><input type="hidden" name="redirect" value="1" /></form>
            </li>
            <li><a href="#" id="subs">Subscribe</a></li>
            <?php if(is_logged()==1) { ?>
               <li><a href="<?php echo base_url()?>orders">My Orders</a></li>
               <li><a href="<?php echo base_url()?>authentication/logout">Logout</a></li>
               <li><a href="<?php echo base_url()?>secure/my_account#dash"><span class="glyphicon glyphicon-gift"></span></a></li>
               
            <?php } else { ?> 
            <li><a href="<?php echo base_url()?>authentication">Log-in</a></li>
            <li><a href="<?php echo base_url()?>authentication?redirect=checkout">Sign-up</a></li>
            <?php } ?>
            
         </ul>
      </nav>
   </div>
</div> <!-- top-nav -->

<div class="main-nav">
   <div class="maxwidth">
      <div class="container-fluid">
         <div class="row">
            <div class="col-xs-3 head-img">
               <a href="<?php echo base_url()?>"><img src="<?php echo images_bundle2('logo-white.png')?>" alt="Gifted Logo" /></a>
            </div>
            <div class="col-xs-9 head-nav">
               <nav class="navbar navbar-default nav-main-nav">
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo base_url()?>products">Send a Gift</a></li>
                        <li><a href="<?php echo base_url()?>how-it-works">How it works</a></li>
                        <li><a href="<?php echo base_url()?>contact">Contact Us</a></li>
                        <!-- <li class="lipress"><a href="<?php echo base_url()?>press">Press</a></li> -->
                        <form class="navbar-form navbar-left nav-search" role="search" accept-charset="utf-8" method="post" action="<?php echo base_url()?>products">
                           <div class="form-group">
                              <input type="text" class="nav-search-input" placeholder="Search" id="search_term" value="" name="txtSearch" id="txtSearch" type="text" placeholder="Search">
                           </div>
                        </form>
                     </ul>
                  </div><!-- /.navbar-collapse -->
               </nav>
            </div>
         </div>
      </div>
   </div>
</div> <!-- main-nav -->
<div class="navbar-header custom-nav">
   <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#id-collapse" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
   </button>
</div>
<div class="mobnav">
   <div class="collapse navbar-collapse custom-col" id="id-collapse">
      <ul class="nav navbar-nav">
         <li><a href="<?php echo base_url()?>products">Send a Gift</a></li>
         <li><a href="<?php echo base_url()?>how-it-works">How it works</a></li>
         <li><a href="<?php echo base_url()?>browse">Browse Gifts</a></li>
         <li><a href="<?php echo base_url()?>press">Press</a></li>
      </ul>
   </div><!-- /.navbar-collapse -->
</div>