<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta property="og:title" content="Gifted.PH" />
<meta property="og:description" content="Gifted.PH: Buy Gift Certificates Online from the Philippines" />
<meta property="og:image" content="<?php echo images_bundle2('favicon.png') ?>">
<meta property="og:type" content="<?php echo images_bundle2('favicon.png') ?>">
<meta name="title" content="Gifted.PH: Buy Gift Certificates Online from the Philippines" />
<meta name="Description" content="Buy and Send Gift Certificates and Gift Cards Online at Gifted.PH for anyone in Manila and Philippines. There's a gift certificate for everybody!" />

<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta property="og:url" content="<?php echo base_url() ?>" />
<title>Gifted.PH: Buy Gift Certificates Online from the Philippines</title>
<link rel="icon" href="<?php echo images_bundle2('item-banner.png') ?>" />
<link rel="shortcut icon" href="<?php echo images_bundle2('item-banner.png') ?>" />