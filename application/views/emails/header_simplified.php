<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html style="margin: 0; padding: 0;width:100%;">
  <head>
    <title>You've been Gifted</title> 
  </head>

  <body style="font-family: 'Helvetica', tahoma; font-size: 12px; color: #333; margin: 0; padding:3px; width:100%;" >
  
  <table width="100%" cellpadding="0" cellspacing="0" style="border:1px solid #ccc;max-width:600px;margin:0 auto;display:block;">
   
    <tr>
      <td style="padding:20px 20px 0 20px;">
        
        <table cellpadding="0" cellspacing="0" width="100%" align="center">     
          <tr>
            <td width="60%">
              <img src="<?php echo images_bundle2('logo.jpg')?>" alt="Logo">            
            </td>
            <td width="40%" align="right">
              <span class="main-date"><?php echo date('F d Y');?></span>
            </td>
          </tr>
        </table>
      
        
      </td>      
    </tr>
        
    <tr>
      <td>        
        <table cellpadding="0" cellspacing="0" width="100%" align="center">
          <tr>
            <td width="100%" colspan="2"  style="position: relative;">
             <hr style="-moz-border-bottom-colors: none; -moz-border-image: none; -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; margin-top:18px; border-color: #ccc; border-style: dashed none none; border-width: 1px;">
            </td>
          </tr>  
        </table>      
      </td>
    </tr>    
    
    <tr>
      <td style="background-color:#D14955;padding:10px;" align="center">        
        <table cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td width="100%" align="left" style="color: #fff; font-size: 20px; ">
             You've been Gifted!<br />This voucher entitles you to purchase the following at:
            </td>
          </tr>  
        </table>      
      </td>
    </tr>    
    
    
    <tr>    
      <td style="padding:20px">