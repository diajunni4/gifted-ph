<?php $this->load->view('emails/header_simplified');  ?>

<table cellpadding="0" cellspacing="0" width="100%" align="center">
  <tr>
    <td valign="middle" align="center">

      <table width="100%" cellpadding="0" cellspacing="0" style="padding:20px;">
        <tr>
          <td width="30%" align="center" valign="top">
            <img src="<?php echo brands_cover_img($image, 'medium') ?>" alt="Logo">
          </td>
          <td align="left" valign="top" style="padding-left:10px;">
            <table width="100%" cellspacing="8" cellpadding="8" style="border-collapse: collapse;">
              <?php if (isset($name)) { ?>
                <tr>
                  <td>Name</td>
                  <td style="color:#2E3235;"><b><?php echo ucwords($name); ?></b></td>
                </tr>
              <?php } else { ?>
                <tr>
                  <td>Surname</td>
                  <td style="color:#2E3235;"><b><?php echo ucwords($lastname); ?></b></td>
                </tr>
              <?php } ?>

              <tr>
                <td>Amount</td>
                <td style="color:#2E3235;"><b><?php echo format_currency2($total); ?></b></td>
              </tr>
              <?php
              $x = 0;
              foreach ($vouchers as $value) {
                $tmp_voucher_code = str_replace(htmlentities("<"), "^", htmlentities($value));
                $word_wrap = str_replace("^", htmlentities("<"), wordwrap($tmp_voucher_code, 13, "<br>\n", true));
              ?>
                <tr>
                  <td><?php echo $x == 0 ? "Voucher Code/s" : ""; ?></td>
                  <td style="color:#2E3235;">
                    <b><?php echo $word_wrap; ?></b>
                  </td>
                </tr>
              <?php
                $x++;
              } ?>

              <tr>
                <td>Order Number</td>
                <td style="color:#2E3235;"><b><?php echo $order_number; ?></b></td>
              </tr>

              <tr>
                <td>Date Issued</td>
                <td style="color:#2E3235;"><b><?php echo date('F d Y'); ?></b></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>

    </td>
  </tr>

  <!--
    <tr>
      <td align="center">
        <p style="font-size:14px;font-weight:400;margin-top:10px;">
         Simply present the attached gift certificate to your merchant<br />
         and avail the gift of your choice. Enjoy     
        </p>
        <br />
      </td>
    </tr>
    -->

  <tr>
    <td align="center">
      <table cellpadding="0" cellspacing="0" width="100%" align="center" style="display: table;">
        <tr>
          <td width="100%" colspan="2" style="position: relative;">
            <hr style="-moz-border-bottom-colors: none; -moz-border-image: none; -moz-border-left-colors: none; -moz-border-right-colors: none; -moz-border-top-colors: none; margin: 18px 0; border-color: #ccc; border-style: dashed none none; border-width: 1px;">
          </td>
        </tr>
      </table>
    </td>
  </tr>

  <tr>
    <td align="center">
      <table width="85%" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left">
            <?php
            $remove_text = array('<p><strong>&nbsp;</strong></p>', '<p>&nbsp;</p>', '<p><strong>How do I Buy this gift card? </strong>Select an Amount in Gift Certificate below, then click Send as Gift. Select a gift card and fill in the details to personalize it, then pay securely via credit card, Paypal or bank transfer.</p>', '<p><strong>How will the gift card be delivered? </strong>Your choice of electronic copy through recipient\'s e-mail or physical delivery to recipient\'s address (specify your preference in check-out page).</p>', '<p dir="ltr"><strong>How do I Buy this gift card?</strong> Select an Amount in Gift Certificate below, then click Send as Gift. Select a gift card and fill in the details to personalize it, then pay securely via credit card, Paypal or bank transfer.&nbsp;</p>', '<p dir="ltr"><strong>How will the gift card be delivered?</strong> Your choice of electronic copy through recipient\'s e-mail or physical delivery to recipient\'s address (specify your preference in check-out page)</p>');

            // $description = str_replace($remove_text, "", $product['description']);

            if ($promo_terms) {

              $promo_terms = '<p style="font-size: 18px;"><b>What\'s in the Box: ' . $promo_terms . '</b></p><br>';
              echo $promo_terms;
            }

            echo $description;
            ?>
          </td>
        </tr>
      </table>
    </td>
  </tr>


</table>



<?php $this->load->view('emails/footer_simplified'); ?>