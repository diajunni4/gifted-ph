<section id="section-generic-content">

	<div id="wrapper">

		<div class="container content-container" id="generic-page">
			<h1 class="main_color_text">FAQ</h1>
			<p><strong>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>How can I buy a gift certificate?</strong></p>
			<p>Simply choose a brand and personalize the gift by selecting a greeting card, typing in your message, and choosing a recommended gift item.&nbsp; Pay using Paypal, credit card or bank transfer. Done!</p>
			<p>&nbsp;</p>
			<p><strong>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>How do I redeem the Gift Certificate voucher?&nbsp; </strong></p>
			<p>Redemption process depends on the brand. You will see this in the Gift Voucher as "How will the gift card be delivered" and "How do I use this gift card". Some brands require printed copy of the voucher emailed to you, others allow electronic copies while some traditional brands' gift certificates have to be delivered to your shipping address. So it is important to refer to the instructions that come with your Gift Voucher.</p>
			<p>&nbsp;</p>
			<p><strong>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>How soon can you redeem the Gift Certificate?</strong></p>
			<p>Immediately upon receiving the gift certificate, for most.&nbsp; For a few, it will take three (3) business days. Double check the gift certificate’s terms and conditions to be sure</p>
			<p>&nbsp;</p>
			<p><strong>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>What kind of Gift Certificates can you buy in Gifted?</strong></p>
			<p>You can buy a gift certificate from over 80 of our gift certificate brands or you can buy a gift certificate for money, which can be deposited into the bank account of the Gift Recipient.&nbsp;</p>
			<p><strong>&nbsp;</strong></p>
			<p><strong>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>How long will it take for the gift certificate to be delivered?</strong></p>
			<p>All order payments will be verified first. If you have purchased an electronic gift certificate, this will be delivered after order verification, usually within 24 hours.&nbsp; If you have selected a physical gift certificate, it will be shipped within 7-10 working days (excluding weekends) and up to 14 working days for Provincial.</p>
			<p><strong>&nbsp;</strong></p>
			<p><strong>6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Does Gifted sell only Gift Certificates or products as well? </strong></p>
			<p>No, Gifted only sends gift certificates.</p>
			<p>&nbsp;</p>
			<p><strong>7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>What does each gift certificate come with?</strong></p>
			<p>Each Gifted Gift Certificate comes with a customized greeting card and a recommended gift item, making the giving of Gift Certificates more thoughtful and acceptable to give.&nbsp;</p>
			<p>&nbsp;</p>
			<p><strong>8. &nbsp; &nbsp; &nbsp; My gift certificate comes with a recommended gift item. Do I need to buy this item?</strong></p>
			<p>No, you do not need to buy the recommended gift item. You can use the gift certificate to buy any product that you want from the store.</p>
			<p>&nbsp;</p>
			<p><strong>9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong><strong>Does the gift certificates expire? </strong></p>
			<p>The gift certificates do not expire.</p>
			<p>&nbsp;</p>
			<p><strong>10.&nbsp;&nbsp; </strong><strong>I need to change the name on the gift certificate voucher to another name. How?</strong></p>
			<p>No need. The Gifted-issued gift certificate is as good as cash.&nbsp; Anyone can redeem it.&nbsp;</p>
			<p>&nbsp;</p>
			<p><strong>11.&nbsp;&nbsp; </strong><strong>Is the gift certificate refundable? </strong></p>
			<p>No. The gift certificate is not refundable and non-convertible to cash. &nbsp;</p>
			<p>&nbsp;</p>
			<p><strong>12.&nbsp;&nbsp; </strong><strong>I lost my gift certificate, how can I get it back? </strong></p>
			<p>It can be resent to you if not yet used. Send us an email via&nbsp;<a href="mailto:customersupport@gifted.ph">customersupport@gifted.ph</a>&nbsp;or <a href="mailto:admin@gifted.ph">admin@gifted.ph</a>. We will check that the gift certificate has not been used and will resend to you once validated.</p>
			<p>&nbsp;</p>
			<p><strong>13.&nbsp;&nbsp; </strong><strong>Can I save a balance or unused amount onto the gift certificate? </strong></p>
			<p>The gift certificates cannot store or save unused amounts.&nbsp; Any unused amount will be forfeited.</p>
			<p>&nbsp;</p>
		</div>

	</div>

</section>