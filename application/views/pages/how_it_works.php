	<!-- how it works -->
	<div class="mainCont">
		<div class="maxwidth hiwCont">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="hiw">
							<p class="title">How it works</p>
							<p class="subtitle">Do you want to buy a gift card?</p>
							<div class="container-fluid">
								<div class="row">
									<div class="col-sm-4 item">
										<div class="img">
											<img src="<?php echo theme_img('hiw_img01.png'); ?>" alt="####" />
										</div>
										<div class="text">
											<p class="ti">Choose A Brand</p>
											<p class="desc">
												Over 80 gift card brands to<br/>choose from!
											</p>
										</div>
									</div><div class="col-sm-4 item">
										<div class="img">
											<img src="<?php echo theme_img('hiw_img02.png'); ?>" alt="####" />
										</div>
										<div class="text">
											<p class="ti">Choose A Greeting Card</p>
											<p class="desc">
												Write a thoughtful message and <br/>recommend an item to buy with <br/>the gift card
											</p>
										</div>
									</div><div class="col-sm-4 item">
										<div class="img">
											<img src="<?php echo theme_img('hiw_img03.png'); ?>" alt="####" />
										</div>
										<div class="text">
											<p class="ti">Pay Securely & Send!</p>
											<p class="desc">
												All major credit cards, Paypal and <br/>bank transfers accepted. Your <br/>recipient receives the gift card <br/>instantly over email.
											</p>
										</div>
									</div>

								</div>
							</div>
							<p class="subtitle">DID YOU RECEIVE A GIFT CARD?</p>
							<div class="container-fluid">
								<div class="row">
									<div class="col-sm-4 card">
										<div class="img">
											<img src="<?php echo theme_img('hiw_img04.png'); ?>" alt="####" />
										</div>
										<div class="text">
											<p class="ti">Print</p>
											<p class="desc">
												Print the gift card sent <br />to you via email.
											</p>
										</div>
									</div>
									<div class="col-sm-4 card">
										<div class="img">
											<img src="<?php echo theme_img('hiw_img05.png'); ?>" alt="####" />
										</div>
										<div class="text">
											<p class="ti">Show</p>
											<p class="desc">
												Show it to the merchant.
											</p>
										</div>
									</div>
									<div class="col-sm-4 card">
										<div class="img">
											<img src="<?php echo theme_img('hiw_img06.png'); ?>" alt="####" />
										</div>
										<div class="text">
											<p class="ti">Shop!</p>
											<p class="desc">
												Shop for the gift of your choice.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="hiw">
							
						</div> <!-- -->
					</div>
				</div>
			</div>
			<div class="op">
				
			</div>
		</div>

		<div class="ourPart">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<p class="title">
								Our partner brands
							</p>
						</div>
						<div class="col-md-12">
							<?php foreach($featured_brands as $brand){ ?>
								<div class="col-sm-2">
							            <div class="col-sm-12">
							                <a href="<?php echo site_url($brand->slug)?>" class="thumbnail">
							                    <div class="item-brand" style="background: url('<?php echo brands_cover_img($brand->image); ?>') no-repeat center center; background-size: 100%;">

							                    </div>
							                </a>    
							            </div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
		</div>

		<img src="<?php echo theme_img('bg_red_left.png'); ?>" alt="" class="rib-left" />
		<img src="<?php echo theme_img('bg_red_right.png'); ?>" alt="" class="rib-right" />
	</div>

	<!-- /how it works -->