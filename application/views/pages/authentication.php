<div class="container authentication">
    <!-- Sing in  Form -->
    <section class="sign-in">
        <div class="container">
            <div class="signin-content">
                <div class="signin-image">
                    <figure><img src="<?php echo images_bundle2('register.jpg') ?>" alt="sign up image" style="height: 160px;"></figure>
                    <center class="d-none d-xs-none d-sm-none d-md-block d-lg-block">
                        <button class="form-submit signup-image-link" style="background: #de414c;color:white; border-radius:0px;">Create a New Account</button>
                        <button class="form-submit signin-image-link" style="background: #de414c;color:white;border-radius:0px;">Log In here</button>
                    </center>
                </div>
                <div class="signin-form">
                    <h2 class="form-title">Sign in</h2>
                    <p>Don't have an account? <a href="#" class="signup-image-link2 main_color" style="display: inline;">click here.</a></p>
                    <?php echo form_open("#", "method='POST' id='login-form' class='login-form'") ?>
                    <input type="hidden" value="<?php echo (get('redirect') != '' ? get('redirect') : '') ?>" name="redirect">
                    
                    <hr>
                    <div class="form-group">
                        <label>Email:</label>
                        <input type="text" name="email" class="form-control" id="your_name" placeholder="Email Address" required />
                    </div>
                    <div class="form-group">
                        <label>Password: </label>
                        <input type="password" class="form-control" name="password" id="login_password" placeholder="Password" required />
                    </div>
                    <div id="result"></div>
                    <div class="form-group remember_me_container">
                        <input type="checkbox" name="remember_me" value="remember" id="remember-me" class="agree-term" checked="" />
                        <label for="remember-me" class="label-agree-term"><span><span></span></span>Remember me</label><br>
                        <small><a href="javascript:void(0)" class="forgot_password" id="forgot-password-modal">Forgot your password?</a></small>
                    </div>
                    <div class="form-group form-button">
                        <button id="signin" class="form-submit login">Log In</button>
                        <!-- <div class="d-xs-block d-sm-block d-md-none d-lg-none">
                            <button class="form-submit signup-image-link" >Create Account</button>
                        </div> -->

                    </div>
                    <?php echo form_close() ?>
                    <!-- <div class="social-login">
                      <span class="social-label">Or login with</span>
                      <ul class="socials">
                          <li><a href="#"><i class="display-flex-center fab fa-facebook"></i></a></li>
                          <li><a href="#"><i class="display-flex-center fab fa-google"></i></a></li>
                      </ul>
                  </div> -->
                </div>
                <div class="signup-form">
                    <h2 class="form-title">Sign up</h2>
                    <p>Already have an account? <a href="#" class="signin-image-link main_color" style="display: inline;">click here.</a></p>
                    <hr>
                    <?php echo form_open("#", "method='POST' id='register-form' class='register-form'") ?>
                    <input type="hidden" value="<?php echo (get('redirect') != '' ? get('redirect') : '') ?>" name="redirect">
                    <div id="result2" style="text-align:center; color:red;"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name:</label>
                                <input type="text" name="firstname" class="form-control" placeholder="First Name" required />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Last Name:</label>
                                <input type="text" name="lastname" class="form-control" placeholder="Last Name" required />
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <label>Phone:</label>
                        <input type="text" class="form-control" name="contact_number" placeholder="Contact number">
                    </div>
                    <div class="form-group">
                        <label>Email:</label>
                        <input type="email" name="email" class="form-control" placeholder="Email Address" required />
                    </div>
                    <div class="form-group">
                        <label>Password: </label>
                        <input type="password" class="form-control password" name="password" placeholder="Password" required />
                    </div>
                    <div class="form-group">
                        <label>Confirm Password: </label>
                        <input type="password" class="form-control cf_password" name="cf_password" placeholder="Confirm Password" required />
                    </div>

                    <small class="terms">Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our
                        <a href="<?php echo base_url("privacy-policy") ?>" class="woocommerce-privacy-policy-link" target="_blank">privacy policy</a>.</small>
                    <div class="form-group form-button">
                        <input type="submit" name="signin" id="signin" class="form-submit  register" value="Register" />
                        <!-- <div class="d-none d-xs-block d-sm-block d-md-none d-lg-none">
                            <button class="form-submit signin-image-link login_link2">Log In here</button>
                        </div> -->
                    </div>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Modal -->
<div id="forgot-password-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Forgot Password</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Email Address: </label>
                    <input type="text" class="form-control" name="email_address" id="" placeholder="Email Address" /><br>
                    <center><button type="button" class="btn btn-default" data-dismiss="modal">Submit</button> </center>
                </div>
            </div>
        </div>

    </div>
</div>