<div class="container">
   <br>
   <div class="row">
      <nav aria-label="breadcrumb">
         <ol class="breadcrumb">
            <li class="breadcrumb-item">
               <a href="<?php echo base_url() ?>">Home</a>
            </li>
            <li class="breadcrumb-item" aria-current="page">
               <a href="<?php echo base_url('orders') ?>">Orders</a>
            </li>
            <li class="breadcrumb-item active">View Order</li>
         </ol>
      </nav>
   </div>
   <div class="row">
      <div class="col-md-12">
         <?php

         $billing_address = json_decode($order_details->billing_address);
         $shipping_address = json_decode($order_details->shipping_address);
         $order_number = $order_details->order_number;
         $total = $order_details->total;
         $firstname = $order_details->firstname;
         $lastname = $order_details->lastname;
         $email = $order_details->email;
         $contact_number = $order_details->contact_number;
         $payment_method = $order_details->payment_method;
         $gift_to_details = json_decode($order_details->gift_to_details);
         $payment_details = "Thank you for your order! You have selected Bank Transfer as your mode of payment. This order reservation is valid for five (5) business days. Please complete your purchase by depositing to the following bank account.
   <br>
   Please email us the scanned copy of the deposit slip at admin@gifted.ph once done. Thank you!
   <br>
   Account Name: E-link Business Innovation Corp
   BPI Account No.: 3911-0043-12
   BDO Account No: 006938009120
   Unionbank Account No. 001920001132"
         ?>

         <h1 style="    font-family: 'Pacifico';">Order Acknowledgement Receipt</h1>
         <table cellpadding="0" cellspacing="0" width="100%" align="center">
            <tr>
               <td align="left">
                  <h2 style="color: #24292d; font-size: 20px; font-weight: 600;">Order Receipt # <?php echo $order_number; ?></h2>
               </td>
            </tr>
         </table>
         <br />
         <br />
         <?php if (isset($gift_to_details->name)) { ?>
            <table cellpadding="0" cellspacing="0" width="100%" align="center" style="border:1px solid #ccc; background-color: #fafafa;padding: 10px;" class="table ">
               <tr>
                  <td align="left">
                     <h4>Recipient Details</h4>
                     <p>Name: <?php echo $gift_to_details->name; ?></p>
                     <p>Email: <?php echo $gift_to_details->email_address; ?></p>
                  </td>
               </tr>
            </table>
            <br />
         <?php } ?>

         <table cellpadding="0" cellspacing="0" width="100%" align="center" style="border:1px solid #ccc; background-color: #fafafa;padding: 10px;" class="table ">
            <tr>
               <td align="left">
                  <h4>Payment Details</h4>
                  <p><?php echo nl2br($payment_details); ?></p>
               </td>
            </tr>
         </table>
         <br />
         <br />

         <table cellpadding="0" cellspacing="0" width="100%" align="center">
            <tr>
               <td valign="top" width="36%" style="height:100px">
                  <h3 style="font-weight: 600; line-height: 1.1; font-size: 16px; margin: 0 0 20px; padding: 0;">Order Number</h3>
                  <?php echo $order_number; ?>
               </td>
               <td valign="top">
                  <h3 style="font-weight: 600; line-height: 1.1; font-size: 16px; margin: 0 0 20px; padding: 0;">Grand Total</h3>
                  <span style="color: #de414d;font-weight:700;"><?php echo format_currency($total); ?></span>
               </td>
               <td valign="top">
                  <h3 style="font-weight: 600; line-height: 1.1; font-size: 16px; margin: 0 0 20px; padding: 0;">Payment Information</h3>
                  <?php
                  if ($payment_method == "bank_deposit") {
                     echo "Bank Deposit (BPI and BDO)";
                  } elseif ($payment_method == "gifted_wallet") {
                     echo "Gifted Wallet";
                  } elseif ($payment_method == "company_wallet") {
                     echo "Company Wallet";
                  } else {
                     echo "Paypal/CC/Debit";
                  }
                  ?>
               </td>
            </tr>

            <tr>
               <td valign="top" style="height:150px">
                  <h3 style="font-weight: 600; line-height: 1.1; font-size: 16px; margin: 0 0 20px; padding: 0;">Account Information</h3>
                  <?php echo $firstname . ' ' . $lastname; ?> <br />
                  <?php echo $email; ?> <br />
                  <?php echo $contact_number; ?>
               </td>

               <?php if (!empty($shipping_address)) { ?>

                  <td valign="top">
                     <h3 style="font-weight: 600; line-height: 1.1; font-size: 16px; margin: 0 0 20px; padding: 0;">Shipping Information</h3>
                     <?php echo $shipping_address->first_name . ' ' . $shipping_address->last_name ?><br />
                     <?php echo $shipping_address->address ?><br />
                     <?php echo $shipping_address->province . ' ' . $shipping_address->country . ' ' . $shipping_address->zip_code ?><br />
                     <?php echo $shipping_address->email ?><br />
                     <?php echo $shipping_address->contact_number ?>
                  </td>

               <?php } ?>

               <td valign="top">
                  <?php if (!empty($billing_address)) : ?>
                     <h3 style="font-weight: 600; line-height: 1.1; font-size: 16px; margin: 0 0 20px; padding: 0;">Billing Information</h3>
                     <?php echo $billing_address->first_name . ' ' . $billing_address->last_name ?><br />
                     <?php echo $billing_address->address ?><br />
                     <?php echo $billing_address->province . ' ' . $billing_address->country . ' ' . $billing_address->zip_code ?><br />
                     <?php echo $billing_address->email ?><br />
                     <?php echo $billing_address->contact_number ?>
                  <?php endif; ?>
               </td>
            </tr>

            <tr>
               <td valign="top" style="height:100px">
                  <h3 style="font-weight: 600; line-height: 1.1; font-size: 16px; margin: 0 0 20px; padding: 0;">Order Status</h3>
                  <?php echo $order_details->status; ?>
               </td>
               <td valign="top">
                  <h3 style="font-weight: 600; line-height: 1.1; font-size: 16px; margin: 0 0 20px; padding: 0;">Delivery Method</h3>
                  <?php if ($order_details->delivery_option == 'send_email') { ?>
                     <?php echo "Send through Email - printable PDF"; ?>
                     <?php echo $order_details->send_later ? '<br />Send Later: ' . $order_details->send_later : ''; ?>
                  <?php } else {  ?>
                     Send via Shipping Address
                  <?php }  ?>
               </td>
               <td valign="top">
                  <h3 style="font-weight: 600; line-height: 1.1; font-size: 16px; margin: 0 0 20px; padding: 0;">Merchant</h3>
                  <?php echo $order_details->brand_name ?>
               </td>
            </tr>

         </table>
         <br />

         <table cellpadding="5" cellspacing="5" width="100%" align="center" border="1" class="table table-bordered">
            <thead>
               <tr>
                  <th width="60%" style="border: solid 1px #ccc;">Product</th>
                  <th width="10%" style="border: solid 1px #ccc;" align="center">Qty</th>
                  <th width="15%" style="border: solid 1px #ccc;" align="center">Total</th>
               </tr>
            </thead>

            <tbody>
               <tr>
                  <td style="border: solid 1px #ccc;word-wrap:break-word">
                     <h4><?php echo $order_details->product_name ?></h4>
                     <p><?php echo $order_details->excerpt ?></p>
                  </td>
                  <td align="center" style="border: solid 1px #ccc;"><?php echo $order_details->quantity ?></td>
                  <td align="center" style="border: solid 1px #ccc;"><?php echo format_currency($order_details->sub_total); ?></td>
               </tr>
            </tbody>
         </table>
         <br />
         <?php
         //echo '<pre style="display: none;">', print_r($go_cart), '</pre>';
         ?>
         <table cellpadding="10" cellspacing="5" width="100%" align="center" border="1" class="table table-bordered">
            <tr>
               <td width="85%" align="right" style="border: solid 1px #ccc;">Sub Total</span></td>
               <td align="left" style="border: solid 1px #ccc;"><?php echo format_currency($order_details->sub_total); ?></span></td>
            </tr>
            <tr>
               <td width="85%" style="border: solid 1px #ccc;" align="right">Delivery Charge</span></td>
               <td align="left" style="margin-left:10px;border: solid 1px #ccc;"><?php echo format_currency($order_details->delivery_charge); ?></span></td>
            </tr>
            <tr>
               <td width="85%" style="border: solid 1px #ccc;" align="right">Service Fee:</td>
               <td align="left" style="margin-left:10px;border: solid 1px #ccc;"><?php echo format_currency($order_details->service_fee); ?> </td>
            </tr>
            <tr>
               <td width="85%" style="border: solid 1px #ccc;" align="right">VAT:</td>
               <td align="left" style="margin-left:10px;border: solid 1px #ccc;"><?php echo format_currency($order_details->vat); ?> </td>
            </tr>
            <?php if ($order_details->promo) { ?>
               <tr>
                  <td width="85%" style="border: solid 1px #ccc;" align="right">Promo Code Discount:</td>
                  <td align="left" style="margin-left:10px;border: solid 1px #ccc;"><?php echo '('.$order_details->promo_code. ') '.format_currency($order_details->promo); ?> </td>
               </tr>
            <?php
            } ?>
            <?php if ($order_details->discount) { ?>
               <tr>
                  <td width="85%" style="border: solid 1px #ccc;" align="right">Corporate Discount:</td>
                  <td align="left" style="margin-left:10px;border: solid 1px #ccc;"><?php echo format_currency($order_details->discount); ?> </td>
               </tr>
            <?php
            } ?>
            <tr>
               <td width="85%" style="border: solid 1px #ccc;" align="right">Total:</td>
               <td align="left" style="margin-left:10px;border: solid 1px #ccc;"><?php echo format_currency($order_details->total); ?></td>
            </tr>

         </table>
      </div>
   </div>
</div>