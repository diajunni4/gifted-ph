<div class="bdycont">
   <h1 class="noshow">Gifted Philippines - Send a Gifted Money Check</h1>
   <div class="maxwidth paddbdy">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-6">
               <img src="<?php echo images_bundle2('itembanner-standard.png')?>" alt="Gifted Philippines" class="banner-img" />
               <div class="banner-img-text">
               </div>
            </div>
            <div class="col-md-6">
               <div class="moto-text">Send personalized electronic gift certificates from over 120 well-loved brands in the Philippines with Gifted.PH!</div>
               <div class="sndgft-btn">
                  <a href="<?php echo base_url()?>products" class="sndgft">Send a Gift</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="carousel-cont">
   <div class="maxwidth">
      <div id="owl-demo" class="owl-carousel owl-theme">
         <div class="item">
            <a href="<?php echo base_url()?>products/The Gifted.PH Gift Card">
               <img src="<?php echo base_url()?>uploads/images/small/257ac28401a58732f42b1cb09ed722dd.png" alt="Buy Gifted.PH Gift Card" /> </a>
         </div>
         <div class="item">
            <a href="<?php echo base_url()?>products/Fully Booked">
               <img src="<?php echo base_url()?>uploads/images/small/5ad19b2c48083b7d72986b1e0fd3915e.jpg" alt="Buy and Send Fully Booked Gift Certificates Online" /> </a>
         </div>
         <div class="item">
            <a href="<?php echo base_url()?>products/Piandre Salon">
               <img src="<?php echo base_url()?>uploads/images/small/da25d00e67d2fa7b09c8a9864e6950bc.png" alt="Buy and Send Piandre Salon Gift Certificates Online" /> </a>
         </div>
         <div class="item">
            <a href="<?php echo base_url()?>products/TGI Fridays">
               <img src="<?php echo base_url()?>uploads/images/small/f1b2277a5606b02379d75c9768a2332c.jpg" alt="Buy and Send TGI Fridays Gift Certificates Online" /> </a>
         </div>
      </div>
   </div>
</div>

<div class="red-cont" style="">
   <div class="maxwidth">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-6">
               <div class="banner-text-red">GET THOUGHTFUL, GET GIFTED</div>
            </div>
            <div class="col-md-6">
               <div class="banner-subtext-red">
                  Simply choose and personalize the gift card of your choice. Pay securely and we send it instantly over email. Your recipient prints and presents the gift card to the store to shop. Done! </div>
            </div>
         </div>
         <br />
         <div class="row">
            <div class="col-md-3 recom-cont">
               <div class="item-red text-center">
                  <div class="title">No Worries</div>
                  <div class="img">
                     <a href=""><img class="fimg" src="<?php echo base_url()?>uploads/homepage/item-col-1.png" alt="Gifted Philippines" /></a>
                  </div>
                  <div class="desc">
                     Zero Traffic, Hassle-Free Gift Shopping </div>
                  <div class="border"></div>
               </div>
            </div>

            <div class="col-md-3 recom-cont">
               <div class="item-red text-center">
                  <div class="title">100% Gift Satisfaction</div>
                  <div class="img">
                     <a href=""><img class="fimg" src="<?php echo base_url()?>uploads/homepage/item-col-2.png" alt="Gifted Philippines" /></a>
                  </div>
                  <div class="desc">
                     People love choosing their own perfect gift. </div>
                  <div class="border"></div>
               </div>
            </div>

            <div class="col-md-3 recom-cont">
               <div class="item-red text-center">
                  <div class="title">Anytime, Anywhere!</div>
                  <div class="img">
                     <a href=""><img class="fimg" src="<?php echo base_url()?>uploads/homepage/item-col-3.png" alt="Gifted Philippines" /></a>
                  </div>
                  <div class="desc">
                     Send a gift in five quick minutes! </div>
                  <div class="border"></div>
               </div>
            </div>

            <div class="col-md-3 recom-cont">
               <div class="item-red text-center">
                  <div class="title">Personalize it!</div>
                  <div class="img">
                     <a href=""><img class="fimg" src="<?php echo base_url()?>uploads/homepage/item-col-4.png" alt="Gifted Philippines" /></a>
                  </div>
                  <div class="desc">
                     Make the gift card special. </div>
                  <div class="border"></div>
               </div>
            </div>

         </div>
      </div>
      <div class="red-margin"></div>
      <div class="text-center">
         <a href="<?php echo base_url()?>how-it-works" class="sndgft2">How It Works</a>
      </div>
   </div>
   <img src="<?php echo images_bundle2('white-line.png')?>" alt="How It Works" class="white-line" />
</div>

<div class="white-cont">
   <div class="maxwidth">
      <div class="wtil">
         Gift Certificates </div>

      <img src="<?php echo images_bundle2('red-line-right.png')?>" alt="Gift Cards to Love" class="red-right-rib" />
   </div>
</div>

<div class="green-cont">
   <div class="maxwidth">
      <div class="container-fluid">
         <div class="row">

            <div class="col-sm-3">

               <div class="recom-item">
                  <a href="<?php echo base_url()?>shakeys-gift-certificate">
                     <div class="image">
                        <div class="img" style="background: url('<?php echo base_url()?>uploads/images/small/5261089e0b3fbd032139df06b5e558c6.jpg/300/200') no-repeat center / cover;"></div>
                        <div class="title">
                           <span>Shakey's</span><br />
                           <span></span>
                        </div>
                     </div>
                  </a>
                  <div class="desc">
                     <div class="cat">Restaurant, Food and Drinks</div>
                     <!--<div class="">Recommended Items</div>-->
                  </div>
               </div>

            </div>
            <div class="col-sm-3">

               <div class="recom-item">
                  <a href="<?php echo base_url()?>grab-food-gift-certificates">
                     <div class="image">
                        <div class="img" style="background: url('<?php echo base_url()?>uploads/images/small/bfef3f4eecad8f76f5ee38fde055ceda.jpg/300/200') no-repeat center / cover;"></div>
                        <div class="title">
                           <span>Grab Food</span><br />
                           <span></span>
                        </div>
                     </div>
                  </a>
                  <div class="desc">
                     <div class="cat">Restaurant, Food and Drinks</div>
                     <!--<div class="">Recommended Items</div>-->
                  </div>
               </div>

            </div>
            <div class="col-sm-3">

               <div class="recom-item">
                  <a href="<?php echo base_url()?>the-travel-club-gift-certificates">
                     <div class="image">
                        <div class="img" style="background: url('<?php echo base_url()?>uploads/images/small/4692f174649089ae19e0257266216950.jpg/300/200') no-repeat center / cover;"></div>
                        <div class="title">
                           <span>The Travel Club</span><br />
                           <span></span>
                        </div>
                     </div>
                  </a>
                  <div class="desc">
                     <div class="cat">Clothing, Shoes and Bags</div>
                     <!--<div class="">Recommended Items</div>-->
                  </div>
               </div>

            </div>
            <div class="col-sm-3">

               <div class="recom-item">
                  <a href="<?php echo base_url()?>skin-pro-gift-certificates">
                     <div class="image">
                        <div class="img" style="background: url('<?php echo base_url()?>uploads/images/small/daf8d841d1426aa82ace4099c8aa40ee.jpg/300/200') no-repeat center / cover;"></div>
                        <div class="title">
                           <span>Skin Pro</span><br />
                           <span></span>
                        </div>
                     </div>
                  </a>
                  <div class="desc">
                     <div class="cat">Beauty, Grooming and Wellness</div>
                     <!--<div class="">Recommended Items</div>-->
                  </div>
               </div>

            </div>
         </div>
      </div>
   </div>
</div>

<div class="white-cont">
   <div class="maxwidth text-center brgf">
      <a href="<?php echo base_url()?>products" class="sndgft">Browse Gifts</a>
   </div>
   <img src="<?php echo images_bundle2('red-line-left.png')?>" alt="Browse Gifts" class="red-left-rib" />
</div>