<html>

<head>

<title>You've Been Gifted</title>

  <style type="text/css">

	body {margin:0;padding:0;font-family: "Helvetica", tahoma;	font-size:12px;color:#434345; }

    h3, h4 {margin:0 0 5px 0;padding:0; }

	@page { margin: 100px 0px; }

    #header { position: fixed; left: 0px; top: -100px; right: 0px; height: 100px; background-color: none; text-align:left;}

    #footer { position: fixed; left: 0px; bottom: -100px; right: 0px; height: 100px;  background-color:#DF3C4E; }

    #footer .page:after { content: counter(page, upper-roman); }    

    

    #header-logo {

    	position:fixed;

    	top:-80px;

    	left:40px;

    }

    

    #footer-logo {

    	position:fixed;

    	bottom:-15px;

    	right:40px;

    	text-align:right;    	    	

    }

    

    #footer-info {

    	position:fixed;

    	bottom:-30px;

    	left:40px;

    	font-weight:600;

		font-size:12px;

		color:#FFF;

		width:400px;

		display:inline-block;

    }



    #content {margin:20px 45px;}

    

    #sub-header {

	    display: block;

	  	top: 0;

	  	background-color:#E03D4E;

	  	width:100%;

	  	text-align:left;

	  	color:#FFF;  	

    }

  

	#sub-header-content {

	   margin:10px 45px;     

	   background-image: url("<?php echo theme_img('red-dots2.jpg');?>");  	

	   background-repeat: repeat-x;

	   height:130px;

	   padding-top:15px;   

	}

  

    #sub-header-content h3 {

  	  font-size:24px;

  	  margin-top:5px;

    }

  

    #sub-header-content h2 {

  	  font-size:28px;

  	  margin:0;

    } 



    #body {     
      margin-top:20px;

    	background:none;

    	width:100%;    

 	}

       

  </style>

  <?php

//     $greetings = $go_cart['greetings']; 

//     $product   = reset($go_cart['contents']); 

  

//   $transact_from_reference = $go_cart['transact_from_reference'];

//   $transact_from_reference = json_decode($transact_from_reference, true);

//   $promo_terms = '';
//   if(!empty($transact_from_reference)) {
//     if (array_key_exists('PromoTerms', $transact_from_reference)) {
//       $promo_terms = $transact_from_reference['PromoTerms'];
//     }
//   }

  $brand_img = $voucher_details->image;
  $brand_name = $voucher_details->brand_name;
  $product_nane = $voucher_details->product_name;
  $about = $voucher_details->about;
  $how_to_redeem = $voucher_details->how_to_redeem;
  $terms_and_condition = $voucher_details->terms_and_condition;
  $firstname = $voucher_details->firstname;
  $lastname = $voucher_details->lastname;
  $voucher_code = $voucher_details->voucher_code;
  ?>   

<body>



  <div id="header">

    <div id="header-logo">          

        <img src="<?php echo theme_img('logo.jpg')?>" alt="Logo">          

    </div>    

  </div>

    

  <div id="footer">

    

   <div id="footer-info">    

       &copy; Gifted : The Gift Giving Company<br />

       admin@gifted.ph<br />	

   </div>

   

   <div id="footer-logo">

     <img src="<?php echo theme_img('gifted-logo-white.png')?>" alt="Logo Inverted">  

   </div>

     

  </div>



  

  <div id="sub-header">

    <div id="sub-header-content">

      <h3>This voucher entitles you to </h3>

      <h2><?php echo $product_nane?></h2>

      <!-- <h3>worth of purchases at</h3> -->

    </div>

  </div>

   

  <div id="content">
    <div id="body">     

    <table cellpadding="0" cellspacing="0" width="100%" align="center">     

        <tr>

          <td width="50%" align="center" valign="middle" style="background-color:#FEFEFE;border:1px solid #E6E6E6">

            <img src="<?php echo brands_cover_img($brand_img,'medium')?>" alt="Logo">

          </td>  

          <td width="50%" align="left" valign="top">

            <table width="100%" cellspacing="8" cellpadding="8">

              <!--

              <tr>

                <td width="45%"><b>Gift Giver</b></td>

                <td style="color:#2E3235;"><?php echo ucwords($firstname.' '. $lastname); ?></td>

              </tr>

              -->

              <tr>

                <td><b>Recipient Surname</b></td>

                <td style="color:#2E3235;"><?php echo ucwords($lastname); ?></td>  

              </tr>

              <tr>

                <td><b>Voucher Code</b></td>
                <?php 
                  $tmp_voucher_code = str_replace(htmlentities("<"), "^", htmlentities($voucher_code));
                  $word_wrap = str_replace("^",htmlentities("<"),wordwrap($tmp_voucher_code,13,"<br>\n",true));
                ?>

                <td style="color:#2E3235;"><?php echo $word_wrap;?></td>

              </tr>

              <tr>

                <td><b>Order Number</b></td>

                <td style="color:#2E3235;"><?php echo "12312321312";?></td>

              </tr>

               <tr>

                <td><b>Date Issued</b></td>

                <td style="color:#2E3235;"><?php echo isset($date_time_completed) ? format_date(date('Y-m-d', strtotime($date_time_completed))) : format_date(date('Y-m-d'));?></td>

              </tr>

            </table>

          </td>       

      </table>

      <h4 style="font-size:22px;margin-top:20px;"><?php echo $brand_name;?></h4>

      <p style="border-color: #999; border-style: dashed none none none; border-width: 1px;">&nbsp;</p>   

      <p style="page-break-inside: auto;">

        <?php 

          $remove_text =array('<p><strong>&nbsp;</strong></p>','<p>&nbsp;</p>','<p><strong>How do I Buy this gift card? </strong>Select an Amount in Gift Certificate below, then click Send as Gift. Select a gift card and fill in the details to personalize it, then pay securely via credit card, Paypal or bank transfer.</p>','<p><strong>How will the gift card be delivered? </strong>Your choice of electronic copy through recipient\'s e-mail or physical delivery to recipient\'s address (specify your preference in check-out page).</p>','<p dir="ltr"><strong>How do I Buy this gift card?</strong> Select an Amount in Gift Certificate below, then click Send as Gift. Select a gift card and fill in the details to personalize it, then pay securely via credit card, Paypal or bank transfer.&nbsp;</p>', '<p dir="ltr"><strong>How will the gift card be delivered?</strong> Your choice of electronic copy through recipient\'s e-mail or physical delivery to recipient\'s address (specify your preference in check-out page)</p>');

          $description = str_replace($remove_text, "", "asdas");

         //  if($promo_terms){

         //    $find_terms = array('<p><strong>About', '<p style="text-align: justify;"><strong>About', '<p style="text-align: left;"><strong>About', '<p style="text-align: center;"><strong>About', '<p style="text-align: right;"><strong>About');

         //    $description = str_replace($find_terms, '<p style="font-size: 18px;"><b>What\'s in the Box: '.$promo_terms.'</b></p><p><strong>About', $product['description']);

         //  }

          echo $about;
          echo $how_to_redeem;
          echo $terms_and_condition;

        ?>

      </p>

    </div>   

  

  </div>

  

</body>

</html>