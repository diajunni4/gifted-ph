<html>

<head>
  <title>You've been Gifted</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <style type="text/css">
    body {
      padding: 0;
      font-family: "Helvetica", tahoma;
      font-size: 12px;
      color: #434345;
    }

    html {
      margin: 0;
      padding: 0
    }

    h3,
    h4 {
      margin: 0 0 3px 0;
      padding: 0;
    }

    #category-icon {
      position: absolute;
      right: 171px;
      top: 120px;
    }

    #category-icon img {
      max-width: 100%;
      width: 135px;
      height: 95px;
    }

    #product-name {
      display: block;
      left: 276px;
      padding: 0;
      position: absolute;
      text-align: center;
      top: 249px;
      width: 420px;

    }

    #product-name h3 {
      font-size: 14px;
    }

    #product-name h4 {
      font-size: 12px;
    }

    #product-name span {
      font-size: 12px !important;
    }

    #wrapper {
      width: 725px;
      margin-left: auto;
      margin-right: auto;
      position: absolute;
      top: 1%;
      bottom: 0;
      left: 5%;
      right: 0;

      height: auto;
    }

    #message {
      left: 6px;
      position: absolute;
      top: 242px;
      width: 299px;
      color: #fff;
    }

    #message h3 {
      font-size: 14px;
    }

    #message p {
      font-size: 12px;
      padding-right: 35px;
    }

    #main-gc-img {
      width: 725px;
      max-width: 100%;
      margin: 0 auto;
    }
  </style>
</head>

<body>
  <?php
  $greetings = json_decode($order_details->gift_to_details);
  $greeting_card = $order_details->greeting_card_id;
  $brand_name = $order_details->brand_name;
  $brand_img  = $order_details->image;
  $product_name = $order_details->product_name;
  $firstname = $order_details->firstname;
  $lastname = $order_details->lastname;
  ?>
  <div id="wrapper">

    <img src="<?php echo card_img($greeting_card, 'cards') ?>" alt="You've been gifted" id="main-gc-img">

    <div id="message">
      <?php if ($greetings->name) { ?>
        <h3>Dear <?php echo $greetings->name; ?></h3>
      <?php } ?>
      <p>
        <?php echo nl2br($greetings->message); ?>
      </p>
      <h3>From: <?php echo $greetings->from_name ? $greetings->from_name : $firstname . ' ' . $lastname; ?></h3>
    </div>

    <div id="category-icon">
<!--     
    <img width="204" height="150" src="<?php echo category_img('d47feaca474d15111ea5112712aea5c8.png')?>" alt="" > -->
      <img width="204" height="150" src="<?php echo brands_cover_img($brand_img, 'medium') ?>">
    </div>

    <div id="product-name">
      <h4><?php echo $greetings->sub_category?></h4>
      <h3><span style="font-size:14px;color:#999;">at </span><?php echo $brand_name; ?></h3>
      <h3><?php echo $order_details->quantity.' x '.$product_name ?></h3>
    </div>

  </div>


</body>

</html>