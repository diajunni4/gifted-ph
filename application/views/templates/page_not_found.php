<!doctype html>
<html lang="en">

<head>
   <?php $this->load->view('includes/meta'); ?>
   <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
   <link rel="stylesheet" type="text/css" href="assets/css/global.css">
   <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js"></script>
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-sham.min.js"></script>
   <script data-ad-client="ca-pub-4408418992162786" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
   <title>Pabile</title>
</head>

<body class="homepage">
   <?php $this->load->view('includes/header'); ?>
   <div class="container content-container" id="404">

      <h1>Page Not Found</h1>


      <div class="row">

         <div class="col-lg-12">
            <div class="alert alert-danger" role="alert">
               <strong>This page is no longer avaialble. </strong> <br><br>

               Visit <a href="https://www.gifted.ph/">www.gifted.ph</a> to browse through our gift certificates.
            </div>
         </div>


      </div>




   </div>
   <?php $this->load->view('includes/footer'); ?>

   <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   <!-- <script type="text/javascript" src="<?php echo scripts_bundle('jquery.suggest.js') ?>"></script> -->
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
   <script type="text/javascript" src="<?php echo scripts_bundle('global.js') ?>"></script>
</body>

</html>