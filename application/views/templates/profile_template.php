<!doctype html>
<html lang="en">

<head>
   <?php $this->load->view('includes/meta'); ?>
   <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
   <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('slick-theme.css'); ?>">
   <link rel="stylesheet" href="<?php echo styles_bundle('sweetalert2.min.css') ?>">
   <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('cart.css'); ?>">
   <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('global.css'); ?>">
   <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js"></script>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-sham.min.js"></script>
   <script data-ad-client="ca-pub-4408418992162786" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

   
      <title>Pabile</title>
</head>

<body class="cart_summary">
   <?php $this->load->view('includes/header'); ?>
   <?php $this->load->view('pages/profile'); ?>
   <?php $this->load->view('includes/footer'); ?>

   <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
   <script src = "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?php echo scripts_bundle('sweetalert2.min.js') ?>"></script>
   <script type="text/javascript" src="<?php echo scripts_bundle('global.js'); ?>"></script>
   <script>
      const base_url = "<?php echo base_url() ?>";
      $("#search_term").autocomplete({
      source: function(request, response) {
        $.ajax({
          url: "<?php echo base_url() ?>api/get_brands",
          dataType: "json",
          data: {
            searchText: request.term
          },
          success: function(data) {
            response($.map(data.brands, function(item) {
              return {
                label: item.name,
                value: item.slug
              };
            }));
          }
        });
      },
      minLength: 3,
      select: function(event, ui) {
        window.location.href = "<?php echo base_url() ?>" + ui.item.value
      }
    });
   </script>
   <script type="text/javascript" src="<?php echo base_url() ?>assets/src/dist/js/app.js?random=<?php echo uniqid(); ?>"></script>
</body>

</html>