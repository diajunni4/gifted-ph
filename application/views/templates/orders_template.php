<!doctype html>
<html lang="en">

<head>
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo images_bundle('favicon.png') ?>">
  <?php $this->load->view('includes/meta'); ?>
  <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo styles_bundle('sweetalert2.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('global.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('gifted-style.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('howitworks.css') ?>">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-sham.min.js"></script>
  <script data-ad-client="ca-pub-4408418992162786" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <link href='https://fonts.googleapis.com/css?family=Quicksand:400,700,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
  <script>
    var base_url = "<?php echo base_url() ?>";
  </script>
</head>

<body class="orders">
  <?php $this->load->view('includes/header'); ?>
  <?php $this->load->view($content); ?>

  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script type="text/javascript" src="<?php echo scripts_bundle('sweetalert2.min.js') ?>"></script>
  <script type="text/javascript" src="<?php echo scripts_bundle('global.js') ?>"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/src/dist/js/app.js?random=<?php echo uniqid(); ?>"></script>
  <script>
    $("#search_term").autocomplete({
      source: function(request, response) {
        $.ajax({
          url: "<?php echo base_url() ?>api/get_brands",
          dataType: "json",
          data: {
            searchText: request.term
          },
          success: function(data) {
            response($.map(data.brands, function(item) {
              return {
                label: item.name,
                value: item.slug
              };
            }));
          }
        });
      },
      minLength: 3,
      select: function(event, ui) {
        window.location.href = "<?php echo base_url() ?>" + ui.item.value
      }
    });
  </script>
</body>

</html>