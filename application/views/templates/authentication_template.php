<!doctype html>
<html lang="en">

<head>
	<?php $this->load->view('includes/meta'); ?>
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!-- Optional theme -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('authentication.css') ?>"> 
  <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('gifted-style.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('howitworks.css') ?>">
  <link rel="stylesheet" href="<?php echo styles_bundle('sweetalert2.min.css') ?>">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-sham.min.js"></script>
	<!-- scripts -->

	<!-- fonts -->
	<link href='https://fonts.googleapis.com/css?family=Quicksand:400,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<style>
	.authentication .signin-image {
		margin: 0 auto !important;
	}
	#result2 p,#result {
		text-align:center; color:red;
		margin-bottom:15px;
	}
	</style>
</head>

<body class="homepage">

	<?php $this->load->view('includes/header'); ?>
	<?php $this->load->view('pages/authentication'); ?>
	<?php $this->load->view('includes/footer'); ?>

	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="<?php echo scripts_bundle('sweetalert2.min.js') ?>"></script>
	<script src="<?php echo scripts_bundle('spin.min.js') ?>"></script>
	<script src="<?php echo scripts_bundle('ladda.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo scripts_bundle('global.js') ?>"></script>
	<script type="text/javascript">
		$("#search_term").autocomplete({
      source: function(request, response) {
        $.ajax({
          url: "<?php echo base_url() ?>api/get_brands",
          dataType: "json",
          data: {
            searchText: request.term
          },
          success: function(data) {
            response($.map(data.brands, function(item) {
              return {
                label: item.name,
                value: item.slug
              };
            }));
          }
        });
      },
      minLength: 3,
      select: function(event, ui) {
        window.location.href = "<?php echo base_url() ?>" + ui.item.value
      }
    });
		function progress() {
			let percent = document.getElementById('loading_percentage');
			let count = 5;
			let progress = 25;
			let id = setInterval(frame, 50);

			function frame() {
				if (count == 100) {
					clearInterval(id);
					return;
				}
				count += 5;
				percent.innerHTML = count + "%";
			}
		}

		var base_url = '<?php echo base_url() ?>';
		var redirect_url = "<?php echo $redirect_url; ?>";

		$(document).on("click", ".signup-image-link", function(e) {
			e.preventDefault();
			$(".signin-form").hide();
			$(".signup-image-link").hide();
			$(".signup-form").show();
			$(".signin-image-link").show();
		})

		$(document).on("click", ".signup-image-link2", function(e) {
			e.preventDefault();
			$(".signin-form").hide();
			$(".signup-image-link").hide();
			$(".signup-form").show();
			$(".signin-image-link").show();
		})

		$(document).on("click", ".signin-image-link", function(e) {
			e.preventDefault();
			$(".signin-form").show();
			$(".signup-image-link").show();
			$(".signup-form").hide();
			$(".signin-image-link").hide();
		})

		// if (redirect_url == "checkout") {
		// 	$(".signup-image-link").click()
		// }

		$(document).on("submit", '#login-form', function(e) {
			e.preventDefault();
			var post_url = '<?php echo base_url('authentication/login'); ?>';
			var l = Ladda.create(document.querySelector('.login'));
			$.ajax({
				type: 'POST',
				url: post_url,
				data: $('#login-form').serialize(),
				dataType: "json",
				beforeSend: function() {
					l.start();
				},
				success: function(res) {
					l.stop();
					if (res.message == "success") {
						window.location.href = res.url;
					} else {
						$('#login_password').val('');
						$("#result").html(res.message);
					}
				},
				error: function(res) {
					console.log(res);
				}
			});
		});

		$(document).on("submit", '#register-form', function(e) {
			e.preventDefault();
			var post_url = '<?php echo base_url('authentication/register'); ?>';
			var l = Ladda.create(document.querySelector('.register'));
			$.ajax({
				type: 'POST',
				url: post_url,
				data: $('#register-form').serialize(),
				dataType: "json",
				beforeSend: function() {
					l.start();
				},
				success: function(res) {
					console.log(res)
					l.stop();
					if (res.message == "success") {
						window.location.href = res.url;
					} else {
						$('.password').val('');
						$('.cf_password').val('');
						$("#result2").html(res.message);
					}
				},
				error: function(res) {
					console.log(res);
				}
			});
		});
	</script>
</body>

</html>