<!doctype html>
<html lang="en">

<head>
   <title><?php echo $brands->seo_title ?></title>
   <?php echo $brands->meta ?>
   <?php $this->load->view('includes/meta'); ?>

   <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">
   <!-- Latest compiled and minified CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
   <!-- Optional theme -->
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
   <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('gifted-style.css') ?>">
   <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('global.css') ?>">
   <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('howitworks.css') ?>">
   <link rel="stylesheet" href="<?php echo styles_bundle('sweetalert2.min.css') ?>">
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.min.js"></script>
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-sham.min.js"></script>
   <!-- scripts -->

   <!-- fonts -->
   <link href='https://fonts.googleapis.com/css?family=Quicksand:400,700,300' rel='stylesheet' type='text/css'>
   <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
</head>

<body class="products_template">
   <?php
   $this->load->view('includes/header');
   $this->load->view('pages/product');
   ?>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   <!-- Latest compiled and minified JavaScript -->
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <script type="text/javascript" src="<?php echo scripts_bundle('sweetalert2.min.js') ?>"></script>
   <script type="text/javascript" src="<?php echo scripts_bundle('global.js') ?>"></script>
   <script>
      var base_url = "<?php echo base_url() ?>";
      var slug = "<?php echo $this->uri->segment(1); ?>";
      var is_logged = "<?php echo $is_logged ?>";
      $("#search_term").autocomplete({
         source: function(request, response) {
            $.ajax({
               url: "<?php echo base_url() ?>api/get_brands",
               dataType: "json",
               data: {
                  searchText: request.term
               },
               success: function(data) {
                  response($.map(data.brands, function(item) {
                     return {
                        label: item.name,
                        value: item.slug
                     };
                  }));
               }
            });
         },
         minLength: 3,
         select: function(event, ui) {
            window.location.href = "<?php echo base_url() ?>" + ui.item.value
         }
      });
   </script>
   <script type="text/javascript" src="<?php echo base_url() ?>assets/src/dist/js/app.js?random=<?php echo uniqid(); ?>"></script>

</body>

</html>