<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// $default_controller = "Homepage";
// $controller_exceptions = array("test2");
// foreach($controller_exceptions as $method) {
//    $route[$method] = "$default_controller/".$method;
//    $route[$method."/(.*)"] = "$default_controller/".$method."/$1";
// }




$route['homepage'] = 'Homepage';
//$route['product/(:any)'] = 'Products/view_product/$1';
$route['products'] = 'Products';
$route['authentication'] = 'Authentication';
$route['cart'] = 'Cart';
$route['coupons_api'] = 'Coupons_api';
$route['checkout'] = 'Checkout';
$route['my_account'] = 'My_account';
$route['order'] = 'Order';
$route['order_summary'] = 'Order_summary';
$route['our_store'] = 'Our_store';
$route['privacy-policy'] = 'Privacy_policy';
$route['products_api'] = 'Products_api';
$route['categories_api'] = 'Categories_api';
$route['company_api'] = 'Company_api';
$route['brand_api'] = 'Brands_api';
$route['widgets_api'] = 'Widgets_api';
$route['secure_admin'] = 'Secure_admin/index';
$route['shipping'] = 'Shipping';
$route['thank_you'] = 'Thank_you';
$route['users_api'] = 'Users_api';
$route['secure_login'] = 'Secure_login';
$route['thank_you'] = 'Thank_you';
$route['api'] = 'Api';
$route['orders'] = 'Orders';
$route['audits_api'] = 'Audits_api';
$route['vouchers_api'] = 'Vouchers_api';
$route['tos'] = 'Tos';
$route['run_product'] = 'Run_product';
$route['contact'] = 'Contact';
$route['visit_us'] = 'Visit_us';
$route['wallet'] = 'Wallet';
$route['wallet_api'] = 'Wallet_api';
$route['open_lazada'] = 'Open_lazada';
$route['validate'] = 'Validate';
$route['redeem'] = 'Redeem';
$route['profile'] = 'Profile';
$route['contact'] = 'Contact';
$route['faq'] = 'Homepage/faq';
$route['orders/view-order/(:any)'] = 'orders/view_order/$1';
$route['api/downloadVoucher/(:any)'] = 'Api/downloadVoucher/$1';
$route['download/(:any)'] = 'Api/downloadVoucher/$1';
$route['api/downloadGreetingCard/(:any)'] = 'Api/downloadGreetingCard/$1';
$route['default_controller'] = 'Homepage';
$route['404_override'] = 'Page_error';
$route['how-it-works'] = 'Homepage/how_it_works';
$route['translate_uri_dashes'] = FALSE;
$route['(:any)'] = 'Products/view_product/$1';
