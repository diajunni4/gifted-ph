<?php
defined('BASEPATH') or exit('No direct script access allowed');

// ------------------------------------------------------------------------


if (!function_exists('encrypt')) {

  function encrypt($string)
  {
    $output = FALSE;

    $encrypt_method = "AES-256-CBC";
    $secret_key = '$6$/!@#$%$3cr3tk3y%$#@!';
    $secret_iv = '$6$/!@#$%$3cr3tIV%$#@!';

    // hash
    $key = hash('sha256', $secret_key);

    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
    $output = base64_encode($output);

    return $output;
  }
}

if (!function_exists('admin_folder')) {
  function admin_folder()
  {
    return base_url('admin/');
  }
}

function random_code($limit)
{ 
  return strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit));
}

if (!function_exists('decrypt')) {

  function decrypt($string)
  {
    $output = FALSE;

    $encrypt_method = "AES-256-CBC";
    $secret_key = '$6$/!@#$%$3cr3tk3y%$#@!';
    $secret_iv = '$6$/!@#$%$3cr3tIV%$#@!';
    // hash
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    return $output;
  }
}

function in_arrayi($needle, $haystack)
{
  return in_array(strtolower($needle), array_map('strtolower', $haystack));
}

if (!function_exists('clean_data')) {
  function clean_data($string)
  {
    return trim(strip_tags(stripslashes($string)));
  }
}

if (!function_exists('generateRandomString')) {
  function generateRandomString($length = 10)
  {

    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }
}

function generateOrderKey($length = 7)
{
  $ci = &get_instance();
  do {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    $filter =  ["order_key" => $randomString];
    $check_exist = $ci->Global_model->check_exist("orders", $filter);
  } while ($check_exist > 0);

  return $randomString;
}

function generateOrderNumber()
{
  $ci = &get_instance();
  do {
    $order_number = date('YmdHis') . rand(10, 99);
    $filter =  ["order_number" => $order_number];
    $check_exist = $ci->Global_model->check_exist("orders", $filter);
  } while ($check_exist > 0);

  return $order_number;
}


if (!function_exists('is_logged')) {
  function is_logged()
  {
    $ci = &get_instance();
    $cookie_key = !empty($_COOKIE['ses_key']) ? $_COOKIE['ses_key'] : '';
    $cookie_pass = !empty($_COOKIE['ses_pass']) ? $_COOKIE['ses_pass'] : '';
    $ci->load->model('User_model');
    if ($ci->session->ses_key != "") {
      $email = decrypt($ci->session->ses_key);
      $filter = ["email" => $email];
      $status = $ci->User_model->check_exist('users', $filter);
      if ($status)
        return 1;
      else
        return 0;
    } else {
      if ($cookie_key != "" && $cookie_pass != "") {
        $email = decrypt($cookie_key);
        $password = decrypt($cookie_pass);
        $filter = ["email" => $email];
        $row = $ci->User_model->fetch_tag_row('password', 'users', $filter);
        if (empty($row)) {
          return 0;
        } else {
          $user_password = $row->password;
          if (password_verify($password, $user_password)) {
            return 1;
          } else {
            return 0;
          }
        }
      } else {
        return 0;
      }
    }
  }
}

function clean_data_arr2($arr)
{
  $clean_arr = [];
  foreach ($arr as $key => $value) {
    $value = clean_data($value);
    $to_push = [$key => $value];
    array_push($clean_arr, $to_push);
  }
  return $clean_arr;
}

function clean_data_arr($arr)
{
  $clean_arr = [];
  foreach ($arr as $tmp_arr => $tmp_new_arr) {
    $new_arr = $tmp_new_arr;
    foreach ($new_arr as $key => $value) {
      $value = clean_data($new_arr[$key]);
      $to_push = [$key => $value];
      array_push($clean_arr, $to_push);
    }
  }
  return $clean_arr;
}

function getFirstName($string)
{
  $names = explode(' ', $string);
  return ucwords($names[0]);
}

function getLastName($string)
{
  $names = explode(' ', $string);
  $lastname = end($names);
  $name_suffix = array('JR', 'jr', 'jr.', 'Jr', 'Jr.', 'II', 'III', 'IV', 'MD', 'md', 'M.D.', 'Ph.D.', 'PhD', 'SR', 'Sr.', 'Sr', 'sr');
  if (in_array($lastname, $name_suffix, true)) {
    $name = explode(' ', $string);
    $lastname_key = count($name) - 2;
    $lastname  = $name[$lastname_key - 1];
  }
  return ucwords($lastname);
}

function get_sku_merchant_id($sku)
{
  $first_dash_pos = strpos($sku, "-");
  if ($first_dash_pos === false) {
    $first_dash_pos = strpos($sku, "_");
  }

  $tmp_sku = substr($sku, 0, $first_dash_pos);
  $merchant_id = substr($tmp_sku, 8);
  return $merchant_id;
}

function httpGet($url)
{
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  //  curl_setopt($ch,CURLOPT_HEADER, false); 

  $output = curl_exec($ch);

  curl_close($ch);
  return $output;
}
function get_sku_denomination($sku)
{
  $first_dash_pos = strpos($sku, "-");
  if ($first_dash_pos === false) {
    $first_dash_pos = strpos($sku, "_");
  }

  $gifted_amount = substr($sku, $first_dash_pos + 1);
  return $gifted_amount;
}

if (!function_exists('is_logged_admin')) {
  function is_logged_admin()
  {
    $ci = &get_instance();
    $ci->load->model('User_model');
    if ($ci->session->ses_key != "") {
      $email = decrypt($ci->session->ses_key);
      $filter = ["email" => $email, "role" => "admin"];
      $status = $ci->User_model->check_exist('users', $filter);
      if ($status)
        return 1;
      else
        return 0;
    } else {
      return 0;
    }
  }
}

function generate_invoice_num($input, $pad_len = 7, $prefix = null)
{
  if ($pad_len <= strlen($input))
    trigger_error('<strong>$pad_len</strong> cannot be less than or equal to the length of <strong>$input</strong> to generate invoice number', E_USER_ERROR);

  if (is_string($prefix))
    return sprintf("%s%s", $prefix, str_pad($input, $pad_len, "0", STR_PAD_LEFT));

  return str_pad($input, $pad_len, "0", STR_PAD_LEFT);
}


if (!function_exists('get_email')) {
  function get_email()
  {
    $ci = &get_instance();
    $cookie_key = !empty($_COOKIE['ses_key']) ? $_COOKIE['ses_key'] : $ci->session->ses_key;
    $email = "";
    if ($cookie_key != '')
      $email = decrypt($cookie_key);
    return $email;
  }
}

if (!function_exists('get_username')) {
  function get_username()
  {
    $ci = &get_instance();
    $cookie_key = !empty($_COOKIE['ses_key']) ? $_COOKIE['ses_key'] : $ci->session->ses_key;
    $username = strstr(decrypt($cookie_key), '@', true);;
    return $username;
  }
}



if (!function_exists('hash_password')) {
  function hash_password($password)
  {
    // $options = array(
    //   'cost' => 11,
    // );
    // return password_hash($password, PASSWORD_BCRYPT, $options);
    return sha1($password);
  }
}

if (!function_exists('check_privilege')) {
  function check_privilege($page_privilege, $privilege)
  {
    $privilege_array = explode(',', $privilege);
    $exists = in_array($page_privilege, $privilege_array);
    return $exists;
  }
}

if (!function_exists('sendMail')) {
  function sendMail($heading = '', $content = '', $from = '', $receiver = '') //lagay nalang parameter ilalagay natin sa helper para global
  {
    require_once(APPPATH . 'third_party/PHPMailer-master/PHPMailerAutoload.php');
    $mail = new PHPMailer();
    $mail->IsSMTP(); // we are going to use SMTP
    $mail->SMTPAuth   = true; // enabled SMTP authentication
    $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
    $mail->Host       = "ssl://smtp.googlemail.com";      // setting GMail as our SMTP server
    $mail->Port       = 465;                   // SMTP port to connect to GMail
    $mail->Username   = "jonasdulay28@gmail.com";  // user email address
    $mail->Password   = "";               // password in GMail
    $mail->SetFrom($receiver, 'Mail');  //Who is sending 
    $mail->isHTML(true);
    $mail->Subject    = "PABILE - " . $heading;
    $mail->Body      = '
            <html>
            <head>
                <title>' . $heading . '</title>
            </head>
            <body>
            <h3>' . $heading . '</h3>
            <br>
            ' . $content . '
            <br>
            <p>With Regards</p>
            <p>' . $from . '</p>
            </body>
            </html>
        ';
    $mail->AddAddress($receiver, $receiver);
    if (!$mail->Send()) {
      return 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
      return 'success';
    }
  }
}

if (!function_exists('sendMail_file')) {
  function sendMail_file($heading = '', $content = '', $from = '', $receiver = '', $path) //lagay nalang parameter ilalagay natin sa helper para global
  {
    require_once(APPPATH . 'third_party/PHPMailer-master/PHPMailerAutoload.php');
    $mail = new PHPMailer();
    $mail->addAttachment($path, $name = '', $encoding = 'base64', $type = 'application/octet-stream');
    $mail->IsSMTP(); // we are going to use SMTP
    $mail->SMTPAuth   = true; // enabled SMTP authentication
    $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
    $mail->Host       = "ssl://smtp.googlemail.com";      // setting GMail as our SMTP server
    $mail->Port       = 465;                   // SMTP port to connect to GMail
    $mail->Username   = "jonasdulay28@gmail.com";  // user email address
    $mail->Password   = "Watheshet288";            // password in GMail
    $mail->SetFrom($receiver, 'Mail');  //Who is sending 
    $mail->isHTML(true);
    $mail->Subject    = $heading . " - PACUCOA";
    $mail->Body      = '
            <html>
            <head>
                <title>Title</title>
            </head>
            <body>
            <h3>' . $heading . '</h3>
            <br>
            ' . $content . '
            <br>
            <p>With Regards</p>
            <p>' . $from . '</p>
            </body>
            </html>
        ';
    $mail->AddAddress($receiver, $receiver);
    if (!$mail->Send()) {
      return false;
    } else {
      return true;
    }
  }
}

if (!function_exists('audit')) {
  function audit($action, $description, $type, $additional_info)
  {

    $ci = &get_instance();
    $email = get_email();
    $ci->load->library('user_agent');
    $agent = "Unidentified User Agent";
    if ($ci->agent->is_browser()) {
      $agent = $ci->agent->browser() . ' ' . $ci->agent->version();
    } elseif ($ci->agent->is_robot()) {
      $agent = $this->agent->robot();
    } elseif ($ci->agent->is_mobile()) {
      $agent = $ci->agent->mobile();
    }

    $platform =  $ci->agent->platform(); // Platform info (Windows, Linux, Mac, etc.)

    $ci->load->model('Audit_model');
    $mode = flag('mode', 'live');

    if ($mode = "test") {
      $data = array(
        "action" => $action, "description" => $description, "type" => $type, "additional_info" => $additional_info,
        "email" => $email, "user_agent" => $agent, "platform" => $platform, "ip_address" => $ci->input->ip_address(), "mode" => "test"
      );
    }

    $data = array(
      "action" => $action, "description" => $description, "type" => $type, "additional_info" => $additional_info,
      "email" => $email, "user_agent" => $agent, "platform" => $platform, "ip_address" => $ci->input->ip_address(), "mode" => "live"
    );
    $ci->Audit_model->insert('tbl_audit', $data);

    return true;
  }
}


function brands_cover_img($uri, $size = false)

{

  $dir = ($size) ? $size . '/' : '';

  if (file_exists('uploads/brands/' . $dir . $uri)) {

    return base_url('uploads/brands/' . $dir . $uri);
  }

  return theme_img('logo-bw.png');
}

function theme_img($uri, $tag = false)
{
  if ($tag) {

    return '<img src="' . theme_url('assets/img/' . $uri) . '" alt="' . $tag . '">';
  } else {

    return theme_url('assets/img/' . $uri);
  }
}

function card_img($img, $size='small')
{
	
	if( file_exists('uploads/greeting_cards/'.$size.'/'.$img.'.jpg') )
	{
		$last_mod = '?lm='.filemtime('uploads/greeting_cards/'.$size.'/'.$img.'.jpg');
	}
		
	return base_url('uploads/greeting_cards/'.$size.'/'.$img.'.jpg'.$last_mod);	

}

function category_img($img)
{
	if( !empty($img) && file_exists(FCPATH."uploads/greeting_cards/category_icons/$img") )

	{
		return base_url("uploads/greeting_cards/category_icons/$img");	
	}
	return base_url('uploads/greeting_cards/category_icons/d47feaca474d15111ea5112712aea5c8.png');
}

function format_currency($value, $symbol=true)
{
	$fmt = numfmt_create( "PHP", NumberFormatter::CURRENCY );
	return numfmt_format_currency($fmt, $value, "PHP"); 
	
	//return $value;
}


function theme_url($uri)

{

	$CI =& get_instance();

	return $CI->config->base_url($uri);

}


if (!function_exists('sendMailCIMailer')) {
  function sendMailCIMailer($email_content)
  {
    $ci = &get_instance();
    $ci->load->library('email');
    $_config = array(
      'protocol' => 'ssl',
      'smtp_host' => 'smtp.lazada.gifted.ph',
      'smtp_user' => 'info@gifted.ph',
      'smtp_pass' => 'Gifted112014!!!',
      'smtp_port' => 465,
      'mailtype' => 'html',
      'charset' => 'utf-8',
      'crlf' => "\r\n",
      'newline' => "\r\n",
      'smtp_crypto' => "ssl",
      'wordwrap' => TRUE
    );
    $ci->email->initialize($_config);
    $ci->email->from($email_content['from'], $email_content['from_name']);
    $ci->email->to($email_content['to']);
    $ci->email->subject($email_content['subject']);
    //$ci->email->cc('marc_daniel_213@yahoo.com');
    $ci->email->bcc('system@gifted.ph');
    $ci->email->message($email_content['message']);
    if ($ci->email->send()) {
      return true;
    } else {
      var_dump($ci->email->print_debugger());
      die;
      return false;
    }
  }
}

if ( ! function_exists('sendMainGreetingCard')) {
  function sendMainGreetingCard($email_content)
  {
      $ci =& get_instance();
      $ci->load->library('email');
      $_config = array(
        'protocol' => 'ssl',
        'smtp_host' => 'smtp.lazada.gifted.ph',
        'smtp_user' => 'info@gifted.ph',
        'smtp_pass' => 'Gifted112014!!!',
        'smtp_port' => 465,
        'mailtype' => 'html',
        'charset' => 'utf-8',
        'crlf' => "\r\n",
        'newline' => "\r\n",
        'smtp_crypto' => "ssl",
        'wordwrap' => TRUE
      );
      $ci->email->initialize($_config);
      $ci->email->from($email_content['from'], $email_content['from_name']);
      $ci->email->to($email_content['to']);
      $ci->email->bcc('system@gifted.ph');
      $ci->email->subject($email_content['subject']);
      $ci->email->message($email_content['message']);
      $ci->email->attach($email_content['attachment'],  'attachment', 'greeting_card.pdf', 'application/pdf');  
      if($ci->email->send()){
          return true;
      }else{
           var_dump($ci->email->print_debugger()); 
           die;
          return false;               
      }
  }
}



function format_currency2($value)
{
  return 'PHP ' . number_format($value, 2, '.', ',');
}


if (!function_exists('sendMailCIMailer2')) {
  function sendMailCIMailer2($email_content)
  {
    $ci = &get_instance();
    $ci->load->library('email');
    $_config = array(
      'protocol' => 'tls',
      'smtp_host' => '',
      'smtp_user' => '',
      'smtp_pass' => '',
      'smtp_port' => 993,
      'mailtype' => 'html',
      'charset' => 'utf-8',
      'crlf' => "\r\n",
      'newline' => "\r\n",
      'smtp_crypto' => "tls",
      'wordwrap' => TRUE
    );
    $ci->email->initialize($_config);
    $ci->email->from($email_content['from'], $email_content['from_name']);
    $ci->email->to($email_content['to']);
    $ci->email->subject($email_content['subject']);
    if (isset($email_content['reply_to'])) {
      $ci->email->reply_to($email_content['reply_to']);
    }
    $ci->email->cc('support@sabaicards.com');
    $ci->email->bcc('bktan@sabaicards.com');
    $ci->email->message($email_content['message']);
    if ($ci->email->send()) {
      return true;
    } else {
      var_dump($ci->email->print_debugger());
      die;
      return false;
    }
  }
}


if (!function_exists('check_status')) {
  function check_status($status = 0)
  {
    if ($status == '0') {
      return 'Pending';
    } else if ($status == '1') {
      return 'Processing';
    } else if ($status == '2') {
      return 'On Hold';
    } else if ($status == '3') {
      return 'Completed';
    } else if ($status == '4') {
      return 'Cancelled';
    } else if ($status == '5') {
      return 'Refunded';
    }

    return 'Failed';
  }
}

if (!function_exists('flag')) {
  function flag($flag, $defaultValue)
  {
    $flag_value = !empty($_GET[$flag]) ? $_GET[$flag] : (!empty($_COOKIE[$flag]) ? $_COOKIE[$flag] : $defaultValue);
    setcookie($flag, $flag_value, time() + (86400 * 30));
    return $flag_value;
  }
}

if (!function_exists('styles_bundle')) {
  function styles_bundle($style)
  {
    return base_url('assets/css/' . $style);
  }
}

if (!function_exists('images_bundle')) {
  function images_bundle($image)
  {
    return base_url('assets/images/' . $image);
  }
}

if (!function_exists('images_bundle2')) {
  function images_bundle2($image)
  {
    return base_url('assets/img/' . $image);
  }
}

if (!function_exists('scripts_bundle')) {
  function scripts_bundle($script)
  {
    return base_url('assets/js/' . $script);
  }
}

if (!function_exists('material_scripts')) {
  function material_scripts()
  {
    return base_url('material/assets/js/');
  }
}
if (!function_exists('material_styles')) {
  function material_styles()
  {
    return base_url('material/assets/css/');
  }
}

if (!function_exists('third_party_bundle')) {
  function third_party_bundle()
  {
    return base_url('assets/third_party/');
  }
}

if (!function_exists('pad')) {
  function pad($num, $size)
  {
    $s = $num . "";
    while (strlen($s) < $size) $s = "0" . $s;
    return $s;
  }
}


if (!function_exists('post')) {
  function post($name)
  {
    $controller = &get_instance();
    return $controller->input->post($name);
  }
}

if ( ! function_exists('format_date'))
{

	function format_date($date){	
	if ($date != '' && $date != '0000-00-00')
	{
		$d	= explode('-', $date);
	
		$m	= Array(
		'January'
		,'February'
		,'March'
		,'April'
		,'May'
		,'June'
		,'July'
		,'August'
		,'September'
		,'October'
		,'November'
		,'December'
		);
	
		return $m[$d[1]-1].' '.$d[2].', '.$d[0]; 
	}
	else
	{
		return false;
	}
}

}

if (!function_exists('get')) {
  function get($name)
  {
    $controller = &get_instance();
    return $controller->input->get($name);
  }
}


function replace()
{
  return preg_replace_callback(
    '/\{([^\}]+)\}/',
    array($this, 'to_symbols'),
    $this->text
  );
}

if (!function_exists('getCookie')) {
  function getCookie($name = '')
  {
    return isset($_GET[$name]) ? $_GET[$name] : (isset($_COOKIE[$name]) ? $_COOKIE[$name] : '');
  }
}

/*if ( ! function_exists('get_level_name')) {
	function get_level_name($level) {
		$this->load->model('Crud_model');
		$filter = array('level_id'=>$level);
		$row = $this->Crud_model->fetch_tag_row('level_name','tbllevel',$filter);
		return $row->level_name;
	}

}*/



function check_direct_access()
{
  if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    return true;
  } else {
    return false;
  }
}
